<?php
/**
 * Created by PhpStorm.
 * User: Riko
 * Date: 8/16/2016
 * Time: 5:12 PM
 */

use Carbon\Carbon;
use Jenssegers\Date\Date;
use App\Logic\WebCache;

function getConfig()
{
    return WebCache::config()->pluck('value', 'key');
}

function activeUrl($url, $output = 'class=active')
{
    if(request()->url() == $url){
        return $output;
    }
}


function activeQuery($url, $output = 'class=active')
{
    if(request()->url().'?'.request()->getQueryString() == $url){
        return $output;
    }
}

function loading(){
    return "<span class='fa fa-circle-o-notch fa-spin'></span>";
}

function g_icon($class, $title = null, $spinning = null){
    return "<i class='fa fa-circle-o-notch fa-spin'></i>";
}


function icon($icon, $title = null){
    return "<i class='fa fa-$icon'></i>";
}

function filterDate($date, $tz = true, $time = 'day')
{
    $date = with(new Carbon($date));

    if($tz == true){
        $date = $date->timezone('Asia/Baku');
    }

    if($time == 'eFull'){
        $date = $date->format('d/m/Y, H:i');
    }
    elseif($time == 'full'){
        $date = $date->format('Y-m-d, H:i');
    }
    elseif($time == 'day'){
        $date = $date->format('Y-m-d');
    }
    elseif($time == 'eDay'){
        $date = $date->format('d/m/Y');
    }
    elseif($time == 'time'){
        $date = $date->format('H:i');
    }
    elseif($time == 'xeditableDate'){
        $date = $date->format('d/m/Y H:i'); //vergul yoxdu
    }
    return $date;
}

function convertDate($date, $time = false, $symbol = '/')
{
    if($time == false){
        list($d, $m, $y) = explode($symbol, $date);
        return "$y-$m-$d";
    }
    else{
        list($d1, $d2) = explode(' ', $date);
        list($d, $m, $y) = explode($symbol, $d1);

        return "$y-$m-$d $d2";
    }
}

function blogDate($date,$symbol='.')
{
    return Carbon::parse($date)->format('d'.$symbol.'m'.$symbol.'Y');
}

function getYoutubeId($url)
{
    preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $url, $match);
    return $match[1];
}

function pageName($name)
{
    $remove = explode(' ', $name)[0];

    $replace = '<strong>'.$remove.'</strong>';

    $newName = str_replace($remove, $replace, $name);

    return $newName;
}

function convertName($name,$symbol = null)
{
    if($symbol != null){
        $explod_name = explode($symbol, $name);

        if(count($explod_name) > 1){
            $name = $explod_name[0].'</br>'.$explod_name[1];
        }
    }

    return $name;
}

function removeSymbol($name,$symbol = null)
{
    if($symbol != null){
        $explod_name = explode($symbol, $name);

        if(count($explod_name) > 1){
            $name = $explod_name[0].' '.$explod_name[1];
        }
    }

    return $name;
}


function percentage($main_price,$app_price)
{

    if($app_price >= $main_price){
        $percentage = 100;
    }else if($app_price <= 0 ){
        $percentage = 0;
    }else{

        $percentage = floor(($app_price * 100)/$main_price);
    }

    return $percentage;
}



function user_application_name($name)
{
    $explode = explode(' ', $name);
    if($explode && isset($explode[0]) && isset($explode[1])){
        $name = $explode[0].' <br>'.$explode[1];
    }

    return $name;
}



function albumLimit()
{
    return 15;
}

function colorText($name)
{
    $remove = explode(' ', $name)[0];

    $replace = '<span class="main-color">'.$remove.'</span>';

    $newName = str_replace($remove, $replace, $name);

    return $newName;
}

function clearCache($cache, $lang = true)
{
    if($lang == true){
        foreach(config('app.locales') as $key => $locale){
            Cache::forget($cache.'_'.$key);
        }
    }
    else{
        Cache::forget($cache);
    }
}


function encode($data)
{
    $newData = [];

    foreach($data as $key => $d){
        $newData[] = ['value' => $key, 'text' => $d];
    }

    return json_encode($newData);
}



