<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Carbon\Carbon;
class User extends Authenticatable
{
    use Notifiable, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'id', 'created_at', 'updated_at',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    public function getCreatedAtAttribute($value)
    {
        return with(new Carbon($value))->format('d/m/Y, H:i');
    }


    public function getUpdatedAtAttribute($value)
    {
        return with(new Carbon($value))->format('d/m/Y, H:i');
    }


    public static function rules($id = null){
        return [
            'name' => 'required',
            'email' => 'nullable|email|unique:users,email,'.$id,
        ];
    }


    public static $messages = [
        'required' => 'Bütün xanaların doldurulması zəruridir! (Kart № istisna)',
    ];


    public function verify()
    {
        return $this->hasOne('App\Models\VerifyUser', 'user_id', 'id')->orderBy('id','desc');
    }
}
