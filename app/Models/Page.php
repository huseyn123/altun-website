<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;
use DB;

class Page extends Model implements HasMedia
{
    use HasMediaTrait,SoftDeletes;

    protected $guarded = ['id'];
    protected $hidden =  ['_token'];
    protected $dates =   ['created_at', 'updated_at', 'deleted_at'];
    public static $width =1920;
    public static $height =300;

    public static $thumb_width =300;
    public static $thumb_width2 =300;
    public static $thumb_height =100;
    public static $thumb_height2 =80;

    public static $width2 =1920;
    public static $height2 =500;
    public static $gallery_width =1350;
    public static $gallery_height =420;
    public static $template_id =0;

    public static function rules($lang, $id,$template = null){

        self::$width =  config('config.page_size.'.$template.'.width') ?? '1920';
        self::$height =  config('config.page_size.'.$template.'.height') ?? '300';
        self::$width2 =  config('config.page_size2.'.$template.'.width') ?? '1920';
        self::$height2 =  config('config.page_size2.'.$template.'.height') ?? '300';

        return [
            'template_id' => 'required|numeric',
            'name' => 'required|max:255',
            'parent_id' => 'nullable|exists:page_translations,id',
            'slug' => 'unique:page_translations,slug,'.$id.',id,lang,'.$lang,
            'image' => 'max:10000|dimensions:min_width='.self::$width.',min_height='.self::$height,
            'cover' => 'max:10000|dimensions:min_width='.self::$width2.',min_height='.self::$height2,
            'content' => 'nullable',
            'forward_url' => 'nullable',
            'target' => 'boolean',
//            'visible' => 'required',
            'meta_description' => 'nullable|max:160',
        ];
    }

    public static function parameterRules()
    {
        return [
            'template_id' => 'required|numeric',
            'menu_file' => 'nullable|mimes:jpeg,jpg,png,pdf,doc,docx|max:10000',
            'image' => 'max:10000|dimensions:min_width='.self::$width.',min_height='.self::$height,
            'cover' => 'max:10000|dimensions:min_width='.self::$width2.',min_height='.self::$height2,
            'target' => 'boolean',
//            'visible' => 'required',
        ];
    }
    public static $messages = [
        'name.required' => "Ad qeyd olunmayıb.",
        'cover.dimensions' => 'Kover şəkilin ölçüsü :min_width x :min_height nisbətdə olmalıdır.',
    ];

    public function getCreatedAtAttribute($value)
    {
        return filterDate($value, true, 'eFull');
    }

    public function getUpdatedAtAttribute($value)
    {
        return filterDate($value, true, 'eFull');
    }

    public function setMetaKeywordsAttribute($value) {

        $keywords = null;

        if(!is_null($value) && is_array($value))
        {
            $keywords = implode(",", $value);
        }

        $this->attributes['meta_keywords'] = $keywords;
    }


    public function scopeVisible($query)
    {
        return $query->where('visible', '<>', 0);
    }


    public function children()
    {
        return $this->hasMany('App\Models\PageTranslation', 'parent_id', 'tid')
            ->join('pages as p', 'p.id', '=', 'page_translations.page_id')
            ->where('p.visible', '<>', 0)
            ->whereIn('p.template_id',[1])
            ->select('page_translations.id as tid', 'page_translations.name', 'page_translations.slug', 'page_translations.parent_id','page_translations.content', 'p.template_id','p.icon','p.id')
            ->orderBy('page_translations.order', 'asc');
    }


    public function MenuChildren($id ='id')
    {
        return $this->hasMany('App\Models\PageTranslation', 'parent_id', $id);
    }



    public function relatedPages()
    {
        return $this->hasMany('App\Models\PageTranslation', 'page_id', 'page_id');
    }

    public function gallery()
    {
        return $this->hasMany('App\Models\Gallery', 'album_id', 'album_id')
            ->orderBy('id', 'asc')
            ->limit(Album::limit);
    }

    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('thumb')
            ->fit('stretch',250,185)
            ->performOnCollections('gallery')
            ->keepOriginalImageFormat();

        $this->addMediaConversion('blade')
            ->fit('stretch',self::$gallery_width,self::$gallery_height)
            ->performOnCollections('gallery')
            ->keepOriginalImageFormat();

        $this->addMediaConversion('thumb')
            ->fit('stretch',self::$thumb_width,self::$thumb_height)
            ->performOnCollections('default')
            ->keepOriginalImageFormat();

        $this->addMediaConversion('blade')
            ->fit('stretch',self::$width,self::$height)
            ->performOnCollections('default')
            ->keepOriginalImageFormat();

        $this->addMediaConversion('thumb')
            ->fit('stretch',self::$thumb_width2,self::$thumb_height2)
            ->performOnCollections('cover')
            ->keepOriginalImageFormat();

        $this->addMediaConversion('blade')
            ->fit('stretch',self::$width2,self::$height2)
            ->performOnCollections('cover')
            ->keepOriginalImageFormat();


    }

}