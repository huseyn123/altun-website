<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AuthorTranslation extends Model
{

    use SoftDeletes;

    protected $guarded = ['id'];
    protected $hidden =  ['_token'];

    public static function rules($id){
        return [
            'name' => 'required|max:255',
            'slug' => 'unique:product_translations,slug,'.$id.',id',
        ];
    }

    public static $messages = [
        'name.required' => "Ad qeyd olunmayıb.",
    ];


}
