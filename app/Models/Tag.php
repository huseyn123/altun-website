<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    protected $guarded = ['id'];
    protected $hidden = ['_token'];
    protected $dates = ['created_at', 'updated_at'];

    public function getCreatedAtAttribute($value)
    {
        return filterDate($value, true, 'eFull');
    }

    public function getUpdatedAtAttribute($value)
    {
        return filterDate($value, true, 'eFull');
    }


    public static $rules = [
        'keyword' => 'required',
    ];


    public static $messages = [
        'keyword.required' => 'Açar söz yazılmayıb',
    ];
}
