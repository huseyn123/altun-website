<?php

namespace App\Models;

use App\Models\AuthorTranslation;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;
use Auth;
use Db;

class Product extends Model implements HasMedia
{
    use HasMediaTrait, SoftDeletes;

    protected $guarded = ['id'];
    protected $hidden = ['_token'];
    public static $type ='image';
    public static $width =1100;
    public static $height =1600;

    public static $gallery_width =850;
    public static $gallery_height =1200;




    public static function rules($id){

        is_null($id) ? $img = 'required' : $img = 'sometimes';
        return [
            'name' => "required",
            'slug' => 'unique:product_translations,slug,'.$id,
            'image' => $img.'|mimes:jpeg,jpg,png|max:10000|dimensions:min_width='.self::$width.',min_height='.self::$height,
            'page_id' => 'required|exists:page_translations,id',
            'price' => "required",
            'bar_code' => 'required|numeric',
            // 'age_id' => "required",
            'status' => 'boolean',
        ];
    }

    public static function parameterRules()
    {
        return [
            'image' => 'sometimes|mimes:jpeg,jpg,png|max:10000|dimensions:min_width='.self::$width.',min_height='.self::$height,
            'bar_code' => 'required|numeric',
            // 'age_id' => "required",
            'price' => "required",
            'status' => 'boolean',
        ];
    }

    public static $messages = [
        'image.required' => "Şəkil əlavə olunmayıb",
        'image.dimensions' => 'Şəkilin şəkilin ölçüsü :min_width x :min_height nisbətdə olmalıdır.',
        'page_id.required' => 'Kateqoriya seçilməyib',
        'price.required' => 'Qiymət daxil edilməyib',
        'bar_code.required' => 'Barkod daxil edilməyib',
        'age_id.required' => 'Yaş seçilməyib',
    ];

    public function setMetaKeywordsAttribute($value) {

        $keywords = null;

        if(!is_null($value) && is_array($value))
        {
            $keywords = implode(",", $value);
        }

        $this->attributes['meta_keywords'] = $keywords;
    }

    public function getProductEducationAttribute($value)
    {
        if(!is_null($value)){
            $value = explode(",", $value);
        }
        return $value;
    }

    public function getCreatedAtAttribute($value)
    {
        return filterDate($value, true, 'eFull');
    }

    public function getUpdatedAtAttribute($value)
    {
        return filterDate($value, true, 'eFull');
    }

    public function relatedPages()
    {
        return $this->hasMany('App\Models\ProductTranslation', 'product_id', 'id');
    }

    public function trans()
    {
        return $this->hasOne(ProductTranslation::class, 'product_id')->orderBy('product_translations.id', 'asc');
    }

    public function scopeByCategory($query, $value)
    {
        if(!is_null($value)){
            if(is_array($value)){
                return $query->whereIn('pc.page_id', $value);
            }
            else{
                return $query->where('pc.page_id', $value);
            }
        }

        return $query;
    }

    public function scopeByPrice($query, $minPrice, $maxPrice)
    {
        if($minPrice >= 0 && $maxPrice >= 0 & $minPrice <= $maxPrice){
            return $query->whereBetween('products.old_price', [$minPrice, $maxPrice]);
        }
    }

    public function scopeBySort($query,$sort)
    {
        $sort_type = config('config.product_filtr_order_type')[$sort] ?? 0;
        if($sort_type && $sort_type !==0){
            $query->orderBy('products.price', $sort_type);
        }else{
            $query->orderBy('ptr.id','desc');
        }
    }

    public function scopeByAge($query,$age)
    {
        if(isset($age) && count($age)){
            return $query->whereIn('products.age_id', $age);
        }

        return $query;
    }

    public function scopeByCat($query,$cat,$tid)
    {
        if(isset($cat) && count($cat)){
            return $query->whereIn('ptr.page_id', $cat);
        }else{
            return $query->where('ptr.page_id', $tid);

        }

        return $query;
    }


    public function productTags() //for datatable
    {
        return $this->belongsToMany(Tag::class, 'product_tags', 'product_id', 'tag_id', 'id');
    }

    public function productTagsSearch()
    {
        return $this->belongsToMany(Tag::class, 'product_tags', 'product_id', 'tag_id', 'id');
    }

    //Admin Panel
    public function product_authors($id = 'tid')
    {
        return $this->hasMany(ProductAuthors::class, 'product_id', $id);
    }

    public function tags($id = 'id')
    {
        return $this->hasMany(ProductTag::class, 'product_id', $id);
    }

    public function categories($id = 'p_id')
    {
        return $this->hasMany(ProductCategory::class, 'product_id', $id);
    }


    public function rel_author($id = 'tid')
    {
        return $this->hasMany(ProductAuthors::class, 'product_id', $id)
            ->join('author_translations', 'product_authors.author_id', '=', 'author_translations.id')
            ->join('authors', 'authors.id', '=', 'author_translations.author_id')
            ->join('page_translations as pt', 'pt.id', '=', 'author_translations.page_id')
            ->join('pages', 'pages.id', '=', 'pt.page_id')
            ->whereNull('pt.deleted_at')
            ->whereIn('pages.template_id',[5])
            ->whereNull('author_translations.deleted_at')
            ->select('product_authors.product_id','product_authors.author_id','author_translations.name as author_name',
                'author_translations.slug as author_slug',
               'pt.slug as cat_slug');
    }

    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('thumb')
            ->fit('stretch',210,150)
            ->performOnCollections('default')
            ->keepOriginalImageFormat();

        $this->addMediaConversion('blade')
            ->fit('stretch',self::$width,self::$height)
            ->performOnCollections('default')
            ->keepOriginalImageFormat();

        $this->addMediaConversion('thumb')
            ->fit('stretch',85,120)
            ->performOnCollections('gallery')
            ->keepOriginalImageFormat();

        $this->addMediaConversion('blade')
            ->fit('stretch',self::$gallery_width,self::$gallery_height)
            ->performOnCollections('gallery')
            ->keepOriginalImageFormat();
    }

}

