<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;
use Auth;
use Db;

class Author extends Model implements HasMedia
{
    use HasMediaTrait,SoftDeletes;

    public static $width =942;
    public static $height =862;

    protected $table='authors';
    protected $guarded = ['id'];
    protected $hidden = ['_token'];
    protected $dates =   ['created_at', 'updated_at', 'deleted_at'];



    public static function rules($id)
    {
        is_null($id) ? $img = 'required' : $img = 'sometimes';

        return [
            'name' => 'required',
            'slug' => 'unique:author_translations,slug,'.$id,
            'image' => $img.'|dimensions:min_width='.self::$width.',min_height='.self::$height.'|max:10000',
            'site_url' => 'nullable',
        ];
    }

    public static function parameterRules()
    {
        return [
            'image' => 'dimensions:min_width='.self::$width.',min_height='.self::$height.'|max:10000',
        ];
    }


    public static $messages = [
        'image.required' => "Şəkil əlavə olunmayıb",
        'image.dimensions' => 'Şəkilin şəkilin ölçüsü :min_width x :min_height nisbətdə olmalıdır.',
    ];


    public function getCreatedAtAttribute($value)
    {
        return filterDate($value, true, 'eFull');
    }


    public function getUpdatedAtAttribute($value)
    {
        return filterDate($value, true, 'eFull');
    }

    public function relatedAuthors()
    {
        return $this->hasMany('App\Models\AuthorTranslation', 'author_id');
    }


    public function trans()
    {
        return $this->hasOne(AuthorTranslation::class, 'author_id');
    }


    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('thumb')
            ->fit('stretch',250,228)
            ->performOnCollections('default')
            ->keepOriginalImageFormat();

        $this->addMediaConversion('blade')
            ->fit('stretch',self::$width,self::$height)
            ->performOnCollections('default')
            ->keepOriginalImageFormat();



    }
}

