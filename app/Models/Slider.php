<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;


class Slider extends Model implements HasMedia
{
    use HasMediaTrait, SoftDeletes;

    protected $guarded = ['id'];
    protected $hidden = ['_token'];
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];

    public static $width =1170;
    public static $height =340;

    public function getCreatedAtAttribute($value)
    {
        return filterDate($value, true, 'eFull');
    }

    public function getUpdatedAtAttribute($value)
    {
        return filterDate($value, true, 'eFull');
    }

    public static function rules($required,$parent_id = null){
        self::$width = 1170;
        self::$height = 340;
        return [
            'title' => 'nullable',
            'summary' => 'nullable',
            'image' => $required.'dimensions:min_width='.self::$width.',min_height='.self::$height,
        ];
    }

    public static $messages = [
        'image.required' => "Şəkil əlavə edilməyib.",
    ];


    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('thumb')
            ->width(300)
            ->performOnCollections('default')
            ->keepOriginalImageFormat();   //for datatable

        $this->addMediaConversion('view')
            ->fit('stretch',self::$width,self::$height)
            ->performOnCollections('default')
            ->keepOriginalImageFormat();//for blade


    }
}
