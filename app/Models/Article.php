<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\Models\Media;
use Auth;
use Db;

class Article extends Model implements HasMedia
{

    use HasMediaTrait, SoftDeletes;

    protected $guarded = ['id'];
    protected $hidden = ['_token'];
    public static $type ='image';
    public static $width =720;
    public static $height =525;

    public static $gallery_width =350;
    public static $gallery_height =270;
    public static $gallery_thumb_width =270;
    public static $gallery_thumb_height =210;

    public static function rules($id){

        is_null($id) ? $img = 'required' : $img = 'sometimes';
        return [
            'name' => "required",
            'slug' => 'unique:article_translations,slug,'.$id,
            'image' => $img.'|mimes:jpeg,jpg,png,pdf|max:10000|dimensions:min_width='.self::$width.',min_height='.self::$height,
            'page_id' => 'required|exists:page_translations,id',
            'published_at' => 'required|date',
            'status' => 'boolean',
            'featured' => 'boolean',
            'summary' => 'nullable',
            'content' => 'nullable',
        ];
    }

    public static function ReportRules($id){

        is_null($id) ? $img = 'required' : $img = 'sometimes';
        return [
            'name' => "required",
            'image' => $img.'|mimes:pdf|max:100000',
            'page_id' => 'required|exists:page_translations,id',
            'published_at' => 'required|date',
        ];
    }

    public function setMetaKeywordsAttribute($value) {

        $keywords = null;

        if(!is_null($value) && is_array($value))
        {
            $keywords = implode(",", $value);
        }

        $this->attributes['meta_keywords'] = $keywords;
    }

    public static function parameterRules()
    {

        return [
            'published_at' => 'required|date',
            'status' => 'boolean',
            'featured' => 'boolean',
            'image' => 'sometimes|mimes:jpeg,jpg,png|max:10000|dimensions:min_width='.self::$width.',min_height='.self::$height,
        ];
    }

    public static function ReportParameterRules()
    {

        return [
            'published_at' => 'required|date',
            'image' => 'sometimes|mimes:pdf|max:10000',
        ];
    }

    public static $messages = [
        'image.required' => "Şəkil əlavə olunmayıb",
        'page_id.required' => 'Kateqoriya seçilməyib'
    ];


    public function getCreatedAtAttribute($value)
    {
        return filterDate($value, true, 'eFull');
    }


    public function getUpdatedAtAttribute($value)
    {
        return filterDate($value, true, 'eFull');
    }

    public function setPublishedByAttribute($value)
    {
        $this->attributes['published_by'] = Auth::guard('admin')->user()->name;
    }

    public function scopeSelectByYear($query, $year)
    {
        if(!is_null($year)){
            return $query->where(DB::raw('YEAR(articles.published_at)'), $year);
        }
        else{
            return $query;
        }
    }

    public function relatedPages()
    {
        return $this->hasMany('App\Models\ArticleTranslation', 'article_id', 'id');
    }

    public function trans()
    {
        return $this->hasOne(ArticleTranslation::class, 'article_id')->orderBy('article_translations.id', 'asc');
    }

    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('thumb')
            ->fit('stretch',160,60)
            ->performOnCollections('default')
            ->keepOriginalImageFormat();

        $this->addMediaConversion('blade')
            ->fit('stretch',self::$width,self::$height)
            ->performOnCollections('default')
            ->keepOriginalImageFormat();

        $this->addMediaConversion('thumb')
            ->fit('stretch',self::$gallery_thumb_width,self::$gallery_thumb_height)
            ->performOnCollections('gallery')
            ->keepOriginalImageFormat();

        $this->addMediaConversion('blade')
            ->fit('stretch',self::$gallery_width,self::$gallery_height)
            ->performOnCollections('gallery')
            ->keepOriginalImageFormat();

    }

}

