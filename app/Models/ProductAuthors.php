<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductAuthors extends Model
{
    public $timestamps = false;
    protected $guarded = ['id', '_token'];
}
