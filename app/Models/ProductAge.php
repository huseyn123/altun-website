<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductAge extends Model
{
    protected $guarded = ['id'];
    protected $hidden = ['_token'];

    public static function rules($required = 'required|')
    {
        return [
            'age' => 'required',
        ];
    }

    public static $messages = [
        'age.required' => "Məlumat doldurulmayıb",
    ];


    public function getCreatedAtAttribute($value)
    {
        return filterDate($value, true, 'eFull');
    }


    public function getUpdatedAtAttribute($value)
    {
        return filterDate($value, true, 'eFull');
    }
}
