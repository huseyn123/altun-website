<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $hidden = ['_token'];
    protected $dates = ['created_at', 'updated_at'];

}
