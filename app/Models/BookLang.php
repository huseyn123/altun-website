<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BookLang extends Model
{
    protected $table = 'book_langs';
    protected $guarded = ['id'];
    protected $dates = ['created_at', 'updated_at'];

    public static function rules()
    {
        return [
            'title_az' => 'required',
        ];
    }

    public static $messages = [
        'title_az.required' => 'Ad az daxil edilməyib',
    ];


}
