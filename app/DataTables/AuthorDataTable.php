<?php

namespace App\DataTables;

use App\Models\Author;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;

class AuthorDataTable extends DataTable
{
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable
            ->editColumn('page_name', function($row) {
                if(is_null($row->page_id)){
                    return '<span class="text text-danger">Silinib və ya mövcud deyil!</span>';
                }
                elseif(is_null($row->deleted_page)){
                    return $row->page_name;
                }
                else{
                    return '<span class="text text-danger" style="text-decoration: line-through">'.$row->page_name.'</span>';
                }
            })
            ->editColumn('image', function($row) {
                if(!is_null($row->getFirstMedia())){
                    if(substr($row->getFirstMedia()->getFullUrl(), -3) == 'svg'){
                        $url = $row->getFirstMedia()->getFullUrl();
                        $style = 'width:100px;height:100px';
                    }else{
                        $url = $row->getFirstMedia()->getUrl('thumb');
                        $style="max-width:100%";
                    }

                    $img = '<div class="input-group"><img src="'.asset($url).'" style="'.$style.'"></div>';

                    return '<img src="'.asset($url).'" style="'.$style.'">';
                }
            })
            ->rawColumns(['action', 'page_name','image'])
            ->addColumn('action', function($row) {
                return view( 'widgets.action-page', [
                    'editRoute' => 'authors',
                    'route' => 'authors',
                    'row' => $row,
                    'softDelete' => true,
                    'forceDelete' => true,
                    'largeModal' => true,
                ])->render();
            });
    }

    public function query(Author $model)
    {
        $query = $model->newQuery()
            ->join('author_translations as at', 'at.author_id', '=', 'authors.id')
            ->leftJoin('page_translations as pt', 'pt.id', '=', 'at.page_id')
            ->leftJoin('pages', 'pages.id', '=', 'pt.page_id')
            ->where('pages.template_id',5)
            ->with(['media'])
            ->with(['media'])
            ->select(
                'at.*',
                'at.id as tid',
                'authors.id',
                'pt.name as page_name',
                'pt.lang','pt.deleted_at as deleted_page','pages.template_id'
            );

        if ($this->request()->has('lang') && $this->request()->get('lang') != 'all') {
            $query->where('at.lang', $this->request()->get('lang'));
        }


        if($this->request()->get('type') == 1){
            $query->whereNull('at.deleted_at');
        }
        elseif($this->request()->get('type') == 2){
            $query->whereNotNull('at.deleted_at');
        }
        return $query;
    }


    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '140px', 'title' => ''])
            ->parameters($this->getBuilderParameters());
    }


    protected function getColumns()
    {
        return [
            ['data' => 'id', 'name' => 'at.id', 'title' => 'ID', 'visible' => false, 'searchable' => false],
            ['data' => 'image', 'name' => 'media.image', 'title' => 'Şəkil', 'orderable' => false,'searchable' => false],
            ['data' => 'name', 'name' => 'at.name', 'title' => 'Ad','searchable' => true],
            ['data' => 'page_name', 'name' => 'pt.name', 'title' => 'Kateqoriya'],
            ['data' => 'lang', 'name' => 'at.lang', 'title' => 'Dil', 'searchable' => false,'orderable' => false],
            ['data' => 'created_at', 'name' => 'at.created_at', 'title' => 'Yaradıldı','orderable' => false, 'class' => 'none', 'searchable' => false],
            ['data' => 'updated_at', 'name' => 'at.updated_at', 'title' => 'Yenilənib','orderable' => false, 'class' => 'none', 'searchable' => false],
        ];
    }


    protected function getBuilderParameters()
    {
        return [
            'responsive' => true,
            'order' => [ [0,'desc'] ],
            'lengthMenu' => [10,25,50],
            'language' => [
                'url' => url('lang.json'),
            ],
            'drawCallback' => 'function() {
                $("#authors_length").prependTo($("#dataTables_length_box"));
            }',
        ];
    }
}
