<?php

namespace App\DataTables;

use App\Models\Config;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;

class ConfigDataTable extends DataTable
{
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable
            ->editColumn('config_path', function($row) {
                if(!is_null($row->config_path)){
                    $path = explode('.', $row->config_path);
                    return 'config/'.$path[0].'.php';
                }
            })
            ->addColumn('route', function($row) {
                return $row->config_path;
            })
            ->addColumn('action', function($row) {
                return view( 'widgets.action-dt', ['row' => $row, 'route' => 'config'])->render();
            })
            ->rawColumns(['action']);
    }


    public function query(Config $model)
    {
        $query = $model->newQuery();

        return $query;
    }


    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '50px', 'title' => ''])
            ->parameters($this->getBuilderParameters());
    }


    protected function getColumns()
    {
        return [
            ['data' => 'id', 'name' => 'settings.id', 'title' => 'ID', 'visible' => false, 'searchable' => false],
            ['data' => 'key', 'name' => 'settings.key', 'title' => 'Açar söz', 'orderable' => false],
            ['data' => 'config_path', 'name' => 'settings.config_path', 'title' => 'Fayl'],
            ['data' => 'route', 'name' => 'settings.config_path', 'title' => 'Route'],
            ['data' => 'value', 'name' => 'settings.value', 'title' => 'Dəyər', 'orderable' => false],
            ['data' => 'created_at', 'name' => 'settings.created_at', 'title' => 'Yaradıldı','orderable' => false, 'searchable' => false, 'class' => 'none'],
            ['data' => 'updated_at', 'name' => 'settings.updated_at', 'title' => 'Yenilənib','orderable' => false, 'searchable' => false],
        ];
    }



    protected function getBuilderParameters()
    {
        return [
            'processing' => true,
            'responsive' => true,
            'filter' => true,
            'order' => [ [0,'desc'] ],
            'lengthMenu' => [10],
            'lengthChange' => false,
            'language' => [
                'url' => url('lang.json'),
            ]
        ];
    }
}
