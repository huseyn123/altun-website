<?php

namespace App\DataTables;

use App\Models\Slider;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;

class SliderDataTable extends DataTable
{
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable
            ->editColumn('image', function($slider) {
                if(substr($slider->getFirstMedia()->getFullUrl(), -3) == 'svg'){
                    $url = $slider->getFirstMedia()->getFullUrl();
                    $style = 'width:300px';
                }else{
                    $url = $slider->getFirstMedia()->getUrl('thumb');
                    $style="";
                }
                return '<img src="'.asset($url).'"  style="'.$style.'">';
            })
            ->editColumn('template_id', function($post) {
                return config("config.slider_category.$post->template_id");
            })
            ->addColumn('action', function($row) {
                return view( 'widgets.action-dt', ['route' => 'slider', 'row' => $row, 'softDelete' => true, 'forceDelete' => true])->render();
            })
            ->rawColumns(['image', 'action']);
    }



    public function query(Slider $model)
    {
        $query = $model->newQuery()->with('media');

        if ($this->request()->has('lang') && $this->request()->get('lang') != 'all') {
            $query->where('sliders.lang', $this->request()->get('lang'));
        }

        if($this->request()->get('type') == 0){
            $query->withTrashed();
        }
        elseif($this->request()->get('type') == 2){
            $query->onlyTrashed();
        }
        return $query;
    }



    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '90px', 'orderable' => false, 'searchable' => false, 'title' => ' '])
            ->parameters($this->getBuilderParameters());
    }



    protected function getColumns()
    {
        return [
            ['data' => 'id', 'name' => 'sliders.id', 'title' => 'ID', 'visible' => false, 'searchable' => false],
            ['data' => 'image', 'name' => 'media.image', 'title' => 'Şəkil', 'searchable' => false, 'orderable' => false],
            ['data' => 'lang', 'name' => 'sliders.lang', 'title' => 'Dil', 'searchable' => false, 'orderable' => false],
            ['data' => 'template_id', 'name' => 'sliders.template_id','title' => 'Kateqoriya', 'searchable' => false],
            ['data' => 'created_at', 'name' => 'sliders.created_at', 'title' => 'Yaradıldı','searchable' => false],
            ['data' => 'updated_at', 'name' => 'sliders.updated_at', 'title' => 'Yenilənib','orderable' => false, 'searchable' => false, 'class' => 'none'],
        ];
    }



    protected function getBuilderParameters()
    {
        return [
            'processing' => false,
            'responsive' => true,
            'filter' => true,
            'order' => [ [0,'desc'] ],
            'lengthMenu' => [5,10],
            'language' => [
                'url' => url('lang.json'),
            ]
        ];
    }


    protected function filename()
    {
        return 'sliderdatatable_' . time();
    }
}
