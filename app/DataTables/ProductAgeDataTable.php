<?php

namespace App\DataTables;

use App\Models\ProductAge;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;

class ProductAgeDataTable extends DataTable
{
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable
            ->addColumn('action', function($row) {
                return view( 'widgets.action-dt', ['row' => $row, 'route' => 'productAge', 'forceDelete' => true])->render();
            })
            ->rawColumns(['age','action']);
    }

    public function query(ProductAge $model)
    {
        $query = $model->newQuery();

        return $query;
    }


    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '100px', 'title' => ''])
            ->parameters($this->getBuilderParameters());
    }


    protected function getColumns()
    {
        return [
            ['data' => 'id', 'name' => 'product_ages.id', 'title' => 'ID', 'visible' => false, 'searchable' => false],
            ['data' => 'age', 'name' => 'product_ages.age','title' => 'Yaş', 'orderable' => true],
            ['data' => 'created_at', 'name' => 'product_ages.created_at','title' => 'Yaradıldı', 'searchable' => false],
            ['data' => 'updated_at', 'name' => 'product_ages.updated_at', 'title' => 'Yenilənib', 'searchable' => false],
        ];
    }

    protected function getBuilderParameters()
    {
        return [
            'processing' => true,
            'responsive' => true,
            'order' => [ [0,'desc'] ],
            'lengthMenu' => [10,25],
            'language' => [
                'url' => url('lang.json'),
            ]
        ];
    }


    protected function filename()
    {
        return 'productAgesdatatable_' . time();
    }

}
