<?php

namespace App\DataTables;

use App\Models\BookLang;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use DB;

class BookLangDataTable extends DataTable
{
    protected $route;

    public function route($route) {
        $this->route = $route;
        return $this;
    }

    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable
            ->rawColumns(['action'])
            ->addColumn('action', function($row) {
                return view( 'widgets.action-dt', [
                    'route' => 'book_lang',
                    'row' => $row,
                    'forceDelete' => true,
                    'largeModal'  => true
                ])->render();
            });
    }



    public function query(BookLang $model)
    {
        $query = $model->newQuery();
        return $query;
    }



    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '90px', 'orderable' => false, 'searchable' => false, 'title' => ' '])
            ->parameters($this->getBuilderParameters());
    }



    protected function getColumns()
    {
        return [
            ['data' => 'id', 'name' => 'book_langs.id', 'title' => 'ID', 'visible' => false, 'searchable' => false],
            ['data' => 'title_az', 'name' => 'book_langs.title_az', 'title' => 'Ad az'],
            ['data' => 'created_at','name' => 'book_langs.created_at', 'title' => 'Yaradıldı','orderable' => false, 'searchable' => false],
            ['data' => 'updated_at','name' => 'book_langs.updated_at', 'title' => 'Yenilənib','orderable' => false, 'searchable' => false],
        ];
    }



    protected function getBuilderParameters()
    {
        return [
            'processing' => false,
            'responsive' => true,
            'filter' => true,
            'order' => [ [0,'desc'] ],
            'lengthMenu' => [5,10],
            'language' => [
                'url' => url('lang.json'),
            ]
        ];
    }


    protected function filename()
    {
        return 'book_langsdatatable_' . time();
    }
}
