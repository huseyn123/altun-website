<?php

namespace App\DataTables;

use App\Models\Subscriber;
use Yajra\DataTables\Services\DataTable;
use App\Logic\userAction;
use Yajra\DataTables\EloquentDataTable;
class SubscriberDataTable extends DataTable
{
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable
            ->editColumn('status', function($row) {
                return '<span class="text-'.config("config.alert.$row->status").' text-bold">'.config("config.status.$row->status").'</span>';
            })
            ->rawColumns(['status', 'action'])
            ->addColumn('action', 'widgets.action-subscriber');
    }



    public function query(Subscriber $model)
    {
        $query = $model->newQuery();

        /*if($this->pending == true){
            $query->where('status', 0);
        }*/

        return $query;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->addAction(['width' => '80px', 'title' => ''])
                    ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ['data' => 'id', 'name' => 'subscribers.id', 'title' => 'ID', 'visible' => false, 'searchable' => false],
            ['data' => 'email', 'name' => 'subscribers.email','title' => 'Email'],
            ['data' => 'status', 'name' => 'subscribers.status','title' => 'Status', 'searchable' => false],
            ['data' => 'created_at', 'name' => 'subscribers.created_at','title' => 'Yaradıldı', 'searchable' => false]
        ];
    }



    protected function getBuilderParameters()
    {
        return [
            'processing' => true,
            'responsive' => true,
            'filter' => true,
            'order' => [ [0,'desc'] ],
            'lengthMenu' => [25,50,100],
            'dom' => '<"col-md-4 hidden-xs pull-left"<"row"<"pull-left"l><"pull-left"<"col-md-12"B>>>><"pull-right col-md-8 custom-filter"f>rt<"col-md-3 col-sm-3"<"row"i>><"col-md-9"<"row"p>>',
            'buttons' => ['excel'],
            'language' => [
                'url' => url('lang.json'),
            ]
        ];
    }



    protected function filename()
    {
        return 'subscriberdatatable_' . time();
    }
}
