<?php

namespace App\DataTables;

use Spatie\MediaLibrary\Models\Media;
use Yajra\DataTables\Services\DataTable;

class GalleryDataTable extends DataTable
{
    protected $data,$gallery_type;

    public function data($data,$gallery_type) {
        $this->data = $data;
        $this->gallery_type = $gallery_type;
        return $this;
    }

    public function dataTable($query)
    {
        return datatables($query)
            ->editColumn('file_name', function($row) {
                return '<img src="'.asset($row->getUrl('thumb')).'">';
            })
            ->editColumn('size', function($row) {
                return $row->human_readable_size;
            })
            ->addColumn('action', function($row) {
                return '<a href="#" class="btn btn-xs btn-danger" data-action="'.route("gallery.destroy", $row->id).'" data-toggle="modal" data-target="#modal-confirm"><i class="fa fa-remove"></i></a>';
            })
            ->rawColumns(['action', 'file_name']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Media $model)
    {
        $query = $this->data->getMedia($this->gallery_type);

        return $query;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->addAction(['width' => '80px', 'title' => ''])
                    ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            ['data' => 'id', 'name' => 'id', 'title' => 'ID', 'visible' => false],
            ['data' => 'file_name', 'name' => 'file_name', 'title' => 'Şəkil', 'searchable' => false, 'orderable' => false],
            ['data' => 'name', 'name' => 'name', 'title' => 'Faylın adı', 'searchable' => false, 'orderable' => false],
            ['data' => 'size', 'name' => 'size', 'title' => 'Faylın ölçüsü', 'searchable' => false, 'orderable' => false],
            ['data' => 'created_at', 'name' => 'created_at', 'title' => 'Yaradıldı', 'searchable' => false, 'exportable' => false],
        ];
    }


    protected function getBuilderParameters()
    {
        return [
            'responsive' => true,
            'order' => [ [0,'asc'] ],
            'lengthMenu' => [20],
            'lengthChange' => false,
            'filter' => false,
            'language' => [
                'url' => url('lang.json'),
            ]
        ];
    }


    protected function filename()
    {
        return 'Albums_' . date('YmdHis');
    }


}
