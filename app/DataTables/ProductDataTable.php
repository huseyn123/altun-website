<?php

namespace App\DataTables;

use App\Models\Product;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;

class ProductDataTable extends DataTable
{

    protected $actions = ['excel', 'print'];

    protected $route;

    public function route($route) {
        $this->route = $route;
        return $this;
    }

    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable

            ->editColumn('tag', function($row) {
                $tags = [];
                foreach ($row->productTags as $tag){

                    $tags[] = $tag->keyword;
                }

                return implode(', ', $tags);
            })
            ->editColumn('page_name', function($row) {
                if(is_null($row->page_id)){
                    return '<span class="text text-danger">Silinib və ya mövcud deyil!</span>';
                }
                elseif(is_null($row->deleted_page)){
                    return $row->page_name;
                }
                else{
                    return '<span class="text text-danger" style="text-decoration: line-through">'.$row->page_name.'</span>';
                }
            })
            ->editColumn('image', function($row) {
                if(!is_null($row->getFirstMedia())){
                    if(substr($row->getFirstMedia()->getFullUrl(), -3) == 'svg'){
                        $url = $row->getFirstMedia()->getFullUrl();
                        $style = 'width:100px;height:100px';
                    }else{
                        $url = $row->getFirstMedia()->getUrl('thumb');
                        $style="max-width:100%";
                    }

                    $img = '<div class="input-group"><img src="'.asset($url).'" style="'.$style.'"></div>';

                    return '<img src="'.asset($url).'" style="'.$style.'">';
                }
            })
            ->with('status', encode(config("config.status")))
            ->rawColumns(['status', 'image', 'name', 'slug', 'price', 'action','page_name'])
            ->addColumn('action', function($row) {
                return view( 'widgets.action-product', [
                    'route' => 'product',
                    'editRoute' => $this->route,
                    'row' => $row,
                    'softDelete' => true,
                    'forceDelete' => true,
                    'album' => true,
                    'largeModal' => true
                ])->render();
            });
    }


    public function query(Product $model)
    {
        $query = $model->newQuery()
            ->join('product_translations as prt', 'prt.product_id', '=', 'products.id')
            ->leftJoin('page_translations as pt', 'pt.id', '=', 'prt.page_id')
            ->leftJoin('pages', 'pages.id', '=', 'pt.page_id')
            ->where('pages.template_id',6)
            ->with(['media'])
            ->select('prt.*', 'prt.id as tid',
                'products.id','products.price','products.size','products.status','products.bar_code','products.page_number','products.age_id',
                'pt.name as page_name',
                'pt.lang','pt.deleted_at as deleted_page','pages.template_id')
            ->with(['productTags']);


        if ($this->request()->has('lang') && $this->request()->get('lang') != 'all') {
            $query->where('prt.lang', $this->request()->get('lang'));
        }

        if ($this->request()->has('tag') && $this->request()->get('tag') != 'all') {
            $tag = $this->request()->get('tag');

            $query->whereHas('productTagsSearch',function($query) use($tag){
                $query->where("keyword", $tag);
            });
        }

        if ($this->request()->has('category') && $this->request()->get('category') != 'all') {
            $query->where('prt.page_id', $this->request()->get('category'));
        }


        if($this->request()->get('type') == 1){
            $query->whereNull('prt.deleted_at');
        }
        elseif($this->request()->get('type') == 2){
            $query->whereNotNull('prt.deleted_at');
        }

        return $query;
    }


    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->addAction(['width' => '140px', 'title' => ''])
            ->parameters($this->getBuilderParameters());
    }


    protected function getColumns()
    {
        return [
            ['data' => 'id', 'name' => 'prt.id', 'title' => 'ID', 'visible' => false, 'searchable' => false],
            ['data' => 'name', 'name' => 'prt.name', 'title' => 'Ad', 'orderable' => false],
            ['data' => 'slug', 'name' => 'prt.slug', 'title' => 'Slug', 'orderable' => false, 'searchable' => false, 'exportable' => false, 'printable' => false],
            ['data' => 'page_name', 'name' => 'pt.name', 'title' => 'Kateqoriya'],
            ['data' => 'bar_code', 'name' => 'products.bar_code', 'title' => 'Bar Kod', 'orderable' => false],
//            ['data' => 'status', 'name' => 'products.status', 'title' => 'Status', 'searchable' => false],
            ['data' => 'price', 'name' => 'products.price', 'title' => 'Qiymət ₼', 'searchable' => false],
            ['data' => 'image', 'name' => 'media.image', 'title' => 'Şəkil', 'orderable' => false,'class' => 'none','searchable' => false],
            ['data' => 'tag', 'name' => 'tags.keyword', 'title' => 'Teqlər', 'orderable' => false, 'class' => 'none', 'searchable' => false, 'exportable' => false, 'printable' => false],
            ['data' => 'created_at', 'name' => 'prt.created_at', 'title' => 'Yaradıldı','orderable' => false, 'class' => 'none', 'searchable' => false],
            ['data' => 'updated_at', 'name' => 'prt.updated_at', 'title' => 'Yenilənib','orderable' => false, 'class' => 'none', 'searchable' => false],
        ];
    }


    protected function getBuilderParameters()
    {
        return [
            'responsive' => true,
            'order' => [ [0,'desc'] ],
            'lengthMenu' => [10,25,50],
            'language' => [
                'url' => url('lang.json'),
            ],
            'drawCallback' => 'function() {
                $("#products_length").prependTo($("#dataTables_length_box"));
            }',
        ];
    }



}
