<?php

namespace App\Crud;

use App\Models\Page;
use App\Logic\MultiLanguageSelect;

class ArticleCrud extends RenderCrud
{
    private function category($lang)
    {
        if(is_null($lang )){

            $select = Page::join('page_translations as pt', 'pt.page_id', '=', 'pages.id')
                ->whereIn('pages.template_id',[3,22])
                ->whereNull('pt.deleted_at')
                ->select("pt.id", "pt.name", 'pt.lang')
                ->orderBy("pt.name", "asc")
                ->get();

            $query = MultiLanguageSelect::multiLang($select);

        }
        else{

            $query = Page::join('page_translations', 'page_translations.page_id', '=', 'pages.id')
                ->select('page_translations.id', 'page_translations.name', 'page_translations.lang')
                ->where('page_translations.lang', $lang)
                ->whereIn('pages.template_id',[3,22])
                ->whereNull('page_translations.deleted_at')
                ->orderBy('page_translations.name', 'asc')
                ->pluck('page_translations.name', 'page_translations.id');
        }

        return $query;
    }



    public function fields($lang, $action, $data = null)
    {
        $fields = [
            [
                "label" => 'Başlıq',
                "db" => "name",
                "type" => 'text',
                "attr" => ['class'=>'form-control', 'required']
            ],
            [
                "label" => "Slug (URL)",
                "db" => "slug",
                "type" => "text",
                "attr" => ['class'=>'form-control', 'autocomplete' => 'off', 'title'=>"Boş saxladığınız təqdirdə sluq avtomatik yaradılacaq."],
            ],
            [
                "label" => "Kateqoriya",
                "db" => "page_id",
                "type" => "select",
                "data" => $this->category($lang),
                "selected" => null,
                "attr" => ['class'=>'form-control']
            ],
            [
                "label" => "Qısa məzmun",
                "db" => "summary",
                "type" => "textarea",
                "attr" => ['class'=>'form-control', 'rows' => 5, 'required'],
            ],
            [
                "label" => "Məzmun",
                "db" => "content",
                "type" => "textarea",
                "attr" => ['class'=>'form-control ckeditor', 'id' => 'editor'.$lang]
            ],
            [
                "label" => "Meta description",
                "db" => "meta_description",
                "type" => "textarea",
                "hide" => [2],
                "attr" => ['class'=>'form-control', 'maxlength' => 160, 'rows' => 3, 'title' => 'Maksimum simvol sayı 160 olmalıdır.Sosial şəbəkələrdə paylaşılan zaman məhz bu yazı linkin altında göstəriləcək']
            ],
            [
                "label" => "Meta keywords",
                "db" => "meta_keywords[]",
                "type" => "select",
                "data" => [],
                "selected" => null,
                "hide" => [2],
                "attr" => ['class'=>'form-control meta_keywords', 'id'  => 'meta_keywords'.$lang, 'multiple' => 'multiple', 'autocomplete' => 'off', 'title' => 'Keyword-ləri bir birindən ayırmaq üçün Enter düyməsini sıxın.'],
            ],
        ];

        if(is_null($lang)){
            $paramFields = (new ArticleParameterCrud())->fields('get');
            return $this->render(array_merge($fields, $paramFields), $action, $data);
        }
        else{
            return $this->render($fields, $action, $data);
        }
    }
}


