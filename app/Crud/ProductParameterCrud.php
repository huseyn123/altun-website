<?php

namespace App\Crud;

use App\Models\ProductAge;
use App\Models\Tag;
use phpDocumentor\Reflection\DocBlock\Tags\Author;

class ProductParameterCrud extends RenderCrud
{


    private function tag()
    {
        return Tag::groupBy('keyword')->pluck('keyword', 'id');
    }

    private function age()
    {
        return ProductAge::groupBy('age')->pluck('age', 'id')->prepend('---', '');;
    }


    private function productTags($data)
    {
        if($data != false){
            return $data->tags->pluck('tag_id')->toArray();
        }
        else{
            return null;
        }
    }

    public function fields($action, $data = false)
    {
        $fields = [
            [
                "label" => "Şəkil",
                "db" => "image",
                "type" => "file",
                "attr" => ['class'=>'form-control image', 'style' => 'display:none','title' => 'Ölçü: 1100x1600'],
                "design" => function($input, $data, $title = ' Əlavə et'){
                    if($data != false && $data->getFirstMedia()){
                        if(substr($data->getFirstMedia()->getFullUrl(), -3) == 'svg'){
                            $url = $data->getFirstMedia()->getFullUrl();
                            $style = 'width:100px;height:100px';
                        }else{
                            $url = $data->getFirstMedia()->getUrl('thumb');
                            $style="max-width:100%";
                        }

                        $img = '<div class="input-group"><img src="'.asset($url).'" style="'.$style.'"></div>';
                    }
                    else{
                        $img = '';
                    }
                    $group_btn =
                        '<label class="input-group-btn">
                        <span class="btn btn-primary">
                            <i class="fa fa-cloud-upload"></i>'.$title.$input.'
                        </span>
                    </label>
                    <input type="text" class="form-control" readonly="" required>
                    <div class="divImage" style="display:none">
                        <img class="showImage" src="#">
                    </div>';

                    return '<div class="input-group">'.$group_btn.'</div><br>'.$img;
                },
            ],
            [
                "label" => "Yaş qrupu",
                "db" => "age_id",
                "type" => "select",
                "data" => $this->age(),
                "selected" => null,
                "attr" => ['class'=>'form-control']
            ],
            [
                "label" => 'Qiymət',
                "db" => "price",
                "type" => 'text',
                "attr" => ['class'=>'form-control', 'min' => 1],
            ],
            [
                "label" => 'Kitabın ölçüləri',
                "db" => "size",
                "type" => 'text',
                "attr" => ['class'=>'form-control', 'min' => 1],
            ],
            [
                "label" => 'Kitabın Kodu',
                "db" => "bar_code",
                "type" => 'text',
                "attr" => ['class'=>'form-control', 'min' => 1],
            ],
            [
                "label" => 'Səhifələrin sayı',
                "db" => "page_number",
                "type" => 'text',
                "attr" => ['class'=>'form-control', 'min' => 1],
            ],
            [
                "label" => 'Online Sifariş Linki',
                "db" => "order_link",
                "type" => 'text',
                "attr" => ['class'=>'form-control', 'min' => 1],
            ],
            [
                "label" => "Teq",
                "db" => "tag[]",
                "type" => "select",
                "data" => $this->tag(),
                "selected" => $this->productTags($data),
                "attr" => ['class'=>'form-control select-multiple', 'multiple' => 'multiple','data-create' => 1,  'title' => 'Teqləri bir birindən ayırmaq üçün Enter düyməsini sıxın.'],
            ],
            [
                "label" => "Ən yenilər",
                "db" => "featured",
                "type" => "checkbox",
                "value" => 1,
                "checked" => false,
                "attr" => []
            ],
        ];


        return $this->render($fields, $action, $data);
    }
}


