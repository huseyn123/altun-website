<?php

namespace App\Crud;

use App\Models\Article;

class ArticleParameterCrud extends RenderCrud
{
    public function fields($action, $data = false)
    {
        $fields = [
            [
                "label" => "Şəkil",
                "db" => "image",
                "type" => "file",
                "attr" => ['class'=>'form-control image', 'style' => 'display:none', 'title' => 'Ölçü: '.Article::$width.'x'.Article::$height],
                "design" => function($input, $data, $title = ' Əlavə et'){
                    if($data != false && $data->getFirstMedia()->getUrl('thumb')){
                        $img = '<div class="input-group"><img src="'.asset($data->getFirstMedia()->getUrl('thumb')).'" style="max-width:100%"></div>';
                    }
                    else{
                        $img = '';
                    }
                    $group_btn =
                        '<label class="input-group-btn">
                        <span class="btn btn-primary">
                            <i class="fa fa-cloud-upload"></i>'.$title.$input.'
                        </span>
                    </label>
                    <input type="text" class="form-control" readonly="" required>
                    <div class="divImage" style="display:none">
                        <img class="showImage" src="#">
                    </div>';

                    return '<div class="input-group">'.$group_btn.'</div><br>'.$img;
                },
            ],
            [
                "label" => "Tarix",
                "db" => "published_at",
                "type" => "text",
                "attr" => ['class'=>'form-control datepicker', 'autocomplete' => 'off', 'required']
            ],
            [
                "label" => "Status",
                "db" => "status",
                "type" => "select",
                "data" => config('config.article-status'),
                "selected" => 1,
                "attr" => ['class'=>'form-control']
            ],
            [
                "label" => "Ana Səhifədə Görünsün",
                "db" => "show_index",
                "type" => "checkbox",
                "value" => 1,
                "checked" => false,
                "attr" => []
            ],
        ];

        return $this->render($fields, $action, $data);
    }
}


