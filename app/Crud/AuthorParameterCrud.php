<?php

namespace App\Crud;

class AuthorParameterCrud extends RenderCrud
{
    public function fields($action, $data = false)
    {
        $fields = [
            [
                "label" => "Şəkil",
                "db" => "image",
                "type" => "file",
                "hide" =>[1,4],
                "attr" => ['class'=>'form-control image', 'style' => 'display:none','title' => 'Ölçü: 942x862'],
                "design" => function($input, $data, $title = ' Əlavə et'){

                    if($data != false && $data->getFirstMedia()){
                        if(substr($data->getFirstMedia()->getFullUrl(), -3) == 'svg'){
                            $url = $data->getFirstMedia()->getFullUrl();
                            $style = 'width:100px;height:100px';
                        }else{
                            $url = $data->getFirstMedia()->getUrl('thumb');
                            $style="max-width:100%";
                        }
                        $img = '<div class="input-group"><img src="'.asset($url).'" style="'.$style.'"></div>';
                    }
                    else{
                        $img = '';
                    }
                    $group_btn =
                        '<label class="input-group-btn">
                        <span class="btn btn-primary">
                            <i class="fa fa-cloud-upload"></i>'.$title.$input.'
                        </span>
                    </label>
                    <input type="text" class="form-control with_image" readonly="">';

                    return '<div class="input-group">'.$group_btn.'</div><br>'.$img;
                }
            ],
        ];

        return $this->render($fields, $action, $data);
    }
}


