<?php

namespace App\Crud;

class ProductAgeCrud extends RenderCrud
{


    public function fields($action, $data = false)
    {
        $fields = [
            [
                "label" => 'Yaş',
                "db" => "age",
                "type" => 'text',
                "attr" => ['class'=>'form-control']
            ]
        ];

        return $this->render($fields, $action, $data);
    }
}


