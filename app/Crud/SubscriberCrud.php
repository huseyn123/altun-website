<?php

namespace App\Crud;

class SubscriberCrud extends RenderCrud
{
    public function fields($action, $data = null)
    {
        $fields = [
            [
                "label" => 'Email',
                "db" => "email",
                "type" => 'text',
                "attr" => ['class'=>'form-control']
            ]
        ];
        return $this->render($fields, $action, $data);
    }
}


