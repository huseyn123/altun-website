<?php

namespace App\Crud;

use App\Logic\MultiLanguageSelect;
use App\Models\Author;
use App\Models\AuthorTranslation;
use App\Models\BookEducation;
use App\Models\BookLang;
use App\Models\Page;

class ProductCrud extends RenderCrud
{


    private function category($lang)
    {
        if(is_null($lang )){

            $select = Page::join('page_translations as pt', 'pt.page_id', '=', 'pages.id')
                ->whereIn('pages.template_id',[6])
                ->whereNull('pt.deleted_at')
                ->select("pt.id", "pt.name", 'pt.lang')
                ->orderBy("pt.name", "asc")
                ->get();

            $query = MultiLanguageSelect::multiLang($select);

        }
        else{

            $query = Page::join('page_translations', 'page_translations.page_id', '=', 'pages.id')
                ->select('page_translations.id', 'page_translations.name', 'page_translations.lang')
                ->where('page_translations.lang', $lang)
                ->whereIn('pages.template_id',[6])
                ->whereNull('page_translations.deleted_at')
                ->orderBy('page_translations.name', 'asc')
                ->pluck('page_translations.name', 'page_translations.id');
        }

        return $query;
    }

    private function product_lang($lang)
    {
        if(is_null($lang )){$lang ='az';}

        $product_lang = BookLang::select('title_'.$lang.' as name','id')->pluck('name', 'id');
        $product_lang->prepend('---', '');
        return $product_lang;
    }


    private function authors($lang)
    {
        if(is_null($lang )){$lang ='az';}
        return AuthorTranslation::where('lang',$lang)->groupBy('author_id')->pluck('name', 'id');

    }

    private function education($lang)
    {
        if(is_null($lang )){$lang ='az';}
        return BookEducation::select('title_'.$lang.' as name','id')->pluck('name', 'id');

    }




    private function productAuthors($data)
    {

        if($data != false){
            return $data->product_authors->pluck('authors_id')->toArray();
        }
        else{
            return null;
        }
    }

    private function author($lang)
    {

        if(is_null($lang )){

            $select = Author::join('author_translations as at', 'at.author_id', '=', 'authors.id')
                ->whereNull('at.deleted_at')
                ->select("at.id", "at.name", 'at.lang')
                ->orderBy("at.name", "asc")
                ->get();

            $author = MultiLanguageSelect::multiLang($select, false, true);

        }
        else{

            $author = Author::join('author_translations', 'author_translations.author_id', '=', 'authors.id')
                ->select('author_translations.id', 'author_translations.name', 'author_translations.lang')
                ->where('author_translations.lang', $lang)
                ->whereNull('author_translations.deleted_at')
                ->orderBy('author_translations.name', 'asc')
                ->pluck('author_translations.name', 'author_translations.id');
            $author->prepend('---', '');

        }

        return $author;
    }


    public function fields($lang, $action, $data = null,$route = null,$dublicate =false)
    {
        $fields = [
            [
                "label" => 'Ad',
                "db" => "name",
                "type" => 'text',
                "attr" => ['class'=>'form-control', 'required']
            ],
            [
                "label" => "Slug (URL)",
                "db" => "slug",
                "type" => "text",
                "attr" => ['class'=>'form-control', 'autocomplete' => 'off', 'title'=>"Boş saxladığınız təqdirdə sluq avtomatik yaradılacaq."],
            ],
            [
                "label" => "Kateqoriya",
                "db" => "page_id",
                "type" => "select",
                "data" => $this->category($lang),
                "selected" => null,
                "attr" => ['class'=>'form-control','required']
            ],
            [
                "label" => "Yazarlar",
                "db" => "product_authors[]",
                "type" => "select",
                "data" => $this->authors($lang),
                "selected" => $this->productAuthors($data),
                "attr" => ['class'=>'form-control select-multiple','id' => 'product_author'.$lang,'multiple' => 'multiple','data-create' => 0],
            ],
            [
                "label" => "Kitab dili",
                "db" => "product_lang",
                "type" => "select",
                "data" => $this->product_lang($lang),
                "selected" => null,
                "attr" => ['class'=>'form-control']
            ],
            [
                "label" => "Təhsil Mərhələsi ",
                "db" => "product_education[]",
                "type" => "select",
                "data" => $this->education($lang),
                "selected" => null,
                "attr" => ['class'=>'form-control select-multiple','id' => 'product_education'.$lang,'multiple' => 'multiple','data-create' => 0],
            ],
//            [
//                "label" => "Qısa məzmun",
//                "db" => "summary",
//                "type" => "textarea",
//                "attr" => ['class'=>'form-control', 'rows' => 5, 'required'],
//            ],
            [
                "label" => "Məzmun",
                "db" => "content",
                "type" => "textarea",
                "attr" => ['class'=>'form-control ckeditor', 'id' => 'editor'.$lang]
            ],
//            [
//                "label" => "Meta keywords",
//                "db" => "meta_keywords[]",
//                "type" => "select",
//                "data" => [],
//                "selected" => null,
//                "attr" => ['class'=>'form-control meta_keywords', 'id'  => 'meta_keywords'.$lang, 'multiple' => 'multiple', 'autocomplete' => 'off', 'title' => 'Keyword-ləri bir birindən ayırmaq üçün Enter düyməsini sıxın.'],
//            ],
        ];


        if(is_null($lang)){
            $paramFields = (new ProductParameterCrud())->fields('get',$data);
            return $this->render(array_merge($fields, $paramFields), $action, $data);
        }
        else{
            return $this->render($fields, $action, $data);
        }
    }
}


