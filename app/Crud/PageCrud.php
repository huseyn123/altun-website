<?php

namespace App\Crud;
use App\Logic\MultiLanguageSelect;
use App\Models\Page;
use phpDocumentor\Reflection\Types\Null_;

class PageCrud extends RenderCrud
{
    private function category($lang)
    {
        if(is_null($lang)){
            $select = Page::join('page_translations', 'page_translations.page_id', '=', 'pages.id')
                ->whereNull('page_translations.deleted_at')
                ->whereNotIn('pages.template_id',[2,5])
                ->select('page_translations.id','page_translations.name', 'page_translations.lang','page_translations.deleted_at')
                ->orderBy('page_translations.name', 'asc')
                ->get();

            $query = MultiLanguageSelect::multiLang($select, false, true);
        }
        else{
            $query = Page::join('page_translations', 'page_translations.page_id', '=', 'pages.id')
                ->select('page_translations.id', 'page_translations.name', 'page_translations.lang')
                ->where('page_translations.lang', $lang)
                ->whereNotIn('pages.template_id',[2,5])
                ->whereNull('page_translations.deleted_at')
                ->orderBy('page_translations.name', 'asc')
                ->pluck('page_translations.name', 'page_translations.id');

            $query->prepend('---', '');
        }
        return $query;
    }

    public function fields($lang, $action, $data = false)
    {
        $fields = [
            [
                "label" => 'Ad',
                "db" => "name",
                "type" => 'text',
                "action" => 'edit',"attr" => ['class'=>'form-control', 'required']
            ],
            [
                "label" => "Kateqoriya",
                "db" => "parent_id",
                "type" => "select",
                "data" => $this->category($lang),
                "selected" => null,
                "hide" => [],
                "attr" => ['class'=>'select-search form-control']
            ],
            [
                "label" => "Dil",
                "db" => "lang",
                "type" => "select",
                "data" => config('app.locales'),
                "selected" => array_first(config('app.locales')),
                "attr" => ['class'=>'form-control'],
                "edit" => false,
                "divClass" => "language-form"
            ],
            [
                "label" => "Slug (URL)",
                "db" => "slug",
                "type" => "text",
                "hide" => [1],
                "attr" => ['class'=>'form-control', 'autocomplete' => 'off', 'title'=>"Boş saxladığınız təqdirdə sluq avtomatik yaradılacaq."],
            ],
            [
                "label" => "Qısa məzmun",
                "db" => "summary",
                "type" => "textarea",
                "hide" => [1,2,4,5,6],
                "attr" => ['class'=>'form-control', 'rows' => 5],
            ],
            [
                "label" => "Məzmun",
                "db" => "content",
                "type" => "textarea",
                "hide" => [1,4,5,6],
                "attr" => ['class'=>'form-control ckeditor', 'id' => 'editor'.$lang]
            ],
            [
                "label" => "Link",
                "db" => "forward_url",
                "type" => "text",
                "hide" => [1],
                "attr" => ['class'=>'form-control', 'title'=>"Səhifəni başqa ünvana yönləndirmək üçün nəzərdə tutulub.", 'placeholder' => 'Məs: http://google.az'],
            ],
//            [
//                "label" => "Meta description",
//                "db" => "meta_description",
//                "type" => "textarea",
//                "hide" => [1,4],
//                "attr" => ['class'=>'form-control', 'maxlength' => 160, 'rows' => 3, 'title' => 'Maksimum simvol sayı 160 olmalıdır.Sosial şəbəkələrdə paylaşılan zaman məhz bu yazı linkin altında göstəriləcək']
//            ],
//            [
//                "label" => "Meta keywords",
//                "db" => "meta_keywords[]",
//                "type" => "select",
//                "data" => [],
//                "selected" => null,
//                "hide" => [1,4],
//                "attr" => ['class'=>'form-control meta_keywords', 'id'  => 'meta_keywords'.$lang, 'multiple' => 'multiple', 'autocomplete' => 'off', 'title' => 'Keyword-ləri bir birindən ayırmaq üçün Enter düyməsini sıxın.'],
//            ],
        ];

        if(is_null($lang)){
            $paramFields = (new PageParameterCrud())->fields('get');
            return $this->render(array_merge($fields, $paramFields), $action, $data);
        }
        else{
            return $this->render($fields, $action, $data);
        }
    }
}


