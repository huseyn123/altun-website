<?php

namespace App\Logic;

use App\Models\Page;

trait Share
{
    public $lang, $config, $menu;

    protected function loadData()
    {
        $webCache = new WebCache;
        $menu = new Menu;

        $this->lang = app()->getLocale();
        $this->dictionary = $webCache->getDictionary($this->lang);
        $this->menu = $menu;
        $this->config = getConfig();


        view()->share('ages', $webCache->getAges($this->lang));
        view()->share('menu', $menu->all());
        view()->share('config', $this->config);
        view()->share('dictionary',$this->dictionary);
        view()->share('sliders', $webCache->getSlider($this->lang));
        view()->share('projects', $webCache->getProjects($this->lang));
        view()->share('productsCount', $webCache->getProductsCount($this->lang));


        view()->share('lang', $this->lang);

    }
}
