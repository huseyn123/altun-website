<?php

namespace App\Logic;
use App\Crud\CertificateCrud;
use App\Crud\PageParameterCrud;
use App\Logic\ImageRepo;
use App\Logic\Order;
use App\Models\PageTranslation;
use Illuminate\Support\Facades\Validator;
use App\Crud\PageCrud;
use App\Models\Page;
use Illuminate\Http\Request;
use App\DataTables\PageDataTable;
use App\Logic\Slug;
use DB;

trait AdminPages
{


    public function __construct($request,$order) {
        $this->middleware('auth:admin');
        $this->middleware('ajax')->except(['index', 'order']);
        $this->requests = $request->except('_token', '_method','image','cover');
        $this->route = $request->segment(2);

        if (in_array(strtolower($request->method()), ['put', 'post', 'patch', 'delete'])) {
            clearCache('pages');
        }
    }


    public function create()
    {
        $fields = $this->crud->fields(null, 'create');

        return view('admin.page.create', ['route' => "$this->route.store", 'fields' => $fields, 'title' => 'Yeni']);
    }

    public function store(Request $request)
    {
        if (count(config('app.locales')) == 1) {
            $lang = app()->getLocale();
        } else {
            $lang = $request->lang;
        }

        $translates = self::TRANSLATES;


        // check Validation
        $validation = Validator::make($request->all(), Page::rules($lang, null,$request->template_id), Page::$messages);
        $response = $this->validation($validation, $lang);

        if ($response['code'] == 1) {
            return $this->responseJson($response);
        }

        if ($request->parent_id > 0) {
            $parent = PageTranslation::findOrFail($request->parent_id);
            $lang = $parent->lang;
        }

        // apply
        $this->requests['lang'] = $lang;
        $this->requests['order'] = PageTranslation::max('order') + 1;
        $this->requests['slug'] = Slug::slugify($request->slug, $request->name, 'pageTranslation', $lang);

        //inputs for models
        $pageInputs = array_diff_key($this->requests, array_flip($translates));
        $translationInputs = array_diff_key($this->requests, $pageInputs);

        //store
        DB::beginTransaction();


        try {
            $page = Page::create($pageInputs);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->errorDt($e->getMessage());
        }

        try {
            $translationInputs['page_id'] = $page->id;
            $page_translation = PageTranslation::create($translationInputs);

        } catch (\Exception $e) {
            DB::rollback();
            return $this->errorDt($e->getMessage());
        }


        if ($request->has('image')){
            Page::$width =  config('config.page_size.'.$page->template_id.'.width') ?? '1920';
            Page::$height = config('config.page_size.'.$page->template_id.'.height') ?? '300';

            //thumb_size
            Page::$thumb_width =  config('config.thumb_page_size.'.$page->template_id.'.width') ?? '300';
            Page::$thumb_height = config('config.thumb_page_size.'.$page->template_id.'.height') ?? '100';

            Page::$template_id = $page->template_id;

            try {
                $page
                    ->addMedia($request->image)
                    ->toMediaCollection();
            } catch (\Exception $e) {
                DB::rollback();
                return $this->errorDt($e->getMessage());
            }
        }

        if ($request->has('cover')){
            Page::$width2 =  config('config.page_size2.'.$page->template_id.'.width') ?? '1920';
            Page::$height2 = config('config.page_size2.'.$page->template_id.'.height') ?? '300';

            //thumb_size
            Page::$thumb_width2 =  config('config.thumb_page_size2.'.$page->template_id.'.width') ?? '500';
            Page::$thumb_height2 = config('config.thumb_page_size2.'.$page->template_id.'.height') ?? '80';


            Page::$template_id = $page->template_id;

            try {
                $page
                    ->addMedia($request->cover)
                    ->toMediaCollection('cover');
            } catch (\Exception $e) {
                DB::rollback();
                return $this->errorDt($e->getMessage());
            }
        }
        if($request->has('icon')){
            try {
                $page
                    ->addMedia($request->icon)
                    ->toMediaCollection('icon');
            } catch (\Exception $e) {
                DB::rollback();
                return $this->errorDt($e->getMessage());
            }
        }



        DB::commit();

        return $this->responseJson($response);
    }

    public function edit($id)
    {
        $parameterCrud = $this->paramCrud;//

        $fields = [];
        $langs = [];
        $keys = [];

        $page = Page::join('page_translations as pt', 'pt.page_id', '=', 'pages.id')
            ->where('pt.id', $id)
            ->whereNull('pt.deleted_at')
            ->select(
                'pt.*',
                'pt.id as tid',
                'pages.id',
                'pages.template_id',
                'pages.visible',
                'pages.target'
            )
            ->firstOrFail();

        if(count(config('app.locales')) == 1){
            return $this->singleEdit($page);
        }

        $relatedPage = $page->relatedPages;
        $parameters = $parameterCrud->fields('edit', $page);

        foreach ($relatedPage as $rel){
            $fields[$rel->lang] = $this->crud->fields($rel->lang, 'edit', $rel);
            $langs[$rel->lang] = $rel->lang;
            $keys[$rel->lang] = array_filter(explode(",", $rel->meta_keywords));
        }

        foreach(array_diff_key(config('app.locales'), $langs) as $key => $locale){
            $fields[$key] = $this->crud->fields( $key, 'edit', null);

            $keys[$key] = [];
        }

        return view('admin.page.edit-withlocale', ['info' => $page, 'langs' => $langs, 'parameters' => $parameters,'fields' => $fields, 'relatedPage' => $relatedPage, 'keys' => $keys, 'model' => 'page', 'route' => 'pageTranslation' ]);
    }

    private function singleEdit($page)
    {
        $fields = $this->crud->fields(null,'edit', $page);

        $keys = array_filter(explode(",", $page->meta_keywords));

        return view('admin.page.edit', ['info' => $page, 'fields' => $fields, 'keys' => $keys, 'model' => 'page']);
    }

    public function update(Request $request, $id)
    {
        $page = Page::findOrFail($id);
        $this->requests['show_index'] = $request->show_index;
        Page::$template_id = $page->template_id;

        Page::$width =  config('config.page_size.'.$page->template_id.'.width') ?? '1920';
        Page::$height = config('config.page_size.'.$page->template_id.'.height') ?? '300';

        Page::$thumb_width = config('config.thumb_page_size.'.$page->template_id.'.width') ?? 300;
        Page::$thumb_height = config('config.thumb_page_size.'.$page->template_id.'.height') ?? 100;

        Page::$width2 =  config('config.page_size2.'.$page->template_id.'.width') ?? '1920';
        Page::$height2 = config('config.page_size2.'.$page->template_id.'.height') ?? '300';

        Page::$thumb_width2 = config('config.thumb_page_size2.'.$page->template_id.'.width') ?? 500;
        Page::$thumb_height2 = config('config.thumb_page_size2.'.$page->template_id.'.height') ?? 80;

        $validation = Validator::make($request->all(), Page::parameterRules(), Page::$messages);
        $response = $this->validation($validation, $id);

        if($response['code'] == 1){
            return $this->responseJson($response);
        }

        foreach($this->requests as $key => $put){
            $page->$key = $put;
        }

        $page->save();




        if($request->hasFile('image')){
            try{

                $page->clearMediaCollection();  //delete old file
                $page->addMedia($request->image)
                    ->toMediaCollection();
            }
            catch(\Exception $e){
                DB::rollback();
                return $this->errorDt($e->getMessage());
            }
        }

        if($request->hasFile('cover')){

            try{

                $page->clearMediaCollection('cover');  //delete old file
                $page->addMedia($request->cover)
                    ->toMediaCollection('cover');
            }
            catch(\Exception $e){
                DB::rollback();
                return $this->errorDt($e->getMessage());
            }
        }
        if($request->hasFile('icon')){
            try{

                $page->clearMediaCollection('icon');  //delete old file
                $page->addMedia($request->icon)
                    ->toMediaCollection('icon');
            }
            catch(\Exception $e){
                DB::rollback();
                return $this->errorDt($e->getMessage());
            }
        }

        return $this->responseJson($response);
    }

    public function updateSingle(Request $request, $id)
    {
        $this->requests['show_index'] = $request->show_index;

        $pageTranslation = PageTranslation::findOrFail($id);

        $translates = self::TRANSLATES;

        // check Validation
        $validation = Validator::make($this->requests, Page::rules($pageTranslation->lang, $id), Page::$messages);
        $response = $this->validation($validation, $pageTranslation->lang);

        if($response['code'] == 1){
            return $this->responseJson($response);
        }

        if($request->parent_id > 0)
        {
            PageTranslation::findOrFail($request->parent_id);
        }

        // apply
        $this->requests['lang'] = $pageTranslation->lang;
        $this->requests['order'] = PageTranslation::max('order') + 1;
        $this->requests['slug'] = Slug::slugify($request->slug, $request->name, 'pageTranslation', $pageTranslation->lang );

        //inputs for models
        $pageInputs = array_diff_key($this->requests, array_flip($translates));
        $translationInputs = array_diff_key($this->requests, $pageInputs);

        //store
        DB::beginTransaction();


        try{
            Page::where('id', $pageTranslation->page_id)->update($pageInputs);
        }
        catch(\Exception $e){
            DB::rollback();
            return $this->errorDt($e->getMessage());
        }

        try{
            foreach($translationInputs as $key => $put){
                $pageTranslation->$key = $put;
            }
            $pageTranslation->save();
        }
        catch(\Exception $e){
            DB::rollback();
            return $this->errorDt($e->getMessage());
        }

        DB::commit();

        return $this->responseJson($response);
    }

    private function validation($validation, $lang, $id = null)
    {
        $response = $this->responseDataTable(0,"", "#pages", '#myModal');

        if($validation->fails()){
            $response = $this->errorDt($validation->errors()->first());
        }

        return $response;
    }

    private function errorDt($msg)
    {
        return $this->responseDataTable(1, $msg);
    }
}
