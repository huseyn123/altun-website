<?php
/**
 * Created by PhpStorm.
 * User: Rashad
 * Date: 2/28/17
 * Time: 22:20
 */

namespace App\Logic;
use Illuminate\Support\Facades\Schema;
class Configs
{
    public static function set()
    {
        $localization = config('laravellocalization.supportedLocales');

        $prefixLocales = array_combine(array_keys($localization), array_keys($localization));

        $locales = $prefixLocales;

        array_walk($locales, function (&$element, $key) use($localization) {
            $element = $localization[$key]['native'];
        });

        \Config::set('app.locales', $prefixLocales);
        \Config::set('app.localesFull', $locales);


        if(Schema::hasTable('settings')){
            $settings = WebCache::config();

            foreach ($settings as $setting) {

                if(!is_null($setting->config_path)){
                    \Config::set($setting->config_path, $setting->value);
                }

                if($setting->key == 'lang'){
                    app()->setLocale($setting->value);
                }
            }
        }

    }
}