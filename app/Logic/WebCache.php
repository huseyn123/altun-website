<?php
/**
 * Created by PhpStorm.
 * User: Rashad
 * Date: 2/28/17
 * Time: 22:20
 */

namespace App\Logic;

use App\Models\ApartmentElement;
use App\Models\Config;
use App\Models\Dictionary;
use App\Models\Product;
use App\Models\Slider;
use App\Models\Partner;
use App\Models\Page;
use App\Models\ProductAge;
use DB;
use Cache;

class WebCache
{
    public function getDictionary($lang)
    {
        $collect = [];

        $dictionary = Cache::rememberForever("dictionary_$lang", function () use($collect, $lang)
        {
            return Dictionary::where('lang_id', $lang)->select('keyword', 'content')->orderBy('keyword', 'asc')->pluck('content', 'keyword')->toArray();
        });

        return $dictionary;
    }

    public function getProductsCount($lang)
    {

        $product = Cache::rememberForever("prouctsCount_$lang", function () use($lang)
        {
            return Product::join('product_translations as ptr','ptr.product_id','products.id')
                ->join('page_translations as pt', 'pt.id', '=', 'ptr.page_id')
                ->join('pages', 'pages.id', '=', 'pt.page_id')
                ->whereNull('ptr.deleted_at')
                ->whereNull('pt.deleted_at')
                ->where('ptr.lang',$lang)
                ->whereIn('pages.template_id',[6])->select('products.id')->get();
        });

        return $product->count();
    }

    public function getSlider($lang)
    {
        $slider = Cache::rememberForever("slider_$lang", function () use($lang){
            return Slider::with('media')->where('template_id',0)->orderBy('order', 'asc')->where('lang',$lang)->limit(10)->get();
        });

        return $slider;
    }

    public function getProjects($lang)
    {
        $slider = Cache::rememberForever("project_$lang", function () use($lang){
            return Slider::with('media')->where('template_id',1)->orderBy('order', 'asc')->where('lang',$lang)->limit(10)->get();
        });

        return $slider;
    }

    public function getAges($lang)
    {
        $ages = Cache::rememberForever("ages", function () use($lang) {
            return ProductAge::join('products', 'products.age_id', '=', 'product_ages.id')
                ->join('product_translations as ptr', 'ptr.product_id', 'products.id')
                ->whereNull('ptr.deleted_at')
                ->where('ptr.lang', $lang)
                ->groupBy('products.age_id')
                ->select('product_ages.id', 'product_ages.age')->get();

        });
        return $ages;
    }

    public static function config()
    {
        $config = Cache::rememberForever('config', function () {
               return Config::all();
        });

        return $config;
    }
}