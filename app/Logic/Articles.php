<?php

namespace App\Logic;
use App\Models\Page;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;
use App\Logic\Slug;
use DB;
use App\Logic\ImageRepo;
use App\Models\Article;
use App\Models\ArticleTranslation;
use App\Models\PageTranslation;


trait Articles
{

    public function create()
    {
        $fields = $this->crud->fields(null, 'create');
        return view('admin.page.create', ['route' => "$this->route.store", 'fields' => $fields, 'title' => "Yeni ".$this->create_name ]);
    }

    public function store(Request $request)
    {
        $tags = $request->tag;
        $parent = PageTranslation::findOrFail($request->page_id);
        $lang = $parent->lang;

        $translates = ['name', 'page_id','slug', 'summary', 'content','meta_description', 'meta_keywords'];

        Article::$width =  config('config.crud.'.$this->route.'-list.width') ?? '720';
        Article::$height = config('config.crud.'.$this->route.'-list.height') ?? '525';

        // check Validation
        if($this->route == 'reports'){
            $validation = Validator::make($request->all(), Article::ReportRules( null), Article::$messages);
        }else{
            $validation = Validator::make($request->all(), Article::rules( null), Article::$messages);
        }
        $response = $this->validation($validation);

        if($response['code'] == 1){
            return $this->responseJson($response);
        }

        // apply
        $this->requests['slug'] = Slug::slugify($request->slug, $request->name, 'articleTranslation', $lang );

        //inputs for models
        $articleInputs = array_diff_key($this->requests, array_flip($translates));
        $translationInputs = array_diff_key($this->requests, $articleInputs);

        //store
        DB::beginTransaction();

        try{

            $article = Article::create($articleInputs);
        }
        catch(\Exception $e){
            DB::rollback();
            return $this->errorDt($e->getMessage());
        }

        try{
            $translationInputs['article_id'] = $article->id;
            $translationInputs['lang'] = $lang;

            ArticleTranslation::create($translationInputs);
        }
        catch(\Exception $e){
            DB::rollback();
            return $this->errorDt($e->getMessage());
        }

        if($request->hasFile('image')) {
            try{
                $article
                    ->addMedia($request->image)
                    ->toMediaCollection();
            }
            catch(\Exception $e){
                DB::rollback();
                return $this->errorDt($e->getMessage());
            }
        }

        DB::commit();

        return $this->responseJson($response);
    }


    public function edit($id)
    {
        $parameterCrud = $this->paramCrud;
        $fields = [];
        $langs = [];
        $keys = [];

        $article = Article::join('article_translations as at', 'at.article_id', '=', 'articles.id')
            ->where('at.id', $id)
            ->whereNull('at.deleted_at')
            ->select(
                'at.*',
                'at.id as tid',
                'articles.id',
                'articles.status',
                'articles.show_index',
                'articles.youtube_link',
                'articles.published_at'
            )
            ->firstOrFail();

        if(count(config('app.locales')) == 1){
            return $this->editSingle($article);
        }

        $relatedPage = $article->relatedPages;
        $parameters = $parameterCrud->fields('edit', $article);

        foreach ($relatedPage as $rel){
            $fields[$rel->lang] = $this->crud->fields($rel->lang, 'edit', $rel);
            $langs[$rel->lang] = $rel->lang;
            $keys[$rel->lang] = array_filter(explode(",", $rel->meta_keywords));
        }

        foreach(array_diff_key(config('app.locales'), $langs) as $key => $locale){
            $fields[$key] = $this->crud->fields( $key, 'edit', null);
            $keys[$key] = [];
        }

        return view('admin.page.edit-withlocale', ['info' => $article, 'langs' => $langs, 'parameters' => $parameters, 'fields' => $fields, 'relatedPage' => $relatedPage,'keys' => $keys,'model' => 'articles', 'route' => 'articleTranslation' ]);
    }


    private function editSingle($page)
    {
        $fields = $this->crud->fields(null,'edit', $page);

        return view('admin.page.edit', ['info' => $page, 'fields' => $fields, 'model' => 'article']);
    }

    public function update(Request $request, $id)
    {
        $article = Article::join('article_translations as at', 'articles.id', '=', 'at.article_id')
            ->join('page_translations as pt', 'pt.id', '=', 'at.page_id')
            ->join('pages', 'pages.id', '=', 'pt.page_id')
            ->select('articles.*','pages.template_id')
            ->findOrFail($id);

        Article::$width =  config('config.crud.list-'.$article->template_id.'.width') ?? '720';
        Article::$height = config('config.crud.list-'.$article->template_id.'.height') ?? '525';


        $validation = Validator::make($request->all(), Article::parameterRules(), Article::$messages);

        $response = $this->validation($validation, $id);

        $this->requests['show_index'] = $request->show_index;


        if($response['code'] == 1){
            return $this->responseJson($response);
        }

        DB::beginTransaction();

        try{
            foreach($this->requests as $key => $put){
                $article->$key = $put;
            }
            $article->save();
        }
        catch(\Exception $e){
            DB::rollback();
            return $this->errorDt($e->getMessage());
        }

        if($request->hasFile('image')){

            try{
                $article->clearMediaCollection();  //delete old file
                $article->addMedia($request->image)
                    ->toMediaCollection();
            }
            catch(\Exception $e){
                DB::rollback();
                return $this->errorDt($e->getMessage());
            }
        }

        DB::commit();

        return $this->responseJson($response);
    }


    public function updateSingle(Request $request, $id)
    {
        $articleTranslation = ArticleTranslation::findOrFail($id);
        $article = Article::findOrFail($articleTranslation->article_id);
        $this->requests['show_index'] = $request->show_index;

        Article::$width =  config('config.crud.'.$this->route.'-list.width') ?? '360';
        Article::$height = config('config.crud.'.$this->route.'-list.height') ?? '260';

        $tags = $request->tag;
        $articleTags = $article->tags->pluck('tag_id')->toArray();

        $translates = ['name', 'page_id', 'slug', 'summary', 'content', 'lang','address'];

        // check Validation
        $validation = Validator::make($request->all(), Article::rules($id), Article::$messages);
        $response = $this->validation($validation, $id);

        if($response['error'] == 1){
            return $this->responseJson($response);
        }

        $page = PageTranslation::findOrFail($request->page_id);


        // apply
        $this->requests['lang'] = $page->lang;
        $this->requests['slug'] = Slug::slugify($request->slug, $request->name, 'articleTranslation', $page->lang);

        //inputs for models
        $articleInputs = array_diff_key($this->requests, array_flip($translates));
        $translationInputs = array_diff_key($this->requests, $articleInputs);

        //store
        DB::beginTransaction();


        try{
            foreach($articleInputs as $key => $put){
                $article->$key = $put;
            }
            $article->save();
        }
        catch(\Exception $e){
            DB::rollback();
            return $this->errorDt($e->getMessage());
        }

        try{
            foreach($translationInputs as $key => $put){
                $articleTranslation->$key = $put;
            }
            $articleTranslation->save();
        }
        catch(\Exception $e){
            DB::rollback();
            return $this->errorDt($e->getMessage());
        }

        if($request->hasFile('image')){

            try{
                $article->clearMediaCollection();  //delete old file
                $article->addMedia($request->image)
                    ->withResponsiveImages()
                    ->toMediaCollection();
            }
            catch(\Exception $e){
                DB::rollback();
                return $this->errorDt($e->getMessage());
            }
        }

        DB::commit();

        return $this->responseJson($response);
    }


    private function validation($validation, $id = null)
    {
        $response = $this->responseDataTable(0, "", "#articles", '#myModal');

        if($validation->fails()){
            $response = $this->errorDt($validation->errors()->first());
        }

        return $response;
    }


    private function errorDt($msg)
    {
        return $this->responseDataTable(1, $msg);
    }
}
