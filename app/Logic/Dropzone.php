<?php

namespace App\Logic;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Response;

class Dropzone extends ImageRepo
{
    public function upload($data, $file,$width,$height,$route,$gallery_type)
    {
        $limit = albumLimit();

        $rules = [
            'file' => 'required|mimes:png,gif,jpeg,jpg|max:1000000|dimensions:min_width='.$width.',min_height='.$height
        ];

        $messages = [
            'file.mimes' => 'Şəkil :values formatda yüklənməlidir',
            'file.max' => 'Şəkil max',
        ];


        $validator = Validator::make(request()->all(), $rules, $messages);

        if ($validator->fails()) {

            $response = ['error' => true, 'message' => $validator->messages()->first(), 'code' => 400];

            return  Response::json($response, 400);
        }

        $count = $data->getMedia($gallery_type)->count();

        if($count >= $limit){
            $response = ['error' => true, 'message' => 'Albom üçün şəkil limiti: '.$limit, 'code' => 400];

            return  Response::json($response, 400);
        }


        if($route === 'projects'){
            $data::$gallery_thumb_width =  config('config.crud.'.$route.'-gallery-thumb.width');
            $data::$gallery_thumb_height =  config('config.crud.'.$route.'-gallery-thumb.height');
            $data::$gallery_width =  config('config.crud.'.$route.'-gallery-blade.width');
            $data::$gallery_height =  config('config.crud.'.$route.'-gallery-blade.height');
        }

        $media = $data
                ->addMedia($file)
                ->toMediaCollection($gallery_type);

        $response = ['error' => false, 'message' => 'Success', 'code' => 200, 'deleteRoute' => route('gallery.destroy', $media->id)];

        return Response::json($response, $response['code']);
    }
}