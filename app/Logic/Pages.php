<?php

namespace App\Logic;
use App\Models\Article;
use App\Models\Author;
use App\Models\Banner;
use App\Models\BookEducation;
use App\Models\Branch;
use App\Models\BranchElement;
use App\Models\Certificates;
use App\Models\Faq;
use App\Models\Kiv;
use App\Models\Page;
use App\Models\ArticleTranslation;
use App\Models\PageTranslation;
use App\Models\Product;
use App\Models\Slider;
use App\Models\Vacancy;
use App\Models\AuthorTranslation;
use DB;
use function GuzzleHttp\Promise\queue;

trait Pages
{

    protected function products($getPage, $relatedPages,$canonical_url)
    {
        $limit = request()->get('limit') ?? 24;//limit
        $paginate = request()->get('page') ?? 1;
        $sort = request()->get('sort') ?? 0;
        $selected_cats = request()->get('cat');
        $selected_ages = request()->get('age');

        $query = Product::join('product_translations as ptr','ptr.product_id','products.id')
            ->join('page_translations as pt', 'pt.id', '=', 'ptr.page_id')
            ->join('pages', 'pages.id', '=', 'pt.page_id')
            ->BySort($sort)
            ->ByAge($selected_ages)
            ->whereNull('ptr.deleted_at')
            ->whereNull('pt.deleted_at')
            ->where('ptr.lang',$this->lang)
            ->whereIn('pages.template_id',[6])
            ->select('ptr.*','ptr.id as tid','pt.slug as page_slug','pt.name as category_name','products.id','products.price','products.bar_code')
            ->with(['rel_author','media']);

        $products = $query->ByCat($selected_cats,$getPage->tid)->paginate($limit);

        $related_products = Product::join('product_translations as ptr','ptr.product_id','products.id')
            ->join('page_translations as pt', 'pt.id', '=', 'ptr.page_id')
            ->join('pages', 'pages.id', '=', 'pt.page_id')
            ->whereNull('ptr.deleted_at')
            ->whereNull('pt.deleted_at')
            ->whereNotIn('ptr.page_id', [$getPage->tid])
            ->where('ptr.lang',$this->lang)
            ->whereIn('pages.template_id',[6])
            ->select('ptr.*','ptr.id as tid','pt.slug as page_slug','pt.name as category_name','products.id','products.price','products.bar_code')
            ->with(['rel_author','media'])->limit(20)->get();

        if(request()->ajax())
        {
            header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
            header("Cache-Control: post-check=0, pre-check=0", false);
            header("Pragma: no-cache");

            $view = view('web.elements.product_crud',['products' => $products,'paginate' => $paginate])->render();
            return response()->json(['html'=>$view]);
        }

        return view("web.products", ['page' => $getPage,'relatedPages' => $relatedPages,
            'products' =>$products,'related_products'=>$related_products,'canonical_url' => $canonical_url
            ,'paginate' => $paginate,'limit'=> $limit,'sort' =>$sort,'selected_cats' => $selected_cats,'selected_ages' => $selected_ages]);
    }

    protected function productSingle($checkPages, $slug)
    {
        $category = $checkPages->first();
        $getProduct = Product::join('product_translations as pt', 'products.id', '=', 'pt.product_id')
            ->leftJoin('author_translations as at','at.id','pt.author_id')
            ->leftJoin('book_langs','book_langs.id','pt.product_lang')
            ->where('pt.slug', $slug)
            ->whereNull('pt.deleted_at')
            ->with('rel_author','media')
            ->select('pt.*', 'pt.id as tid', 'products.*','at.name as author','book_langs.title_'.$this->lang.' as lang_title')
            ->firstOrFail();
        $product_education = BookEducation::whereIn('id',$getProduct->product_education ? $getProduct->product_education : [])->select('title_'.$this->lang.' as name')->get();

        $relatedPages = $this->menu->relatedPages($checkPages->last(), $getProduct);
        $similarProducts = $this->similarProducts($getProduct->id,20,$category->tid)->get();
        $canonical_url = route('showPage',[$category->slug, $getProduct->slug]);

        return view('web.product-single',['product' => $getProduct,'product_education'=>$product_education,'similarProducts' => $similarProducts,'page' => $category,'relatedPages' => $relatedPages,'canonical_url' => $canonical_url]);
    }

    protected function authorSingle($checkPages, $slug)
    {
        $paginate = request()->get('page') ?? 1;
        $limit = request()->get('limit') ?? 24;//limit

        $category = $checkPages->first();
        $getAuthor = Author::join('author_translations as at', 'authors.id', '=', 'at.author_id')
            ->where('at.slug', $slug)
            ->whereNull('at.deleted_at')
            ->with('media')
            ->select('at.*', 'at.id as tid', 'authors.*')
            ->firstOrFail();

        $relatedPages =null;

        $authorBooks =  Product::join('product_translations as ptr', 'products.id', '=', 'ptr.product_id')
            ->join('page_translations as pt', 'pt.id', '=', 'ptr.page_id')
            ->join('pages', 'pages.id', '=', 'pt.page_id')
            ->join('product_authors as paut','paut.product_id','ptr.id')
            ->whereNull('ptr.deleted_at')
            ->where('paut.author_id',$getAuthor->tid)
            ->with(['rel_author','media'])
            ->select('ptr.*','ptr.id as tid','pt.slug as page_slug','pt.name as category_name','ptr.id as tid', 'products.*')
            ->paginate($limit);


        if(request()->ajax())
        {
            header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
            header("Cache-Control: post-check=0, pre-check=0", false);
            header("Pragma: no-cache");

            $view = view('web.elements.author_product_crud',['author' => $getAuthor,'products' => $authorBooks,'paginate' => $paginate])->render();
            return response()->json(['html'=>$view]);
        }



        $canonical_url = route('showPage',[$category->slug, $getAuthor->slug]);

        return view('web.author-single', ['author' => $getAuthor,'products' => $authorBooks,'page' => $category,'relatedPages' => $relatedPages,'canonical_url' => $canonical_url]);
    }

    protected function static($getPage, $relatedPages,$view,$canonical_url)
    {
        return view("web.".$view, ['page' => $getPage,'relatedPages' => $relatedPages,'canonical_url' => $canonical_url]);
    }

    protected function abouts($getPage, $relatedPages,$canonical_url)
    {
        $products = Product::join('product_translations as ptr','ptr.product_id','products.id')
            ->join('page_translations as pt', 'pt.id', '=', 'ptr.page_id')
            ->join('pages', 'pages.id', '=', 'pt.page_id')
            ->whereNull('ptr.deleted_at')
            ->whereNull('pt.deleted_at')
            ->where('ptr.lang',$this->lang)
            ->whereIn('pages.template_id',[6])
            ->select('ptr.*','ptr.id as tid','pt.slug as page_slug','pt.name as category_name','products.id','products.price','products.bar_code')
            ->with(['rel_author','media'])->limit(20)->get();

        return view("web.about", ['products' => $products,'page' => $getPage,'relatedPages' => $relatedPages,'canonical_url' => $canonical_url]);
    }

    protected function authors($getPage, $relatedPages,$canonical_url)
    {
        $authors = Author::join('author_translations as at','at.author_id','authors.id')
            ->join('page_translations as pt', 'pt.id', '=', 'at.page_id')
            ->join('pages', 'pages.id', '=', 'pt.page_id')
            ->whereNull('at.deleted_at')
            ->whereNull('pt.deleted_at')
            ->where('at.lang',$this->lang)
            ->where('at.page_id', $getPage->tid)
            ->whereIn('pages.template_id',[5])
            ->select('at.*','pt.slug as page_slug','authors.id')
            ->with('media')
            ->get();

        $products = Product::join('product_translations as ptr','ptr.product_id','products.id')
            ->join('page_translations as pt', 'pt.id', '=', 'ptr.page_id')
            ->join('pages', 'pages.id', '=', 'pt.page_id')
            ->whereNull('ptr.deleted_at')
            ->whereNull('pt.deleted_at')
            ->where('ptr.lang',$this->lang)
            ->whereIn('pages.template_id',[6])
            ->select('ptr.*','ptr.id as tid','pt.slug as page_slug','pt.name as category_name','products.id','products.price','products.bar_code')
            ->with(['rel_author','media'])
            ->limit(20)
            ->get();

        return view("web.authors", ['page' => $getPage,'relatedPages' => $relatedPages,'canonical_url' => $canonical_url,'authors'=> $authors,'products'=>$products]);
    }

    protected function static_block($getPage, $relatedPages,$view,$canonical_url,$template_id =[2])
    {
        $blocks = Page::join('page_translations as pt','pt.page_id','pages.id')
            ->whereNull('pt.deleted_at')
            ->where('pt.parent_id',$getPage->tid)
            ->where('pt.lang',$this->lang)
            ->whereIn('pages.template_id',$template_id)
            ->select('pt.name','pt.content','pages.id')
            ->with('media')
            ->get();

        return view("web.".$view, ['page' => $getPage,'blocks' =>$blocks,'relatedPages' => $relatedPages,'canonical_url' => $canonical_url]);
    }

    protected function news($getPage, $relatedPages,$canonical_url)
    {
        $articles = Article::join('article_translations as at', 'articles.id', '=', 'at.article_id')
            ->join('page_translations as pt', 'pt.id', '=', 'at.page_id')
            ->where('articles.status', 1)
            ->where('at.page_id', $getPage->tid)
            ->where('at.lang', $getPage->lang)
            ->whereNull('pt.deleted_at')
            ->whereNull('at.deleted_at')
            ->orderBy('articles.published_at', 'desc')
            ->orderBy('articles.id', 'desc')
            ->with('media')
            ->select('articles.id','articles.id','at.article_id','at.name',
                'at.slug', 'at.summary', 'articles.published_at',
                'pt.name as page_name', 'pt.slug as page_slug')
            ->paginate(9);

        return view("web.news", ['page' => $getPage,'articles' =>$articles,'relatedPages' => $relatedPages,'canonical_url' => $canonical_url]);
    }

    protected function getPageByTemplateId($templateId)
    {
        $page = Page::join('page_translations as pt', 'pt.page_id', '=', 'pages.id')
            ->where('pt.lang', $this->lang)
            ->where('pages.template_id', $templateId)
            ->select('pt.slug', 'pt.name', 'pt.id')
            ->first();

        return $page;
    }

    private function getChildrenPage($page)
    {
        $children = $page->children;

        if($children->count() && $page->id != $children->first()->id){

            return $children->first();
        }
        else{
            return $page;
        }
    }

    protected function similarArticles($id = 0,$template_id,$limit)
    {
        $articles = Article::join('article_translations as at', 'articles.id', '=', 'at.article_id')
            ->join('page_translations as pt', 'pt.id', '=', 'at.page_id')
            ->join('pages', 'pages.id', '=', 'pt.page_id')
            ->select('at.name', 'at.slug as article_slug','pt.slug','articles.published_at','articles.id')
            ->where('articles.id', '<>', $id)
            ->where('pages.template_id',$template_id)
            ->where('at.lang', $this->lang)
            ->where('articles.status', 1)
            ->whereNull('pt.deleted_at')
            ->whereNull('at.deleted_at')
            ->orderBy('articles.published_at', 'desc')
            ->orderBy('articles.id', 'desc')
            ->limit($limit);

        return $articles;
    }

    protected function similarProducts($id = 0,$limit,$page_id)
    {
        $products = Product::join('product_translations as ptr', 'products.id', '=', 'ptr.product_id')
            ->join('page_translations as pt', 'pt.id', '=', 'ptr.page_id')
            ->join('pages', 'pages.id', '=', 'pt.page_id')
            ->where('products.id', '<>', $id)
            ->whereNull('pt.deleted_at')
            ->whereNull('ptr.deleted_at')
            ->where('pt.lang', $this->lang)
            ->where('ptr.lang', $this->lang)
            ->where('ptr.page_id',$page_id)
            ->with(['rel_author','media'])
            ->select('ptr.*', 'ptr.id as tid','pt.name as category_name','products.*','pt.slug as page_slug')
            ->inRandomOrder()
            ->limit($limit);

        return $products;
    }

    protected function dropdown($getPage,$relatedPages)
    {
        if ($getPage->MenuChildren('tid')->count()) {
            return redirect()->route('showPage', $getPage->MenuChildren('tid')->first()->slug);
        } else {
            return redirect()->route('home');
        }
    }
}
