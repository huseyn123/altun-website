<?php

namespace App\Logic;

use App\Models\BookEducation;
use App\Models\BookLang;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

trait NoCrud
{

    public function create()
    {
        return view('admin.'.$this->view.'.create', ['route' => $this->route.'.store','title' => $this->title,'form_data' =>$this->form_data]);

    }

    public function store(Request $request)
    {

        // check Validation
        $validation = Validator::make($request->all(), $this->model::rules(), $this->model::$messages);
        $response = $this->validation($validation);

        if($response['code'] == 1){
            return $this->responseJson($response);
        }


        DB::beginTransaction();

        try{
            $this->model::create($this->requests);
        }
        catch(\Exception $e){
            DB::rollback();
            return $this->errorDt($e->getMessage());
        }

        DB::commit();

        return $this->responseJson($response);
    }

    public function edit($id)
    {
        $data = $this->model::findOrFail($id);

        return view('admin.'.$this->view.'.edit', ['title' => 'Düzəliş et','data'=>$data,'route' => [$this->route.'.update', $id],'form_data' =>$this->form_data]);
    }

    public function update(Request $request, $id)
    {
        $data = $this->model::findOrFail($id);

        $response = $this->validation(null);

        if($response['code'] == 1){
            return $this->responseJson($response);
        }

        foreach($this->requests as $key => $put){
            $data->$key = $put;
        }

        $data->save();

        return $this->responseJson($response);
    }

    public function destroy($id)
    {
        $data = $this->model::findOrFail($id);
        $data->forceDelete();

        $response = $this->responseDataTable(0,"", "#".$this->route, "#modal-confirm");
        return $this->responseJson($response);
    }

    private function validation()
    {
        $validation = Validator::make(request()->all(), $this->model::rules(), $this->model::$messages);

        $response = $this->responseDataTable(0, "", "#".$this->route, '#myModal');

        if($validation->fails()){
            $response = $this->errorDt($validation->errors()->first());
        }

        return $response;
    }

    private function errorDt($msg)
    {
        return $this->responseDataTable(1, $msg);
    }


}
