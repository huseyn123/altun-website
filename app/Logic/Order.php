<?php

namespace App\Logic;

class Order
{
    public function get($route, $title, $name, $lang = false, $depth = 1)
    {
        $model = config('config.model.'.$route);

        if($lang == false){
            $items 	= $model::orderBy('order', 'asc')->join('pages','pages.id','page_translations.page_id')->whereNotIn('pages.template_id',[2,5])->select('page_translations.*')->get();
        }
        else{
            $items 	= $model::where('lang', $lang)->join('pages','pages.id','page_translations.page_id')->whereNotIn('pages.template_id',[2,5])->select('page_translations.*')->orderBy('page_translations.order', 'asc')->get();
        }

        $html = $this->view($items, $name);

        return view('admin.order', ['items' => $items, 'html' => $html, 'route' => "$route.order" ,'depth' => $depth, 'lang' => $lang]);
    }


    private function view($items, $name, $parentid = 0)
    {
        $result = null;
        foreach ($items as $item)
            if ($item->parent_id == $parentid) {
                $result .= "<li class='dd-item nested-list-item' data-order='{$item->order}' data-id='{$item->id}'>
                <div class='dd-handle nested-list-handle'>
                    <span class='fa fa-list-ul fa-fw'></span>
                </div>
                <div class='nested-list-content'>{$item->$name}
                </div>".$this->view($items, $name, $item->id) . "</li>";
            }
        return $result ?  "\n<ol class=\"dd-list\">\n$result</ol>\n" : null;
    }


    private function breadcrumb()
    {
        $breadcrumbs = new Breadcrumbs();
        $breadcrumbs->setListElement('ul');
        $breadcrumbs->addCrumb(webConfig()->company_name, route('dashboard'));
        $breadcrumbs->setCssClasses("breadcrumb");
        $breadcrumbs->setDivider('');

        return $breadcrumbs;
    }


    public function post($request, $route, $parent = false)
    {
        $model = config('config.model.'.$route);

        $source = $request->input('source');

        if($parent == true){

            $item = $model::find($source);

            $destination  = $request->input('destination', null); //id of destination page

            $item->parent_id  = $destination;

            /*if(!is_null($destination)){
                $destinationPage = $model::find($destination);
            }
            else{
                $destinationPage = null;
            }*/

            /*if(!is_null($destination )){
                $item->main_parent_id  = is_null($destinationPage->parent_id) ? $destinationPage->id : $destinationPage->main_parent_id;;
                $item->third_level = $destinationPage->third_level + 1;
            }*/

            $item->save();
        }


        $ordering       = json_decode($request->input('order'));
        $rootOrdering   = json_decode($request->input('rootOrder'));

        if($ordering){
            foreach($ordering as $order=>$item_id){
                if($itemToOrder = $model::find($item_id)){
                    $itemToOrder->order = $order;
                    $itemToOrder->save();
                }
            }
        } else {
            foreach($rootOrdering as $order=>$item_id){
                if($itemToOrder = $model::find($item_id)){
                    $itemToOrder->order = $order;
                    $itemToOrder->save();
                }
            }
        }

        return 'ok ';
    }
}