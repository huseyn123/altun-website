<?php

namespace App\Logic;


class userAction
{
    public static function showAction($trashedItem = true)
    {
        if(auth()->user()->role == 1){
            return true;
        }
        else{
            if($trashedItem == true)
            {
                return false;
            }
            else{
                return true;
            }
        }
    }
}