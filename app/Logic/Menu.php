<?php
/**
 * Created by PhpStorm.
 * User: Rashad
 * Date: 2/28/17
 * Time: 22:20
 */

namespace App\Logic;

use App\Models\Page;
use Cache;

class Menu
{

    public static function all($lang = null)
    {
        if(is_null($lang)){
            $lang = app()->getLocale();
        }

        $select = ['pages.visible','pages.id', 'pages.target', 'pages.template_id', 'pt.parent_id','pt.order','pt.id as tid', 'pt.page_id', 'pt.name', 'pt.slug', 'pt.lang'];

        $menu = Cache::remember('pages_'.$lang, 1440, function () use($select, $lang) {
            return Page::join('page_translations as pt', 'pages.id', '=', 'pt.page_id')
                ->where('pt.lang', $lang)
                ->whereNull('pt.deleted_at')
                ->whereNotIn('template_id',[0])
                ->whereNotIn('pages.visible', [0])
                ->orderBy('pt.order', 'asc')
                ->select($select)
                ->withCount([
                    'menuChildren' => function($query) {
                        $query
                            ->join('pages as join_page', 'join_page.id', '=', 'page_translations.page_id')
                            ->whereNotIn('join_page.template_id',[0])
                            ->whereNotIn('join_page.visible',[0]);
                    }
                ])
                ->get();
        });

        return $menu;
    }


    protected function nestedBuild($pages, $thisPage, $nestedSlug = false)
    {
        $nested = [];

        foreach ($pages as $page){
            if($page->tid == $thisPage->parent_id){
                $nested = $this->nestedBuild($pages, $page, $nestedSlug);
            }
        }

        if($nestedSlug == false){
            $nested[$thisPage->slug] = ['breadcrumb' => ['slug' => $thisPage->slug, 'name' => $thisPage->name], 'data' => $thisPage];
        }
        else{
            $nested[] = $thisPage->slug;
        }


        return $nested;
    }


    protected function nestedBuildSeo($pages, $thisPage, $nestedSlug = false)
    {
        $nested = [];

//        foreach ($pages as $page){
//            if($page->tid == $thisPage->parent_id){
//                $nested = $this->nestedBuild($pages, $page, $nestedSlug);
//            }
//        }

        if($nestedSlug == false){
            $nested[$thisPage->slug] = ['breadcrumb' => ['slug' => $thisPage->slug, 'name' => $thisPage->name], 'data' => $thisPage];
        }
        else{
            $nested[] = $thisPage->slug;
        }


        return $nested;
    }


    public function getData($thisPage, $splice)
    {
        $pages = $this->all();
        $nested = $this->nestedBuild($pages, $thisPage);

        $data = array_splice($nested, $splice);

        $get = array_first($data);

        return $get['data'];
    }


    /**
     * sehifenin diger diller uchun URL-u: meselen: site.com/haqqimizda, site.com/en/about
     * Hemchinin parent-id-si null olmayan sehifenin URL-larini da nezere alir: site.com/biz-kimik bu formada generasiya olunur: site.com/haqqimizda/biz-kimik, site.com/en/about/who-are-we
     */

    public function relatedPages($thisPage, $join = false)
    {
        $relatedUrl = [];

        if(count(config('app.locales')) > 1){

            $related = $thisPage->relatedPages;

            foreach ($related as $rel){
                $pages = $this->all($rel->lang);
                $url = ($rel->lang == app()->getLocale() ? ''  : $rel->lang.'/').implode('/', $this->nestedBuildSeo($pages, $rel, true));

                $relatedUrl[$rel->lang] = $url;
            }

            if($join == true){
                $joinRelated = $join->relatedPages;

                foreach ($joinRelated as $jrel){
                    if(isset($relatedUrl[$jrel->lang])){
                        $relatedUrl[$jrel->lang] = $relatedUrl[$jrel->lang].'/'.$jrel->slug;
                    }
                }
            }
        }

        return $relatedUrl;
    }


    /**
     * axtarish ve sitemap-de istifade olunacaq. URL-u full nested formada generasiya edecek: site.com/biz-kimik => site.com/haqqimizda/biz-kimik
     * Menyularda da istifade oluna biler, eger ehtiyac olarsa, menyularda adeten birbasha sonuncu slug gelir, meselen: site.com/biz-kimik
     */

    public static function fullNestedSlug($parentId, $slug,$lang = null)
    {
        $nested = [];
        $pages = self::all($lang);

        foreach ($pages as $page){
            if($page->tid == $parentId){
                $nested = (new self())->nestedBuild($pages, $page, true);
            }
        }

        $nested[] = $slug;

        $url = implode('/', $nested);

        return url($url);
    }
}