<?php
/**
 * Created by PhpStorm.
 * User: Rashad
 * Date: 2/28/17
 * Time: 22:20
 */

namespace App\Logic;

class MultiLanguageSelect
{
    public static function multiLang($query, $empty = false, $prepend = false,$prepend_data = false)
    {

        if($prepend == true){
            if($prepend_data){
                $optgroup = [$prepend_data['db'] => $prepend_data['text']];
            }else{
                $optgroup = [' ' => '-----------'];
            }
        }
        else{
            $optgroup = [];
        }

        foreach($query as $data)
        {
            $lang = config("app.localesFull.".$data->lang);
            $optgroup[$lang][$data->id] = $data->name;
        }

        return $optgroup;
    }

    public static function multiSelect($query)
    {
        $optgroup = [];

        foreach(config('app.locales') as $key => $locale){
            $lang = config("app.localesFull.".$key);

            foreach($query as $data)
            {
                if($data['title_'.$key]){
                    $optgroup[$lang][$data->id] =$data['title_'.$key];
                }
            }
        }

        return $optgroup;
    }
}