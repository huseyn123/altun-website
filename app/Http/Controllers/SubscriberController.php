<?php

namespace App\Http\Controllers;

use App\Crud\SubscriberCrud;
use App\Models\Subscriber;
use Illuminate\Http\Request;
use App\DataTables\SubscriberDataTable;
use Illuminate\Support\Facades\Validator;
use App\Models\Dictionary;

class SubscriberController extends Controller
{
    private $crud, $title;

    public function __construct(Request $request, SubscriberCrud $crud)
    {
        $this->middleware('auth:admin')->except(['store']);
        $this->middleware('ajax')->except(['index']);

        $this->crud = $crud;
        $this->title = "İzləyicilər";
    }


    public function index(SubscriberDataTable $dataTable)
    {
        return $dataTable->render('admin.subscribers', ['title' => $this->title]);
    }


    public function create()
    {
        $fields = $this->crud->fields('create');

        return view('admin.dt.create', ["fields" => $fields, 'route' => 'subscribers.store', 'title' => 'Yeni izləyici']);
    }


    public function store(Request $request)
    {

        $inputs = [
            'email' => $request->input('email'),
            'status' => auth()->guard('admin')->check() ? 1 : 0
        ];

        $validator = Validator::make($inputs, Subscriber::$rules);

        if ($validator->fails()) {
            $response = $this->responseDataTable(1, $validator->errors()->first());
            return $this->responseJson($response);

        }

        Subscriber::create($inputs);

        $dictionary = Dictionary::where('keyword', 'subscribe_success')->where('lang_id', app()->getLocale())->first();

        $response = $this->responseDataTable(0, $dictionary->content, "#subscribers", "#myModal");
        return $this->responseJson($response);
    }


    public function update($id)
    {
        $user = Subscriber::findOrFail($id);
        $user->status = 1;
        $user->save();

        $response = $this->responseDataTable(0,"", "#subscribers", "#myModal");
        return $this->responseJson($response);
    }


    public function destroy($id)
    {
        $user = Subscriber::findOrFail($id);
        $user->delete();

        $response = $this->responseDataTable(0,"İzləyici siyahıdan silindi", "#subscribers", "#modal-confirm");
        return $this->responseJson($response);
    }
}
