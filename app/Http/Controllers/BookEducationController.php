<?php

namespace App\Http\Controllers;

use App\DataTables\BookEducationDataTable;
use App\Logic\NoCrud;
use Illuminate\Http\Request;
use App\Models\BookEducation;
use Illuminate\Support\Facades\Validator;
use DB;

class BookEducationController extends Controller
{
    use NoCrud;
    private $crud,$requests,$title,$route,$form_data;

    public function __construct(Request $request)
    {
        $this->middleware('auth:admin');
        $this->middleware('ajax')->except(['index']);

        $this->title = "Təhsil Mərhələsi";
        $this->route = "book_education";
        $this->view = "nocrud";
        $this->form_data = [['name'=>'title']];
        $this->model = 'App\Models\BookEducation';
        $this->requests = $request->except('_token', '_method');

    }


    public function index(BookEducationDataTable $dataTable)
    {
        return $dataTable->render('admin.main', ['title' => $this->title,'route' =>$this->route]);
    }



}
