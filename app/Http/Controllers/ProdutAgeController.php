<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ProductAge;
use App\DataTables\ProductAgeDataTable;
use App\Crud\ProductAgeCrud;
use Illuminate\Support\Facades\Validator;
use DB;

class ProdutAgeController extends Controller
{
    private $crud, $requests, $title;

    public function __construct(Request $request, ProductAgeCrud $crud)
    {
        $this->middleware('auth:admin');
        $this->middleware('ajax')->except(['index']);

        $this->crud = $crud;
        $this->title = "Yaş";
        $this->requests = $request->except('_token', '_method');
    }


    public function index(ProductAgeDataTable $dataTable)
    {
        return $dataTable->render('admin.age', ['title' => $this->title]);
    }


    public function create()
    {
        $fields = $this->crud->fields('create');
        return view('admin.dt.create', ['fields' => $fields,'title' => 'Yeni Yaş', 'route' => 'productAge.store']);
    }



    public function store(Request $request)
    {
        $response = $this->validation('required|');

        if($response['code'] == 1){
            return $this->responseJson($response);
        }


        ProductAge::create($this->requests);

        return $this->responseJson($response);
    }


    public function edit($id)
    {
        $data = ProductAge::findOrFail($id);

        $fields = $this->crud->fields('edit', $data);

        return view('admin.dt.edit', ['title' => 'Düzəliş et', 'fields' => $fields, 'data' => $data,  'route' => ['productAge.update', $id]]);
    }


    public function update(Request $request, $id)
    {
        $data = ProductAge::findOrFail($id);

        $response = $this->validation(null);

        if($response['code'] == 1){
            return $this->responseJson($response);
        }

        foreach($this->requests as $key => $put){
            $data->$key = $put;
        }

        $data->save();

        return $this->responseJson($response);
    }



    public function destroy($id)
    {
        $data = ProductAge::findOrFail($id);
        $data->delete();

        $response = $this->responseDataTable(0,"", "#productAge", "#modal-confirm");
        return $this->responseJson($response);
    }

    private function validation($required = 'required|')
    {
        $validation = Validator::make(request()->all(), ProductAge::rules($required), ProductAge::$messages);

        $response = $this->responseDataTable(0,"", "#productAge", '#myModal');

        if($validation->fails()){
            $response = $this->responseDataTable(1, $validation->errors()->first());
        }

        return $response;
    }

}
