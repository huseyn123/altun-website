<?php

namespace App\Http\Controllers;
use App\Crud\CertificateCrud;
use App\Crud\PageParameterCrud;
use App\Logic\AdminPages;
use App\Logic\ImageRepo;
use App\Logic\Order;
use App\Models\PageTranslation;
use Illuminate\Support\Facades\Validator;
use App\Crud\PageCrud;
use App\Models\Page;
use Illuminate\Http\Request;
use App\DataTables\PageDataTable;
use App\Logic\Slug;
use DB;

class PageController extends Controller
{

    use AdminPages;

    use AdminPages {
        AdminPages::__construct as private __TrConstruct;
    }

    const TRANSLATES = ['name','seo_title', 'parent_id','lang', 'slug', 'order', 'summary', 'content', 'forward_url', 'meta_description', 'meta_keywords'];

    private $crud,$paramCrud, $image, $requests, $title, $order, $route;

    public function __construct(Request $request, Order $order)
    {
        $this->__TrConstruct($request,$order);
        $this->order = $order;


        $this->crud = new PageCrud();
        $this->title = "Səhifələr";
        $this->paramCrud = new PageParameterCrud();

    }


    public function index(PageDataTable $dataTable)
    {
        return $dataTable->render('admin.pages', ['title' => $this->title, 'route' => $this->route]);
    }

}
