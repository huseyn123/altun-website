<?php

namespace App\Http\Controllers;
use App\Logic\Order;
use App\Models\PageTranslation;
use Illuminate\Support\Facades\Validator;
use App\Models\Page;
use Illuminate\Http\Request;
use App\Logic\Slug;
use DB;

class PageTranslationController extends Controller
{
    private $order, $requests;

    public function __construct(Request $request, Order $order)
    {
        $this->middleware('auth:admin');
        $this->middleware('ajax')->except(['order']);

        $this->requests = $request->except('_token', '_method');
        $this->order = $order;

        if (in_array(strtolower($request->method()), ['put', 'post', 'patch', 'delete'])) {
            clearCache('pages');
            clearCache('shops');
        }
    }


    public function store(Request $request)
    {
        $response = $this->validation($request->lang);

        if($response['code'] == 1){
            return $this->responseJson($response);
        }

        $this->requests['order'] = PageTranslation::max('order') + 1;
        $this->requests['slug'] = Slug::slugify($request->slug, $request->name, 'pageTranslation', $request->lang );


       $page = PageTranslation::create($this->requests);

        if ($request->has('tr_image')){
            PageTranslation::$width =  config('config.page_size.'.$page->template_id.'.width') ?? '10000';
            PageTranslation::$height = config('config.page_size.'.$page->template_id.'.height') ?? '10000';

            //thumb_size
            PageTranslation::$thumb_width =  config('config.thumb_page_size.'.$page->template_id.'.width') ?? '45';
            PageTranslation::$thumb_height = config('config.thumb_page_size.'.$page->template_id.'.height') ?? '100';

            PageTranslation::$template_id = $page->template_id;

            try {
                $page
                    ->addMedia($request->tr_image)
                    ->toMediaCollection();
            } catch (\Exception $e) {
                DB::rollback();
                return $this->errorDt($e->getMessage());
            }
        }

        return $this->responseJson($response);
    }


    public function update(Request $request, $id)
    {
        $page = PageTranslation::findOrFail($id);

        $response = $this->validation($page->lang, $id);

        if($response['code'] == 1){
            return $this->responseJson($response);
        }

        $this->requests['slug'] = Slug::slugify($request->slug, $request->name, 'pageTranslation', $request->lang );


        foreach($this->requests as $key => $put){
            $page->$key = $put;
        }
        $page->save();

        return $this->responseJson($response);
    }


    public function trash($id)
    {
        $page = PageTranslation::findOrFail($id);

        DB::beginTransaction();


        try{
            $page->delete();
        }
        catch(\Exception $e){
            DB::rollback();
            return $this->errorDt($e->getMessage());
        }


        try{
            $page->children()->delete();
        }
        catch(\Exception $e){
            DB::rollback();
            return $this->errorDt($e->getMessage());
        }


        DB::commit();

        $response = $this->responseDataTable(0,"", "#pages", "#modal-confirm");
        return $this->responseJson($response);
    }


    public function restore($id)
    {
        $page = PageTranslation::onlyTrashed()->findOrFail($id);

        DB::beginTransaction();


        try{
            $page->restore();
        }
        catch(\Exception $e){
            DB::rollback();
            return $this->errorDt($e->getMessage());
        }


        try{
            $page->children()->restore();
        }
        catch(\Exception $e){
            DB::rollback();
            return $this->errorDt($e->getMessage());
        }


        DB::commit();

        $response = $this->responseDataTable(0,"", "#pages", "#modal-confirm");
        return $this->responseJson($response);
    }


    public function destroy($id)
    {
        $page = PageTranslation::onlyTrashed()->findOrFail($id);

        $relatedPages = PageTranslation::where('page_id', $page->page_id)->withTrashed()->count();

        if($relatedPages == 1){
            Page::where('id', $page->page_id)->forceDelete();
        }
        else{
            $page->forceDelete();
        }

        $response = $this->responseDataTable(0,"", "#pages", "#modal-confirm");
        return $this->responseJson($response);
    }


    public function order()
    {
        $lang = request()->get('lang', 'az');

        return $this->order->get('pageTranslation', "sex", 'name', $lang, 5);
    }


    public function postOrder(Request $request)
    {
        return $this->order->post($request, 'pageTranslation', true);

    }


    private function validation($lang, $id = null)
    {
        $validation = Validator::make(request()->all(), PageTranslation::rules($lang, $id), PageTranslation::$messages);

        $response = $this->responseDataTable(0,"", "#pages", '#myModal');

        if($validation->fails()){
            $response = $this->errorDt($validation->errors()->first());
        }

        return $response;
    }


    private function errorDt($msg)
    {
        return $this->responseDataTable(1, $msg);
    }
}
