<?php

namespace App\Http\Controllers;
use App\Models\AuthorTranslation;
use App\Models\ProductAuthors;
use App\Models\ProductTranslation;
use Illuminate\Support\Facades\Validator;
use App\Models\Product;
use Illuminate\Http\Request;
use App\Logic\Slug;
use DB;

class ProductTranslationController extends Controller
{
    private $order, $requests;

    public function __construct(Request $request)
    {
        $this->middleware('auth:admin');
        $this->middleware('ajax')->except(['order']);

        $this->requests = $request->except('_token', '_method','product_authors');

        if (in_array(strtolower($request->method()), ['put', 'post', 'patch', 'delete'])) {
            clearCache('ages',false);
            clearCache('prouctsCount');
        }
    }


    public function store(Request $request)
    {
        $response = $this->validation($request->lang);

        if($response['code'] == 1){
            return $this->responseJson($response);
        }

        $this->requests['slug'] = Slug::slugify($request->slug, $request->name, 'productTranslation', $request->lang );


       $product = ProductTranslation::create($this->requests);

        $product_authors = $request->product_authors;

        if(!empty($product_authors))
        {

            $selectedAuthors = AuthorTranslation::whereIn('id', $product_authors)->pluck('id')->toArray();


            foreach($selectedAuthors as $list)
            {
                try{
                    ProductAuthors::create(['product_id' => $product->id, 'author_id' => $list]);
                }
                catch(\Exception $e){
                    DB::rollback();
                    throw new DataTableException($e->getMessage());
                }
            }
        }


        return $this->responseJson($response);
    }


    public function update(Request $request, $id)
    {
        $product = ProductTranslation::findOrFail($id);
        $authors = $request->product_authors;
        $currentAuthors = $product->product_authors('id')->pluck('author_id')->toArray();


        $response = $this->validation($product->lang, $id);

        if($response['code'] == 1){
            return $this->responseJson($response);
        }

        $this->requests['slug'] = Slug::slugify($request->slug, $request->name, 'productTranslation', $request->lang );


        foreach($this->requests as $key => $put){
            $product->$key = $put;
        }
        $product->save();

        if($authors != $currentAuthors){

            try{
                $product->product_authors('id')->delete();
            }
            catch(\Exception $e){
                DB::rollback();
                throw new DataTableException($e->getMessage());
            }

            if(!empty($authors))
            {

                $selectedAuthors = AuthorTranslation::whereIn('id', $authors)->pluck('id')->toArray();
//
//                foreach (array_diff($tags, $selectedTags) as $newTag){
//                    $newTag = Tag::create([
//                        'keyword' => $newTag,
//                        'name' => $newTag,
//                        'lang' => 'az'
//                    ]);
//
//                    $selectedTags[] = $newTag->id;
//                }

                foreach($authors as $list)
                {
                    try{
                        ProductAuthors::create(['product_id' => $product->id, 'author_id' => $list]);
                    }
                    catch(\Exception $e){
                        DB::rollback();
                        throw new DataTableException($e->getMessage());
                    }
                }

            }
        }


        return $this->responseJson($response);
    }


    public function trash($id)
    {
        $product = ProductTranslation::findOrFail($id);
        $product->delete();

        $response = $this->responseDataTable(0,"", "#products", "#modal-confirm");
        return $this->responseJson($response);
    }

    public function restore($id)
    {
        $producr = ProductTranslation::onlyTrashed()->findOrFail($id);
        $producr->restore();

        $response = $this->responseDataTable(0,"", "#products", "#modal-confirm");
        return $this->responseJson($response);
    }


    public function destroy($id)
    {
        $product =  ProductTranslation::onlyTrashed()->findOrFail($id);

        $relatedPages = ProductTranslation::where('product_id', $product->product_id)->withTrashed()->count();

        if($relatedPages == 1){
            Product::where('id', $product->product_id)->forceDelete();
        }
        else{
            $product->forceDelete();
        }

        $response = $this->responseDataTable(0,"", "#products", "#modal-confirm");
        return $this->responseJson($response);
    }

    private function validation($lang, $id = null)
    {
        $validation = Validator::make(request()->all(), ProductTranslation::rules($id), ProductTranslation::$messages);

        $response = $this->responseDataTable(0,"", "#products", '#myModal');

        if($validation->fails()){
            $response = $this->errorDt($validation->errors()->first());
        }

        return $response;
    }


    private function errorDt($msg)
    {
        return $this->responseDataTable(1, $msg);
    }
}
