<?php

namespace App\Http\Controllers;

use App\DataTables\TagDataTable;
use App\Exceptions\DataTableException;
use Illuminate\Http\Request;
use App\Models\Tag;
use Illuminate\Support\Facades\Validator;
use DB;

class TagController extends Controller
{
    public $title, $requests;

    public function __construct(Request $request)
    {
        $this->title = "Teqlər";
        $this->requests = $request->except('_token', '_method');

    }


    public function index(TagDataTable $dataTable)
    {
        return $dataTable->render('admin.tags', ['title' => $this->title]);
    }


    public function create()
    {
        return view("admin.tag.create",  ['title' => 'Yeni teq', 'route' => 'tag.store']);
    }


    public function edit($id)
    {
        $tag = Tag::findOrFail($id);
        $tags = Tag::where('keyword', $tag->keyword)->get()->toArray();

        return view('admin.tag.edit', ['info' => $tag, 'tags' => $tags, 'route' => ['tag.update', $tag->id] ]);
    }


    public function store(Request $request)
    {
        $names = $request->input('name');
        $keyword = $request->input('keyword');

        $response = $this->validation();

        if($response['code'] == 1){
            return $this->responseJson($response);
        }

        DB::beginTransaction();

        foreach(array_filter($names) as $lang => $name){

            try{
                Tag::create(['keyword' => $keyword, 'name' => $name, 'lang' => $lang]);
            }
            catch(\Exception $e){
                DB::rollback();
                throw new DataTableException($e->getMessage());
            }
        }

        DB::commit();

        return $this->responseJson($response);
    }


    public function update(Request $request, $id)
    {
        $tag = Tag::findOrFail($id);

        $names = $request->input('name');

        $response = $this->validation();

        if($response['code'] == 1){
            return $this->responseJson($response);
        }
        $tags = Tag::where('keyword', $tag->keyword)->pluck('lang')->toArray();


        DB::beginTransaction();


        foreach(array_filter($names) as $lang => $name){

            if(in_array($lang, $tags))
            {
                try{
                    Tag::where('keyword', $tag->keyword)->where('lang', $lang)->update(['name' => $name]);
                }
                catch(\Exception $e){
                    DB::rollback();
                    throw new DataTableException($e->getMessage());
                }
            }
            else{
                try{
                    Tag::create(['keyword' => $tag->keyword, 'name' => $name, 'lang' => $lang]);
                }
                catch(\Exception $e){
                    DB::rollback();
                    throw new DataTableException($e->getMessage());
                }
            }

        }

        DB::commit();

        return $this->responseJson($response);
    }


    public function destroy($id)
    {
        $tag = Tag::findOrFail($id);
        $tag->delete();

        $response = $this->responseDataTable(0,"", "#tags", "#modal-confirm");
        return $this->responseJson($response);
    }

    private function validation()
    {
        $validation = Validator::make(request()->all(), Tag::$rules, Tag::$messages);

        $response = $this->responseDataTable(0,"", "#tags", '#myModal');

        if($validation->fails()){
            $response = $this->responseDataTable(1, $validation->errors()->first());
        }

        return $response;
    }
}
