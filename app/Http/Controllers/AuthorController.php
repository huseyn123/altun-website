<?php

namespace App\Http\Controllers;
use App\Crud\AuthorParameterCrud;
use App\Crud\AuthorCrud;
use App\Models\AuthorTranslation;
use App\Models\PageTranslation;
use App\Models\Author;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\DataTables\AuthorDataTable;
use App\Logic\Slug;
use DB;

class AuthorController extends Controller
{
    private $crud, $requests, $title;

    public function __construct(AuthorCrud $crud,Request $request)
    {
        $this->middleware('auth:admin');
        $this->middleware('ajax')->except(['index']);

        $this->requests = $request->except('_token', '_method','image');
        $this->crud = $crud;
        $this->title = "Yazarlar";
    }


    public function index(AuthorDataTable $dataTable)
    {
        return $dataTable->render('admin.authors', ['title' => $this->title]);
    }

    public function create()
    {
        $fields = $this->crud->fields(null, 'create');

        return view('admin.page.create', ['title' => 'Yeni Yazar', 'fields' => $fields, 'route' => 'authors.store']);
    }


    public function store(Request $request)
    {
        if(count(config('app.locales')) == 1){
            $lang = app()->getLocale();
        }
        else{
            $lang = $request->lang;
        }

        $translates = ['author_id','name','lang','slug', 'summary', 'content','page_id',];

        // check Validation
        $validation = Validator::make($request->all(), Author::rules(null), Author::$messages);
        $response = $this->validation($validation, $lang);

        if($response['code'] == 1){
            return $this->responseJson($response);
        }


        $parent = PageTranslation::findOrFail($request->page_id);
        $lang = $parent->lang;

        $this->requests['lang'] = $lang;
        $this->requests['slug'] = Slug::slugify($request->slug, $request->name, 'authorTranslation', $lang);

        //inputs for models
        $authorInputs = array_diff_key($this->requests, array_flip($translates));
        $translationInputs = array_diff_key($this->requests, $authorInputs);

        //store
        DB::beginTransaction();


        try{
            $author = Author::create($authorInputs);
        }
        catch(\Exception $e){
            DB::rollback();
            return $this->errorDt($e->getMessage());
        }

        try{
            $translationInputs['author_id'] = $author->id;
            AuthorTranslation::create($translationInputs);
        }
        catch(\Exception $e){
            DB::rollback();
            return $this->errorDt($e->getMessage());
        }

        if ($request->has('image')){

            try {
                $author
                    ->addMedia($request->image)
                    ->toMediaCollection();
            } catch (\Exception $e) {
                DB::rollback();
                return $this->errorDt($e->getMessage());
            }
        }

        DB::commit();

        return $this->responseJson($response);
    }


    public function edit($id)
    {
        $parameterCrud = new AuthorParameterCrud();
        $fields = [];
        $langs = [];
        $keys = [];



        $authors = Author::join('author_translations as at', 'at.author_id', '=', 'authors.id')
            ->where('at.id', $id)
            ->select(
                'at.*',
                'at.id as tid',
                'authors.id'
            )
            ->firstOrFail();


        if(count(config('app.locales')) == 1){
            return $this->singleEdit($authors);
        }

        $relatedAuthor = $authors->relatedAuthors;
        $parameters = $parameterCrud->fields('edit', $authors);

        foreach ($relatedAuthor as $rel){
            $fields[$rel->lang] = $this->crud->fields($rel->lang, 'edit', $rel);
            $langs[$rel->lang] = $rel->lang;
        }

        foreach(array_diff_key(config('app.locales'), $langs) as $key => $locale){
            $fields[$key] = $this->crud->fields( $key, 'edit', null);
        }

        return view('admin.page.edit-withlocale', ['info' => $authors, 'langs' => $langs, 'parameters' => $parameters, 'pageId' => $authors->author_id, 'fields' => $fields, 'relatedPage' => $relatedAuthor,'model' => 'authors', 'route' => 'authorTranslation','editor2' =>true]);
    }


    private function singleEdit($page)
    {
        $fields = $this->crud->fields(null,'edit', $page);

        $keys = array_filter(explode(",", $page->meta_keywords));

        return view('admin.page.edit', ['info' => $page, 'fields' => $fields, 'keys' => $keys, 'model' => 'page']);
    }


    public function update(Request $request, $id)
    {
        $author = Author::findOrFail($id);

        $validation = Validator::make($request->all(), Author::parameterRules(), Author::$messages);
        $response = $this->validation($validation, $id);

        if($response['code'] == 1){
            return $this->responseJson($response);
        }

        foreach($this->requests as $key => $put){
            $author->$key = $put;
        }

        $author->save();

        if($request->hasFile('image')){
            try{

                $author->clearMediaCollection();  //delete old file
                $author->addMedia($request->image)
                    ->toMediaCollection();
            }
            catch(\Exception $e){
                DB::rollback();
                return $this->errorDt($e->getMessage());
            }
        }


        return $this->responseJson($response);
    }


    public function updateSingle(Request $request, $id)
    {
        $authorTranslation = AuthorTranslation::findOrFail($id);

        $translates = ['name','lang', 'slug','content',];

        // check Validation
        $validation = Validator::make($this->requests, Author::rules($authorTranslation->lang, $id), Author::$messages);
        $response = $this->validation($validation, $authorTranslation->lang);

        if($response['code'] == 1){
            return $this->responseJson($response);
        }


        //inputs for models
        $authorInput = array_diff_key($this->requests, array_flip($translates));
        $translationInputs = array_diff_key($this->requests, $authorInput);

        //store
        DB::beginTransaction();


        try{
            Author::where('id', $authorTranslation->author_id)->update($authorInput);
        }
        catch(\Exception $e){
            DB::rollback();
            return $this->errorDt($e->getMessage());
        }

        try{
            foreach($translationInputs as $key => $put){
                $authorInput->$key = $put;
            }
            $authorTranslation->save();
        }
        catch(\Exception $e){
            DB::rollback();
            return $this->errorDt($e->getMessage());
        }

        DB::commit();

        return $this->responseJson($response);
    }




    private function validation($validation, $lang, $id = null)
    {
        $response = $this->responseDataTable(0,"", "#authors", '#myModal');

        if($validation->fails()){
            $response = $this->errorDt($validation->errors()->first());
        }

        return $response;
    }


    private function errorDt($msg)
    {
        return $this->responseDataTable(1, $msg);
    }
}
