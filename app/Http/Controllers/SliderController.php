<?php

namespace App\Http\Controllers;

use App\Logic\ImageRepo;
use Illuminate\Http\Request;
use App\Models\Slider;
use App\DataTables\SliderDataTable;
use App\Crud\SliderCrud;
use DB;
use Illuminate\Support\Facades\Validator;

class SliderController extends Controller
{
    private $crud, $requests;
    public $title;

    public function __construct(Request $request, SliderCrud $crud)
    {
        $this->middleware('auth:admin');
        $this->middleware('ajax')->except(['index']);

        $this->crud = $crud;
        $this->requests = $request->except('_token', '_method', 'image','m_image');
        $this->title = "Slider";

        if (in_array(strtolower($request->method()), ['put', 'post', 'patch', 'delete'])) {
            clearCache('slider');
            clearCache('project');
        }
    }


    public function index(SliderDataTable $dataTable)
    {
        return $dataTable->render("admin.sliders", ['title' => $this->title]);
    }


    public function create()
    {
        $fields = $this->crud->fields('create');

        return view("admin.dt.create",  ['fields' => $fields, 'title' => 'Yeni Slider', 'route' => 'slider.store']);
    }


    public function store(Request $request)
    {
        $response = $this->validation('required|',$request->parent_id);

        if($response['code'] == 1){
            return $this->responseJson($response);
        }

        DB::beginTransaction();

        $slider = Slider::create($this->requests);

        $slider
            ->addMedia($request->image)
         //   ->withResponsiveImages()
            ->toMediaCollection();


        DB::commit();

        return $this->responseJson($response);
    }



    public function edit($id)
    {
        $data = Slider::findOrFail($id);

        $fields = $this->crud->fields('edit', $data);

        return view('admin.dt.edit', ['title' => 'Düzəliş et', 'fields' => $fields, 'data' => $data, 'route' => ['slider.update', $id]]);
    }


    public function update(Request $request, $id)
    {
        $img = Slider::findOrFail($id);

        $response = $this->validation(null,$request->parent_id);

        if($response['code'] == 1){
            return $this->responseJson($response);
        }

        if($request->hasFile('image')){

            $img->clearMediaCollection();  //delete old file
            $img->addMedia($request->image)
              //  ->withResponsiveImages()
                ->toMediaCollection();
        }

        foreach($this->requests as $key => $put){
            $img->$key = $put;
        }

        $img->save();

        return $this->responseJson($response);
    }

    public function trash($id)
    {
        $data = Slider::findOrFail($id);
        $data->delete();

        $response = $this->responseDataTable(0,"", "#sliders");
        return $this->responseJson($response);
    }


    public function restore($id)
    {
        $member = Slider::onlyTrashed()->findOrFail($id);

        $member->restore();

        $response = $this->responseDataTable(0,"", "#sliders");
        return $this->responseJson($response);
    }


    public function destroy($id)
    {
        $slider = Slider::onlyTrashed()->findOrFail($id);

        $slider->forceDelete();

        $response = $this->responseDataTable(0,"", "#sliders", "#modal-confirm");
        return $this->responseJson($response);
    }

    protected function resize()
    {
        $resizeImage = ['resize' => ['fit' => true, 'size' => [1140, null]], 'thumb' => ['fit' => true, 'size' => [300, 100]] ];

        return $resizeImage;
    }

    private function validation($required = 'required|',$parent_id = null)
    {
        $validation = Validator::make(request()->all(), Slider::rules($required,$parent_id), Slider::$messages);

        $response = $this->responseDataTable(0,"", "#sliders", '#myModal');

        if($validation->fails()){
            $response = $this->responseDataTable(1, $validation->errors()->first());
        }

        return $response;
    }
}