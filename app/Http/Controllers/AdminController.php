<?php

namespace App\Http\Controllers;

use App\Crud\AdminCrud;
use App\DataTables\AdminsDataTable;
use App\Admin;
use App\Logic\ImageRepo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Hash;

class AdminController extends Controller
{
    private $requests, $crud, $title;

    public function __construct(Request $request, AdminCrud $crud)
    {
        $this->middleware('auth:admin');
        $this->middleware('ajax')->except(['index', 'profile', 'updatePhoto']);

        $this->requests = $request->except('_token', '_method', 'type');
        $this->crud = $crud;
        $this->title = 'Adminlər';
    }


    public function index(AdminsDataTable $dataTable)
    {
        return $dataTable->render('admin.admins', ['title' => $this->title]);
    }

    public function profile()
    {
        $user = auth()->guard('admin')->user();

        $fields = $this->crud->fields('profile', $user, false);

        return view('admin.profile', ['fields' => $fields, 'title' => 'Profil', 'disabled' => false]);
    }


    public function create()
    {
        $fields = $this->crud->fields('create');

        return view('admin.dt.create', ['fields' => $fields, 'title' => 'Yeni admin', 'route' => 'admins.store']);
    }


    public function store(Request $request)
    {
        $response = $this->validation(1);

        if($response['code'] == 1){
            return $this->responseJson($response);
        }

        $this->requests['password'] =  Hash::make($request->password);

        Admin::create($this->requests);

        return $this->responseJson($response);
    }


    public function edit($id)
    {
        $data = Admin::findOrFail($id);

        $fields = $this->crud->fields('edit', $data);

        return view('admin.dt.edit', ['data' => $data, 'fields' => $fields, 'route' => ['admins.update', $id] ]);
    }


    public function update(Request $request, $id)
    {
        $data = Admin::findOrFail($id);

        $type = $request->input('type', 1);

        $response = $this->validation($type, $id);

        if($response['code'] == 1){
            return $this->responseJson($response);
        }

        foreach($this->requests as $key => $put){
            $data->$key = $put;
        }

        $data->save();

        return $this->responseJson($response);
    }


    public function updatePassword(Request $request)
    {
        $user = auth()->guard('admin')->user();

        $inputs = [
            'current_password' => 'required|current_password_match',
            'password'     => 'required|min:6|confirmed',
        ];

        $validation = Validator::make($this->requests, $inputs);

        if($validation->fails()){
            $response = $this->responseDataTable(1, $validation->errors()->first());
        }
        else{
            $response = $this->responseDataTable(0, "Şifrəniz yeniləndi");
            $user->passsword = Hash::make($inputs['password']);
            $user->save();
        }

        return $this->responseJson($response);
    }


    public function updatePhoto(Request $request)
    {
        $image = new ImageRepo;
        $resizeImage = ['resize' => ['fit' => true, 'size' => [215, 215]], 'thumb' => null ];
        $user = auth()->guard('admin')->user();

        $inputs = [
            'photo' => 'required|mimes:png,gif,jpeg,jpg,bmp|max:10000'
        ];

        $request->validate($inputs);

        $upload = $image->store($request->photo, $resizeImage);
        $image->deleteFile($user->image); //delete old file

        $user->image = $upload;
        $user->save();

        return redirect()->back();
    }


    public function trash($id)
    {
        $data = Admin::findOrFail($id);

        $admins = Admin::count();

        if($admins == 1){
            $response = $this->responseDataTable(1,"Başqa admin olmadığı üçün əməliyyat baş tutmadı");
        }
        else{
            $data->delete();
            $response = $this->responseDataTable(0,"", "#admins");
        }

        return $this->responseJson($response);
    }


    public function restore($id)
    {
        $data = Admin::onlyTrashed()->findOrFail($id);

        $data->restore();

        $response = $this->responseDataTable(0, "", "#admins");
        return $this->responseJson($response);
    }


    /*public function destroy($id)
    {
        $data = Admin::onlyTrashed()->findOrFail($id);

        $data->forceDelete();

        $response = $this->responseDataTable(0, "", "#admins", "#modal-confirm");
        return $this->responseJson($response);
    }*/


    private function validation($type = 1, $id = null)
    {
        $validation = Validator::make($this->requests, Admin::rules($id), Admin::$messages);

        if($type == 1){
            $response = $this->responseDataTable(0,"", "#admins", '#myModal');
        }
        else{
            $response = $this->responseDataTable(0,"");
        }

        if($validation->fails()){
            $response = $this->responseDataTable(1, $validation->errors()->first());
        }

        return $response;
    }
}
