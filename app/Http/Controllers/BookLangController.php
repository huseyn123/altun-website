<?php

namespace App\Http\Controllers;

use App\DataTables\BookLangDataTable;
use App\Logic\NoCrud;
use Illuminate\Http\Request;
use App\Models\BookLang;
use Illuminate\Support\Facades\Validator;
use DB;

class BookLangController extends Controller
{
    use NoCrud;
    private $crud,$requests,$title,$route,$form_data;

    public function __construct(Request $request)
    {
        $this->middleware('auth:admin');
        $this->middleware('ajax')->except(['index']);

        $this->title = "Kitab Dilləri";
        $this->route = "book_lang";
        $this->view = "nocrud";
        $this->form_data = [['name'=>'title']];
        $this->model = 'App\Models\BookLang';
        $this->requests = $request->except('_token', '_method');

    }


    public function index(BookLangDataTable $dataTable)
    {
        return $dataTable->render('admin.main', ['title' => $this->title,'route' =>$this->route]);
    }



}
