<?php

namespace App\Http\Controllers;
use App\Crud\ArticleParameterCrud;
use App\Crud\EventCrud;
use App\Crud\EventParameterCrud;
use App\DataTables\ArticleDataTable;
use App\DataTables\EventDataTable;
use App\Logic\Articles;
use App\Crud\ArticleCrud;
use Illuminate\Http\Request;

class ArticleController extends Controller
{
    use Articles;

    private $crud, $requests, $title,$route,$create_name;

    public function __construct(ArticleCrud $crud, Request $request)
    {
        $this->middleware('auth:admin');
        $this->middleware('ajax')->except(['index']);

        $this->requests = $request->except('_token', '_method', 'image');


        $this->route = 'articles';
        $this->title = "Xəbərlər";
        $this->create_name = "Xəbər";
        $this->crud = new ArticleCrud();
        $this->paramCrud = new ArticleParameterCrud();
    }


    public function index(ArticleDataTable $dataTable)
    {
        return $dataTable->render('admin.articles', ['title' => $this->title,'route' => $this->route]);

    }


}
