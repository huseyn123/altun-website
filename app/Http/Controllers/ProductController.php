<?php

namespace App\Http\Controllers;
use App\Crud\ProductCrud;
use App\Crud\ProductParameterCrud;
use App\DataTables\ProductDataTable;
use App\Logic\Slug;
use App\Models\AuthorTranslation;
use App\Models\BookEducation;
use App\Models\Page;
use App\Models\PageTranslation;
use App\Models\Product;
use App\Models\ProductAuthors;
use App\Models\ProductTag;
use App\Models\Tag;
use App\Models\ProductTranslation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{
    private $crud, $requests, $title,$route,$create_name;

    const TRANSLATES = ['name','page_id','product_id','author_id','lang', 'slug','summary', 'content',
        'meta_description', 'meta_keywords','image_meta_title','product_lang','product_education'];

    public function __construct(ProductCrud $crud, Request $request)
    {
        $this->middleware('auth:admin');
        $this->middleware('ajax')->except(['index']);

        $this->requests = $request->except('_token', '_method', 'image','tag','product_authors');


        $this->route = 'product';
        $this->title = "Məhsullar";
        $this->create_name = "Məhsul";
        $this->crud = new ProductCrud();
        $this->paramCrud = new ProductParameterCrud();

        if (in_array(strtolower($request->method()), ['put', 'post', 'patch', 'delete'])) {
            clearCache('ages',false);
            clearCache('prouctsCount');
        }
    }


    public function index(ProductDataTable $dataTable)
    {

        $categories = Page::join('page_translations as pt', 'pages.id', '=', 'pt.page_id')
            ->where('pt.lang', 'az')
            ->whereNull('pt.deleted_at')
            ->whereNotIn('pages.visible', [0])
            ->where('pages.template_id',6)
            ->orderBy('pt.order', 'asc')
            ->pluck('pt.name','pt.id');

        return $dataTable->route($this->route)->render('admin.products', ['title' => $this->title,'route' => $this->route,'categories' => $categories]);
    }

    public function create()
    {
        $fields = $this->crud->fields(null, 'create');

        return view('admin.page.create', ['route' => "$this->route.store", 'fields' => $fields, 'title' => 'Yeni']);
    }

    public function store(Request $request)
    {
        if (count(config('app.locales')) == 1) {
            $lang = app()->getLocale();
        } else {
            $lang = $request->lang;
        }

        $translates = self::TRANSLATES;
        $tags = $request->tag;
        $product_authors = $request->product_authors;

        // check Validation
        $validation = Validator::make($request->all(), Product::rules(null), Product::$messages);
        $response = $this->validation($validation, $lang);

        if ($response['code'] == 1) {
            return $this->responseJson($response);
        }

        $parent = PageTranslation::findOrFail($request->page_id);
        $lang = $parent->lang;


        // apply
        $this->requests['lang'] = $lang;
        $this->requests['slug'] = Slug::slugify($request->slug, $request->name, 'productTranslation', $lang);

        //inputs for models
        $productInputs = array_diff_key($this->requests, array_flip($translates));
        $translationInputs = array_diff_key($this->requests, $productInputs);

        //store
        DB::beginTransaction();


        try {
            $product = Product::create($productInputs);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->errorDt($e->getMessage());
        }

        try {
            $translationInputs['product_id'] = $product->id;
            $product_translation = ProductTranslation::create($translationInputs);
        } catch (\Exception $e) {
            DB::rollback();
            return $this->errorDt($e->getMessage());
        }


        if ($request->has('image')){

            try {
                $product
                    ->addMedia($request->image)
                    ->toMediaCollection();
            } catch (\Exception $e) {
                DB::rollback();
                return $this->errorDt($e->getMessage());
            }
        }

        if(!empty($product_authors))
        {

            $selectedAuthors = AuthorTranslation::whereIn('id', $product_authors)->pluck('id')->toArray();


            if($lang != 'az'){
                $selectRelationLang = AuthorTranslation::whereIn('id', $product_authors)->pluck('author_id')->toArray();
                $selectedAuthors = AuthorTranslation::where('lang',$lang)->whereIn('author_id', $selectRelationLang)->pluck('id')->toArray();
            }

            foreach($selectedAuthors as $list)
            {
                try{
                    ProductAuthors::create(['product_id' => $product_translation->id, 'author_id' => $list]);
                }
                catch(\Exception $e){
                    DB::rollback();
                    throw new DataTableException($e->getMessage());
                }
            }
        }


        if(!empty($tags))
        {

            $selectedTags = Tag::whereIn('id', $tags)->pluck('id')->toArray();

            foreach (array_diff($tags, $selectedTags) as $newTag){
                $newTag = Tag::create([
                    'keyword' => $newTag,
                    'name' => $newTag,
                    'lang' => $lang
                ]);

                $selectedTags[] = $newTag->id;
            }

            foreach($selectedTags as $list)
            {
                try{
                    ProductTag::create(['product_id' => $product->id, 'tag_id' => $list]);
                }
                catch(\Exception $e){
                    DB::rollback();
                    throw new DataTableException($e->getMessage());
                }
            }
        }

        DB::commit();

        return $this->responseJson($response);
    }

    public function edit($id)
    {
        $parameterCrud = $this->paramCrud;

        $fields = [];
        $langs = [];
        $keys = [];

        $product = Product::join('product_translations as prt', 'prt.product_id', '=', 'products.id')
            ->where('prt.id', $id)
            ->whereNull('prt.deleted_at')
            ->select(
                'prt.*',
                'prt.id as tid',
                'products.id',
                'products.price',
                'products.order_link',
                'products.page_number',
                'products.size',
                'products.bar_code',
                'products.age_id',
                'products.featured'
            )
            ->firstOrFail();

        if(count(config('app.locales')) == 1){
            return $this->singleEdit($product);
        }

        $relatedPage = $product->relatedPages;
        $parameters = $parameterCrud->fields('edit', $product);

        foreach ($relatedPage as $rel){
            $fields[$rel->lang] = $this->crud->fields($rel->lang, 'edit', $rel);
            $langs[$rel->lang] = $rel->lang;
            $keys[$rel->lang] = array_filter(explode(",", $rel->meta_keywords));
            $authors[$rel->lang] = $rel->product_authors->pluck('author_id')->toArray();
            $education[$rel->lang] = BookEducation::whereIn('id',explode(",", $rel->product_education))->pluck('id')->toArray();
        }

        foreach(array_diff_key(config('app.locales'), $langs) as $key => $locale){
            $fields[$key] = $this->crud->fields( $key, 'edit', null);
            $keys[$key] = [];
            $authors[$key] = [];
            $education[$key] = [];
        }

//        die(print_r($authors));
        return view('admin.page.edit-withlocale', ['info' => $product,'langs' => $langs, 'parameters' => $parameters,'fields' => $fields, 'relatedPage' => $relatedPage,'keys' => $keys, 'authors' => $authors,'education'=>$education,'model' => 'product', 'route' => 'productTranslation']);
    }

    private function singleEdit($page)
    {
        $fields = $this->crud->fields(null,'edit', $page);

        return view('admin.page.edit', ['info' => $page, 'fields' => $fields, 'model' => 'product']);
    }

    public function update(Request $request, $id)
    {
        $product = Product::findOrFail($id);

        $tags = $request->tag;

        $validation = Validator::make($request->all(), Product::parameterRules(), Product::$messages);
        $response = $this->validation($validation, $id);

        if($response['code'] == 1){
            return $this->responseJson($response);
        }

        $this->requests['featured'] = $request->featured;
        $currentTags = $product->tags('id')->pluck('tag_id')->toArray();


        foreach($this->requests as $key => $put){
            $product->$key = $put;
        }

        $product->save();



        if($request->hasFile('image')){
            try{

                $product->clearMediaCollection();  //delete old file
                $product->addMedia($request->image)
                    ->toMediaCollection();
            }
            catch(\Exception $e){
                DB::rollback();
                return $this->errorDt($e->getMessage());
            }
        }


        if($tags != $currentTags){

            try{
                $product->tags('id')->delete();
            }
            catch(\Exception $e){
                DB::rollback();
                throw new DataTableException($e->getMessage());
            }

            if(!empty($tags))
            {

                $selectedTags = Tag::whereIn('id', $tags)->pluck('id')->toArray();

                foreach (array_diff($tags, $selectedTags) as $newTag){
                    $newTag = Tag::create([
                        'keyword' => $newTag,
                        'name' => $newTag,
                        'lang' => 'az'
                    ]);

                    $selectedTags[] = $newTag->id;
                }

                foreach($selectedTags as $list)
                {
                    try{
                        ProductTag::create(['product_id' => $product->id, 'tag_id' => $list]);
                    }
                    catch(\Exception $e){
                        DB::rollback();
                        throw new DataTableException($e->getMessage());
                    }
                }

            }
        }

        return $this->responseJson($response);
    }

//    public function updateSingle(Request $request, $id)
//    {
//        $productTranslation = PageTranslation::findOrFail($id);
//        $product = Product::findOrFail($productTranslation->product_id);
//
//        $translates = self::TRANSLATES;
//
//        // check Validation
//        $validation = Validator::make($this->requests, Product::rules($id), Product::$messages);
//        $response = $this->validation($validation, $productTranslation->lang);
//
//        if($response['code'] == 1){
//            return $this->responseJson($response);
//        }
//
//        // apply
//        $this->requests['lang'] = $productTranslation->lang;
//        $this->requests['slug'] = Slug::slugify($request->slug, $request->name, 'productTranslation', $productTranslation->lang );
//
//        //inputs for models
//        $productInputs = array_diff_key($this->requests, array_flip($translates));
//        $translationInputs = array_diff_key($this->requests, $productInputs);
//
//        //store
//        DB::beginTransaction();
//
//
//        try{
//            foreach($productInputs as $key => $put){
//                $product->$key = $put;
//            }
//            $product->save();
//        }
//        catch(\Exception $e){
//            DB::rollback();
//            return $this->errorDt($e->getMessage());
//        }
//
//        try{
//            foreach($translationInputs as $key => $put){
//                $productTranslation->$key = $put;
//            }
//            $productTranslation->save();
//        }
//        catch(\Exception $e){
//            DB::rollback();
//            return $this->errorDt($e->getMessage());
//        }
//
//
//        DB::commit();
//
//        return $this->responseJson($response);
//    }

    public function fastUpdate($id, Request $request)
    {
        $value = $request->value;
        $type = $request->type;

        $rules = [
            'name' =>       ['value' => "required"],
            'slug' =>       ['value' => "required|unique:product_translations,slug,'.$id"],
            'status' =>     ['value' => "required|boolean"],
        ];

        $validation = Validator::make($request->all(), $rules[$type],Product::$messages);

        if($validation->errors()->first()){
            throw new DataTableException($validation->errors()->first());
        }



        if($type == 'name' || $type == 'slug'){
            $data = ProductTranslation::findOrFail($id);
        }
        else{
            $data = Product::findOrFail($id);
        }

        $data->{$type} = $value;
        $data->save();

        return $this->responseSuccess();
    }

    private function validation($validation, $lang, $id = null)
    {
        $response = $this->responseDataTable(0,"", "#products", '#myModal');

        if($validation->fails()){
            $response = $this->errorDt($validation->errors()->first());
        }

        return $response;
    }

    private function errorDt($msg)
    {
        return $this->responseDataTable(1, $msg);
    }


}
