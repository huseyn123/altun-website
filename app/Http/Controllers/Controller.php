<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;


    protected function headers()
    {
        return ['Content-type'=> 'application/json; charset=utf-8'];
    }

    protected function responseDataTable($code, $msg, $draw=false, $close=false, $data = false)
    {
        return array("code" => $code, "msg" => $msg, "draw" => $draw, "close" => $close, "data" => $data);
    }

    protected function responseJson($response)
    {
        return response()->json($response, 200, $this->headers(), JSON_FORCE_OBJECT, JSON_UNESCAPED_UNICODE);
    }

    protected function validateResponse($request, $response)
    {
        if($request->ajax()){
            return $this->responseJson($response);
        }
        else{
            return redirect()->back()->withInput($request->input())->withErrors(["errors" => $response['msg']]);
        }

    }
}
