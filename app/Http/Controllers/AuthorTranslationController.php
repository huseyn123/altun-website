<?php

namespace App\Http\Controllers;
use App\Models\Author;
use App\Models\AuthorTranslation;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Logic\Slug;
use DB;

class AuthorTranslationController extends Controller
{
    private $requests;

    public function __construct(Request $request)
    {
        $this->middleware('auth:admin');
        $this->middleware('ajax');
        $this->requests = $request->except('_token', '_method');

    }


    public function store(Request $request)
    {
        $response = $this->validation($request->lang);

        if($response['code'] == 1){
            return $this->responseJson($response);
        }

        $this->requests['slug'] = Slug::slugify($request->slug, $request->name, 'authorTranslation', $request->lang );


        DB::beginTransaction();

        $author = AuthorTranslation::create($this->requests);

        DB::commit();

        return $this->responseJson($response);
    }


    public function update(Request $request, $id)
    {
        $author = AuthorTranslation::findOrFail($id);

        $response = $this->validation($author->lang, $id);

        if($response['code'] == 1){
            return $this->responseJson($response);
        }

        $this->requests['slug'] = Slug::slugify($request->slug, $request->name, 'authorTranslation', $request->lang );


        DB::beginTransaction();


        foreach($this->requests as $key => $put){
            $author->$key = $put;
        }

        $author->save();


        DB::commit();

        return $this->responseJson($response);
    }


    public function trash($id)
    {
        $author = AuthorTranslation::findOrFail($id);
        $author->delete();

        $response = $this->responseDataTable(0,"", "#authors", "#modal-confirm");
        return $this->responseJson($response);
    }


    public function restore($id)
    {
        $author = AuthorTranslation::onlyTrashed()->findOrFail($id);
        $author->restore();

        $response = $this->responseDataTable(0,"", "#authors", "#modal-confirm");
        return $this->responseJson($response);
    }

    public function destroy($id)
    {
        $author =  AuthorTranslation::onlyTrashed()->findOrFail($id);

        $relatedPages = AuthorTranslation::where('author_id', $author->author_id)->withTrashed()->count();

        if($relatedPages == 1){
            Author::where('id', $author->author_id)->forceDelete();
        }
        else{
            $author->forceDelete();
        }

        $response = $this->responseDataTable(0,"", "#authors", "#modal-confirm");
        return $this->responseJson($response);
    }



    private function validation($id = null)
    {
        $validation = Validator::make(request()->all(), AuthorTranslation::rules($id), AuthorTranslation::$messages);

        $response = $this->responseDataTable(0,"", "#authors", '#myModal');

        if($validation->fails()){
            $response = $this->errorDt($validation->errors()->first());
        }

        return $response;
    }


    private function errorDt($msg)
    {
        return $this->responseDataTable(1, $msg);
    }
}
