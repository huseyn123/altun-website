<?php

namespace App\Http\Controllers\Web;

use App\Logic\Share;
use App\Http\Controllers\Controller;
use App\Models\Banner;
use App\Models\Page;
use App\Models\Article;
use App\Models\Product;
use DB;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    use Share;

    public function __construct()
    {
        $this->middleware('web');
        $this->middleware('ajax')->only(['findBranch']);
        $this->loadData();
    }

    public function search()
    {
       $keyword = trim(strip_tags(request()->get('keyword')));

       $pages = Page::join('page_translations as pt', 'pages.id', '=', 'pt.page_id')
           ->where('pt.lang',  $this->lang)
           ->whereNull('pt.deleted_at')
           ->where('pt.name', 'like', "%$keyword%")
           ->whereNotIn('pages.template_id',[2,5])
           ->whereNotIn('pages.visible', [0])
           ->orderBy('pt.order', 'asc')
           ->select('pages.visible','pages.id', 'pt.id as tid','pt.name','pt.slug','pt.content','pt.summary')
           ->paginate(10);

        if(!$keyword){
            return redirect()->route('home');
        }

        if (request()->ajax()) {
            $view = view('web.elements.search_result', ['pages' => $pages] )->render();
            return response()->json(['html'=>$view,'pages' => $pages]);
        }

        return view("web.search", ['keyword' => $keyword,'pages' => $pages]);
    }

    public function productSearch(Request $request)
    {

            $key = $request->search;
            if($key){
                $products =Product::join('product_translations as ptr','ptr.product_id','products.id')
                    ->join('page_translations as pt', 'pt.id', '=', 'ptr.page_id')
                    ->join('pages', 'pages.id', '=', 'pt.page_id')
                    ->whereNull('ptr.deleted_at')
                    ->whereNull('pt.deleted_at')
                    ->where('ptr.lang',$this->lang)
                    ->whereIn('pages.template_id',[6])
                    ->where('ptr.name', 'like', "%$key%")
                    ->orWhereHas('productTagsSearch', function($query) use($key) {
                        $query->join('product_translations as pt2', 'pt2.product_id', '=', 'product_tags.product_id')
                            ->whereNull('pt2.deleted_at')
                            ->where('pt2.lang', $this->lang)
                            ->where('tags.name','like', "%$key%")
                            ->where('tags.lang',$this->lang);
                    })
                    ->GroupBy('products.id')
                    ->select('ptr.*','ptr.id as tid','pt.slug as page_slug','pt.name as category_name','products.id','products.price','products.bar_code')
                    ->with(['rel_author','media'])
                    ->limit(50)
                    ->get();

            }else{
                $products = null;
            }

        return view("web.product_search_html", ['products' => $products,'key' => $key]);


//        $view = view('web.elements.product_search',['search_products' => $products])->render();
//            return response()->json(['html'=>$view,'result' =>1,'lang'=>$this->lang]);


    }

}
