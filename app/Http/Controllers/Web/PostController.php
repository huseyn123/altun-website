<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Logic\Share;
use App\Mail\ContactForm;
use App\Models\Config;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Mail\ApplicationForm;
use Mail;
 use DB;

class PostController extends Controller
{

    use Share;

    public function __construct(Request $request)
    {
        $this->loadData();
//        $this->middleware('ajax');
    }

    public function store(Request $request)
    {
        $method = $request->input('method');

        $rules = [
            'name' => 'required',
            'surname' => 'required',
            'email' => 'required|email',
            'subject' => 'required',
        ];


        return $this->$method($request,$method,$rules);

    }


    public function contact($request,$post_page,$mainRules)
    {

        $rules = [
            'message' => 'required|min:1,max:500',
        ];

        $rules = array_merge($mainRules,$rules);

        return $this->sendMail($request->all(),$rules,[], $post_page);
    }

    public function store_location_surver($request,$post_page,$mainRules)
    {
        $rules = [
            'address' => 'required',
            'area' => 'required',
            'depot' => 'required',
            'parking' => 'required',
            'message' => 'required|min:10,max:500',
            'image' =>'required|mimes:png,zip,pdf|max:10000',
        ];

        $rules = array_merge($mainRules,$rules);

        return $this->sendMail($request->all(),$rules,[$request->file('image')], $post_page);
    }


    public function product_offer_survey($request,$post_page,$mainRules)
    {
        $rules = [
            'company_name' => 'required',
            'brend_name' => 'required',
            'product_name' => 'required',
            'message' => 'required|min:10,max:500',
            'image' =>'required|mimes:png,zip,pdf|max:1000',
        ];

        $rules = array_merge($mainRules,$rules);

        return $this->sendMail($request->all(),$rules,[$request->file('image')], $post_page);
    }

    public function product($request,$post_page,$mainRules)
    {
        $rules = [
            'subject' => 'required',
            'message' => 'required'
        ];

        $rules = array_merge($mainRules,$rules);

        return $this->sendMail($request->all(),$rules,[], $post_page);
    }




    private function sendMail($form,$rules,$files, $title,$send_mail = null)
    {
        $email = Config::where('key', 'contact_email')->firstOrFail();

        $subject = $this->dictionary[$title.'_subject'] ?? 'AAAAAAA';

        $validation = Validator::make($form , $rules);

        if($validation->fails()){
            $response = $this->responseDataTable(1, $validation->errors()->first());
            return $this->responseJson($response);
        }

        try {
            Mail::to($email['value'])->send(new ApplicationForm($form,'contact',array_filter($files),$subject));

            $response = $this->responseDataTable(0, 'Succes');
            return $this->responseJson($response);

        } catch (\Exception $e) {
            $response = $this->responseDataTable(1, $e->getMessage());
            return $this->responseJson($response);
        }


    }

}
