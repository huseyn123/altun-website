<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Logic\Menu;
use App\Logic\Share;
use App\Logic\WebCache;
use App\Models\AuthorTranslation;
use App\Models\Config;
use App\Models\Page;
use App\Logic\Pages;
use App\Models\Product;
use App\Models\ProductAge;
use App\Models\Slider;
use App\Models\Author;
use Illuminate\Support\Facades\Validator;
use DB;
use Illuminate\Http\Request;
use App\Models\Dictionary;
use Mail;
use App\Mail\ContactForm;


class PageController extends Controller
{
    use Share, Pages;

    public function __construct()
    {
        $this->middleware('web');
        $this->middleware('ajax')->only(['postContact']);

        $this->loadData();
    }

    public function index()

    {

        $paginate = request()->get('page') ?? 1;
        $limit = request()->get('limit') ?? 24;//limit

        $products = Product::join('product_translations as ptr','ptr.product_id','products.id')
            ->join('page_translations as pt', 'pt.id', '=', 'ptr.page_id')
            ->join('pages', 'pages.id', '=', 'pt.page_id')
            ->whereNull('ptr.deleted_at')
            ->whereNull('pt.deleted_at')
            ->where('ptr.lang',$this->lang)
            ->whereIn('pages.template_id',[6])
            ->where('products.featured',1)
            ->select('ptr.*','ptr.id as tid','pt.slug as page_slug','pt.name as category_name','products.id','products.price','products.bar_code')
            ->with(['rel_author','media'])->paginate($limit);

        $authors = Author::join('author_translations as at','at.author_id','authors.id')
            ->join('page_translations as pt', 'pt.id', '=', 'at.page_id')
            ->join('pages', 'pages.id', '=', 'pt.page_id')
            ->whereNull('at.deleted_at')
            ->whereNull('pt.deleted_at')
            ->where('at.lang',$this->lang)
            ->whereIn('pages.template_id',[5])
            ->select('at.*','pt.slug as page_slug','authors.id')
            ->with('media')
            ->get();

        if(request()->ajax())
        {
            header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
            header("Cache-Control: post-check=0, pre-check=0", false);
            header("Pragma: no-cache");

            $view = view('web.elements.index_product_crud',['index_products' => $products])->render();
            return response()->json(['html'=>$view]);
        }


        $relatedPages = [];

        foreach(config("app.locales") as $key => $locale){
            $relatedPages[$key]='/'.($key == $this->lang ? '' : $key);

        }

        return view('web.index', ['index_products' => $products,'index_authors' =>$authors,'relatedPages' => $relatedPages,'paginate' =>$paginate]);
    }

    public function showPage($slug1, $slug2 = null, $slug3 = null)
    {
        $slugs = array_filter([$slug1, $slug2, $slug3]);

        $checkPages = Page::join('page_translations as pt', 'pt.page_id', '=', 'pages.id')
            ->whereIn('pt.slug', $slugs)
            ->where('pt.lang', $this->lang)
            ->whereNull('pt.deleted_at')
            ->orderByRaw('FIELD(pt.slug, "'.end($slugs).'")')
            ->select('pages.template_id', 'pages.visible', 'pt.*', 'pt.id as tid', 'pages.id')
            ->get();


        if(count($slugs) != $checkPages->count()){
            if((!is_null($slug2) && $checkPages->count() > 0) && ( in_array($checkPages->first()->template_id,[5]) || in_array($checkPages->last()->template_id,[5] ))){   //maybe it is an article
                return $this->authorSingle($checkPages, end($slugs));
            }
            elseif((!is_null($slug2) && $checkPages->count() > 0) && ( in_array($checkPages->first()->template_id,[6]) || in_array($checkPages->last()->template_id,[6] ))){   //maybe it is an article
                return $this->productSingle($checkPages, end($slugs));
            }
            else{
                return redirect()->route('home');
            }
        }


        $getPage = $checkPages->last();

        $relatedPages = $this->menu->relatedPages($getPage);
//        $canonical_url = $this->menu->fullNestedSlug($getPage->parent_id, $getPage->slug,$this->lang);
        $canonical_url = url($getPage->slug);
        if (!is_null($getPage->forward_url)) {
            return redirect()->away($getPage->forward_url);
        }

        if ($getPage->template_id == 1) {
            return $this->dropdown($getPage, $relatedPages);
        }
        elseif ($getPage->template_id == 2) {
            return $this->abouts($getPage, $relatedPages,$canonical_url);
        }

        elseif ($getPage->template_id == 4) {
            return $this->static($getPage, $relatedPages,'contact',$canonical_url);
        }
        elseif ($getPage->template_id == 5) {
            return $this->authors($getPage, $relatedPages,$canonical_url);
        }
        elseif ($getPage->template_id == 6) {
            return $this->products($getPage, $relatedPages,$canonical_url);
        }
        else{
            return redirect()->route('home');
        }
    }


    public function ProductAges($age_id)
    {
        $limit = request()->get('limit') ?? 24;//limit
        $paginate = request()->get('page') ?? 1;
        $sort = request()->get('sort') ?? 0;

        $products = Product::join('product_translations as ptr','ptr.product_id','products.id')
            ->join('page_translations as pt', 'pt.id', '=', 'ptr.page_id')
            ->join('pages', 'pages.id', '=', 'pt.page_id')
            ->BySort($sort)
            ->whereNull('ptr.deleted_at')
            ->whereNull('pt.deleted_at')
            ->where('ptr.lang',$this->lang)
            ->whereIn('pages.template_id',[6])
            ->whereIn('products.age_id',[$age_id])
            ->select('ptr.*','pt.slug as page_slug','pt.name as category_name','products.id','products.price','products.bar_code')
            ->with(['rel_author','media'])->paginate($limit);


        if(!$products->count()){
            return redirect()->route('home');
        }

        if(request()->ajax())
        {
             header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
            header("Cache-Control: post-check=0, pre-check=0", false);
            header("Pragma: no-cache");
            
            $view = view('web.elements.index_product_crud',['index_products' => $products])->render();
            return response()->json(['html'=>$view]);
        }

        return view("web.product_ages", ['products' =>$products,'paginate' => $paginate,'limit'=> $limit,'sort' =>$sort]);

    }

    public function postContact(Request $request)
    {
        $inputs = [
            'full_name' => 'required',
            'phone' => 'required',
            'email' => 'required|email',
        ];


        $message = $this->dictionary['post_contact_subject'];

        return $this->send_mail($request,$inputs,$message);
    }

    public function send_mail($form,$validate_input,$message){
        $dictionary = Dictionary::where('keyword', 'email_sent')->where('lang_id', $this->lang)->first();
        $email = Config::where('key', 'contact_email')->firstOrFail();

        $validation = Validator::make($form->all(), $validate_input);

        if($validation->fails()){
            $response = $this->responseDataTable(1, $validation->errors()->first());
            return $this->responseJson($response);
        }

        try {
            Mail::to($email->value)->send(new ContactForm($form->all(),'contact',$message));
            $response = $this->responseDataTable(0, $dictionary->content);
            return $this->responseJson($response);

        } catch (\Exception $e) {
            $response = $this->responseDataTable(1, $e->getMessage());
            return $this->responseJson($response);
        }

    }

}