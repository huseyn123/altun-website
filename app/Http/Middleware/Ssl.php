<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class Ssl
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        Request::setTrustedProxies([$request->getClientIp()],Request::HEADER_X_FORWARDED_ALL);
        if (!$request->secure() && env('APP_ENV') == 'production' ) {
          return redirect()->secure($request->getRequestUri());
        }
        return $next($request);
    }
}


