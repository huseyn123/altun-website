<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDetailToProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->boolean('status')->index()->unsigned()->after('price');
            $table->unsignedInteger('age_id')->nullable()->after('price');
            $table->unsignedBigInteger('bar_code')->after('price');
            $table->string('size', 25)->nullable()->after('price');
            $table->string('page_number', 25)->nullable()->after('price');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->dropColumn('status');
            $table->dropColumn('age_id');
            $table->dropColumn('author_id');
            $table->dropColumn('bar_code');
            $table->dropColumn('size');
            $table->dropColumn('page_number');
        });
    }
}
