<?php

Auth::routes();

// patient routes
/*Route::prefix('user')->group(function (){
    Route::post('/logout', 'Auth\LoginController@logout')->name('user.logout');
});*/


// admin routes
Route::prefix('admin')->group(function (){
    Route::get('/login', 'Auth\AdminLoginController@showloginForm')->name('admin.login');
    Route::post('/login', 'Auth\AdminLoginController@login')->name('admin.login.submit');
   // Route::get('/logout', 'Auth\AdminLoginController@logout')->name('admin.logout');

    // Password reset routes
    Route::prefix('password')->group(function (){
        Route::post('/email', 'Auth\AdminForgotPasswordController@sendResetLinkEmail')->name('admin.password.email');
        Route::get('/reset', 'Auth\AdminForgotPasswordController@showLinkRequestForm')->name('admin.password.request');
        Route::post('/reset', 'Auth\AdminResetPasswordController@reset');
        Route::get('/reset/{token}', 'Auth\AdminResetPasswordController@showResetForm')->name('admin.password.reset');
    });
});


// site routes

Route::group(
[
    'prefix' => LaravelLocalization::setLocale(),
    'middleware' => [ 'localeSessionRedirect', 'localizationRedirect', 'localeViewPath' ]
],
function() {
    Route::get('/', 'Web\PageController@index')->name('home');
    Route::get('/product_age/{age_id}', 'Web\PageController@ProductAges')->name('product.age');
    Route::post('/product/search', 'Web\SearchController@productSearch')->name('product.search');
//    Route::get('productList/{listCategory}', 'Web\PageController@getProductList')->name('product_list');

   Route::get('{slug1}/{slug2?}/{slug3?}',['as' => 'showPage', 'uses' => 'Web\PageController@showPage']);

});

Route::post('/subscribe', 'SubscriberController@store')->name('subscribe');
Route::post('/contact', 'Web\PostController@store')->name('web.contact');

