<?php

Route::get('/', 'DashboardAdminController@index')->name('admin.dashboard');
Route::get('/profile', 'AdminController@profile')->name('admin.profile');
Route::put('updatePassword', 'AdminController@updatePassword')->name('admins.updatePassword');
Route::put('updatePhoto', 'AdminController@updatePhoto')->name('admins.updatePhoto');

Route::resource('analytic', 'GoogleAnalyticsController')->only(['index', 'store']);

Route::resource('admins', 'AdminController');
Route::group(['prefix'=>'admins'], function()
{
    Route::put('{id}/trash', 'AdminController@trash')->name('admins.trash');
    Route::put('{id}/restore', 'AdminController@restore')->name('admins.restore');
});

Route::resource('page', 'PageController', ['except' => ['show','destroy']]);
Route::group(['prefix'=>'page'], function()
{
    Route::put('{id}/updateSingle', 'PageController@updateSingle')->name('page.updateSingle');

});

Route::resource('pageTranslation', 'PageTranslationController')->only(['store', 'update', 'destroy'])->names(['destroy' => 'page.destroy']);
Route::group(['prefix'=>'pageTranslation'], function()
{
    Route::get('order', 'PageTranslationController@order')->name('pageTranslation.order');
    Route::post('postOrder', 'PageTranslationController@postOrder')->name('post.pageTranslation.order');
    Route::put('{id}/trash', 'PageTranslationController@trash')->name('page.trash');
    Route::put('{id}/restore', 'PageTranslationController@restore')->name('page.restore');
});


Route::resource('product', 'ProductController')->except(['show', 'destroy']);
Route::group(['prefix'=>'product'], function()
{
    Route::get('{id}/get', 'ProductController@getProductDetail')->name('product.get');
    Route::get('{id}/duplicate', 'ProductController@duplicate')->name('product.duplicateView');
    Route::put('{id}/duplicate', 'ProductController@duplicatePost')->name('product.duplicate');
    Route::put('{id}/updateSingle', 'ProductController@updateSingle')->name('product.updateSingle');
    Route::put('{id}/fastUpdate', 'ProductController@fastUpdate')->name('product.fastUpdate');
    Route::get('updateAll', 'ProductController@updateAllProducts')->name('product.updateAll');
});
Route::put('updateMinSalesCount', 'ProductController@updateMinSalesCount')->name('product.updateMinSalesCount');


Route::resource('productTranslation', 'ProductTranslationController')->only(['store', 'update','destroy'])->names(['destroy' => 'product.destroy']);
Route::group(['prefix'=>'productTranslation'], function()
{
    Route::put('{id}/trash', 'ProductTranslationController@trash')->name('product.trash');
    Route::put('{id}/restore', 'ProductTranslationController@restore')->name('product.restore');
    Route::get('order', 'ProductTranslationController@order')->name('productTranslation.order');
    Route::get('index_order', 'ProductTranslationController@index_order')->name('IndexProductTranslation.order');
    Route::post('indexPostOrder', 'ProductTranslationController@postIndexOrder')->name('post.IndexProductTranslation.order');
    Route::post('postOrder', 'ProductTranslationController@postOrder')->name('post.productTranslation.order');
});




Route::resource('authors', 'AuthorController')->except(['show', 'destroy']);
Route::group(['prefix'=>'authors'], function()
{
    Route::put('{id}/updateSingle', 'AuthorController@updateSingle')->name('authors.updateSingle');
});

Route::resource('authorTranslation', 'AuthorTranslationController')->only(['store', 'update', 'destroy'])->names(['destroy' => 'authors.destroy']);
Route::group(['prefix'=>'products'], function()
{
    Route::put('{id}/trash', 'AuthorTranslationController@trash')->name('authors.trash');
    Route::put('{id}/restore', 'AuthorTranslationController@restore')->name('authors.restore');
});


Route::resource('articles', 'ArticleController')->except(['show', 'destroy']);
Route::group(['prefix'=>'articles'], function()
{
    Route::put('{id}/updateSingle', 'ArticleController@updateSingle')->name('article.updateSingle');
});

Route::resource('articleTranslation', 'ArticleTranslationController')->only(['store', 'update', 'destroy'])->names(['destroy' => 'articles.destroy']);
Route::group(['prefix'=>'articles'], function()
{
    Route::put('{id}/trash', 'ArticleTranslationController@trash')->name('articles.trash');
    Route::put('{id}/restore', 'ArticleTranslationController@restore')->name('articles.restore');
});


Route::resource('slider', 'SliderController', ['except' => ['show']]);
Route::group(['prefix'=>'slider'], function()
{
    Route::put('{id}/trash', 'SliderController@trash')->name('slider.trash');
    Route::put('{id}/restore', 'SliderController@restore')->name('slider.restore');
});

//partners routes
Route::resource('partners', 'PartnerController');
Route::resource('{model}/{id}/gallery','GalleryController')->only(['index', 'store']);
Route::delete('gallery/{id}', 'GalleryController@destroy')->name('gallery.destroy');

Route::resource('productAge', 'ProdutAgeController', ['except' => ['show']]);
Route::resource('tag', 'TagController', ['except' => ['show']]);

Route::resource('book_education', 'BookEducationController')->except(['show']);
Route::resource('book_lang', 'BookLangController')->except(['show']);


Route::resource('subscribers', 'SubscriberController')->except(['edit']);
Route::resource('config', 'ConfigController', ['only' => ['index', 'edit', 'update']]);
Route::resource('dictionary', 'DictionaryController',['only' => ['index', 'edit', 'update', 'create', 'store']]);
Route::resource('city', 'CityController',['only' => ['index', 'edit', 'update', 'create', 'store']]);
Route::resource('sitemap', 'SitemapController')->only(['index', 'store']);


