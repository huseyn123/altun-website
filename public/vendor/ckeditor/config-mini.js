﻿CKEDITOR.editorConfig = function( config ) {
    config.autoParagraph = false;
    config.removeButtons = 'Copy,Save,NewPage,Preview,Print,Templates,Cut,Paste,PasteText,PasteFromWord,Undo,Redo,Find,Replace,SelectAll,Scayt,Form,Checkbox,Radio,TextField,Textarea,Select,Button,ImageButton,HiddenField,Underline,Strike,Superscript,Subscript,CopyFormatting,RemoveFormat,NumberedList,Outdent,Indent,Blockquote,CreateDiv,JustifyCenter,JustifyRight,JustifyBlock,Language,BidiRtl,BidiLtr,JustifyLeft,Unlink,Link,Anchor,Image,Flash,Table,HorizontalRule,Smiley,SpecialChar,Iframe,PageBreak,Styles,Font,FontSize,TextColor,BGColor,Maximize,ShowBlocks,About';
    config.language = 'az';
};