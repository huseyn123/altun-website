(function ($, DataTable) {
    "use strict";

    DataTable.ext.buttons.editSales = {
        className: 'editsales-modal',
        text: function (dt) {
            return '<i class="fa fa-edit"></i> ' + dt.i18n('buttons', 'Endirim Faizi');
        }

        /*action: function (e, dt, button, config) {
            window.location = window.location.href.replace(/\/+$/, "") + '/create';
        }*/
    };
})(jQuery, jQuery.fn.dataTable);