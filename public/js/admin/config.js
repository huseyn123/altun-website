var loadingIcon = "<i class='fa fa-circle-o-notch fa-spin text-center'></i>";
var requestSent = false;

$(function() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('.datepicker').datetimepicker({
        format: 'YYYY-MM-DD',
        useCurrent:false
    });

    $('.datetimepicker').datetimepicker({
        format: 'DD/MM/YYYY HH:mm',
        stepping : 30,
        minDate: moment().add(1, 'days'),
        locale: 'az',
        showClose: true
    });


    $('.select-search').select2();

    $(".ajax-select").each(function(){
        var $this = $(this);

        $this.select2({
            placeholder: $this.data('placeholder'),
            minimumInputLength: $this.data('mininput'),
            allowClear:$this.data('clear'),
            ajax: {
                url: $this.data('url'),
                delay: 250,
                processResults: function (data) {
                    return {
                        results: data
                    };
                }
            }
        });
    });


    $(".select-multiple").each(function(){
        var $this = $(this);

        $this.select2({
            placeholder: $this.data('placeholder'),
            tags: true,
            maximumSelectionLength: 10,

            createTag: function(params) {
                if($this.data('create') && $this.data('create') == 1){
                    return {
                        id: params.term,
                        text: params.term,
                        isNew : true
                    };
                }
                else{
                    return undefined;
                }
            }
        });
    }).on("change", function(e) {
        var isNew = $(this).find('[data-select2-tag="true"]');
        if(isNew.length){
            isNew.replaceWith('<option selected value="'+isNew.val()+'">'+isNew.val()+'</option>');
        }
    }).on(
        'select2:close',
        function () {
            $(this).focus();
        }
    );





});


$(document).ready( function() {
    var image_size = {
        5 :{width:10,height:20}
    };
    $("#template_id").change(function(){
        $(this).find("option:selected").each(function(){
            var optionValue = $(this).attr("value");
            if(optionValue){
                $(".form-class").not(".hh" + optionValue).show();
                var find = $(".hh" + optionValue).hide();
                if(find.find('.image')){
                    $(".hh" + optionValue+' input.with_image').val("");
                    $(".hh" + optionValue+' input.image').val("");
                }
            } else{
                $(".form-class").hide();
            }
        });
    }).change();


    $("#parent_id").change(function(){

        if($('#parent_id option:selected').val() == 0){
            $(".language-form").show();
            //$(".cover_photo").show();Z
        } else{
            $(".language-form").hide();
            //$(".cover_photo").hide();
        }


    }).change();


    $(':file').on('fileselect', function(event, numFiles, label) {

        var input = $(this).parents('.input-group').find(':text'),
            log = numFiles > 1 ? numFiles + ' files selected' : label;

        if( input.length ) {
            input.val(log);
        }
    });


    $(':file').on('change',function(e){

        var input = $(this),
            numFiles = input.get(0).files ? input.get(0).files.length : 1,
            label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
        input.trigger('fileselect', [numFiles, label]);

    });
});


$.fn.select2.defaults.set( "theme", "bootstrap" );

$('#modal-confirm').on('show.bs.modal', function(e) {
    $(this).find('.warning-modal').attr('action', $(e.relatedTarget).attr('data-action'));
});


$('#myModal').on('hide.bs.modal', function (e) {
    if (typeof(CKEDITOR) != "undefined"){

        for ( instance in CKEDITOR.instances ) {
            CKEDITOR.instances[instance].destroy();
        }
    }
});



function datatableAction(form){

    $.ajax({
        url:  form.action,
        type: 'PUT',
        timeout: 20000,
        headers: { 'X-CSRF-Token' : $('meta[name="csrf-token"]').attr('content') },
        dataType: "json",

        beforeSend: function(response) {
            $("#loadingButton").button('loading');
        },

        success: function(response){
            $("#loadingButton").button('reset');

            if(response.code == 0) {

                if(typeof response.draw =='object') {
                    $(response.draw[0]).DataTable().draw( false );
                    $(response.draw[1]).DataTable().draw( false );
                }
                else{
                    $(response.draw).DataTable().draw( false );
                }

                toastr.success('Əməliyyat uğurlu alındı.');
            }
            else{
                toastr.error('Əməliyyat baş tutmadı');
            }
        },

        error: function(res){
            $("#loadingButton").button('reset');
            toastr.error('Unexpected Error!');
        }
    });
}


$(document).ready(function(){


    $("body").on('click', '.open-modal-dialog', function (e){
        e.preventDefault();
        if(requestSent) {return;}
        modal($(this), $(this).attr('data-link'));
    });

    $("body").on('click', '.event-modal', function (e){
        e.preventDefault();
        if(requestSent) {return;}
        modal($(this), $(this).attr('href'));
    });

    function modal(data, route) {
        requestSent = true;

        var mBody = "#modal-body";

        if(data.attr('second-modal') === true){
            mBody = "#second-modal";
            $('#mySecondModal').modal('show');
        }
        else{
            $('#myModal').modal('show');
        }

        if(data.attr('data-large') == true){
            $('.modal-dialog').addClass("modal-lg");
        }
        else{
            $('.modal-dialog').removeClass( "modal-lg" );
        }

        $.ajax({
            url: route,
            type: "GET",
            timeout: 20000,

            beforeSend: function() {
                $(mBody).html(loadingIcon);
            },
            success: function(data){
                requestSent = false;
                $(mBody).html(data);
            },
            error: function() {
                requestSent = false;
                $('#myModal').modal("toggle");
                toastr.error('Unexpected Error!');
            }
        });
    }



    $(".actionPage").on("submit",function (event){
        event.preventDefault();
        actionPage($(this));
    });

    function actionPage(action){

        for ( instance in CKEDITOR.instances ) {
            CKEDITOR.instances[instance].updateElement();
        }

        $.ajax({
            url:  action.attr('action'),
            type: action.attr('method'),
            timeout: 20000,
            headers: { 'X-CSRF-Token' : $('meta[name="csrf-token"]').attr('content') },
            //data: action.serialize(),
            data: new FormData(action[0]),
            dataType: "json",
            processData: false,
            contentType: false,

            beforeSend: function() {
                $(".loadingButton").button('loading');
            },

            success: function(response, event){

                $(".loadingButton").button('reset');


                if(response.code == 0) {

                    $(response.draw).DataTable().draw( false );

                    if(response.close)
                    {
                        $(response.close).modal("toggle");
                    }

                    if(response.redirect)
                    {
                        window.location = response.redirect;
                    }

                    toastr.success('Əməliyyat uğurlu alındı');
                }
                else{
                    toastr.error(response.msg);
                }
            },

            error: function(){
                $(".loadingButton").button('reset');
                toastr.error('Unexpected Error!');
            }
        });
    }


    $("#dtForm").on("submit",function (event){


        if( typeof(CKEDITOR) !== "undefined" ){
            for ( instance in CKEDITOR.instances ) {
                CKEDITOR.instances[instance].updateElement();
            }
        }



        var loading = '.loadingButton';
        event.preventDefault();
        if(requestSent) {return;}
        datatableForm($(this), loading);
    });

    $(".dtForm").on("submit",function (event){
        var loading = '.loadingButton';

        event.preventDefault();
        if(requestSent) {return;}
        datatableForm($(this), loading);
    });

    function datatableForm(action, loading){
        requestSent = true;

        $.ajax({
            url:  action.attr('action'),
            type: action.attr('method'),
            timeout: 20000,
            headers: { 'X-CSRF-Token' : $('meta[name="csrf-token"]').attr('content') },
            //data: action.serialize(),
            data: new FormData(action[0]),
            dataType: "json",
            processData: false,
            contentType: false,

            beforeSend: function(response) {
                $(loading).button('loading');
            },

            success: function(response){
                $(loading).button('reset');

                if(response.code == 0) {

                    if(response.draw) {

                        if(typeof response.draw =='object') {
                            $(response.draw[0]).DataTable().draw( false );
                            $(response.draw[1]).DataTable().draw( false );
                        }
                        else{
                            $(response.draw).DataTable().draw( false );
                        }
                    }

                    if(response.close)
                    {
                        if(typeof response.close =='object') {

                            $(response.close[0]).modal("toggle");
                            $(response.close[1]).modal("toggle");
                        }
                        else{
                            $(response.close).modal("toggle");
                        }
                    }

                    if(response.data)
                    {
                        $('#calendar').fullCalendar('removeEvents', response.data['id']);
                    }

                    toastr.success('Əməliyyat uğurlu alındı');
                }
                else{
                    toastr.error(response.msg);
                }

                requestSent = false;
            },

            error: function(res){
                requestSent = false;
                $(loading).button('reset');
                toastr.error('Unexpected Error!');
            }
        });
    }

});