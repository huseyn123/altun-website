@if ($paginator->hasPages())
<div class="col-12 col-md-12">
    <div class="imagebox-pagination">
        <ul class="flat-pagination">
            @if (!$paginator->onFirstPage())
                <li class="prev">
                    <a href="{{ $paginator->url($paginator->onFirstPage()) }}" ></a>
                </li>
            @endif

                {{-- Pagination Elements --}}
                @foreach ($elements as $element)

                    {{-- Array Of Links --}}
                    @if (is_array($element))
                        @foreach ($element as $page => $url)
                            @if ($page == $paginator->currentPage())
                                <li class="active">
                                    <a >{{ $page }}</a>
                                </li>
                            @else
                                <li >
                                    <a href="{{ $url }}">{{ $page }}</a>
                                </li>
                            @endif
                        @endforeach
                    @endif
                @endforeach

                {{-- Next Page Link --}}
                @if ($paginator->hasMorePages())
                    <li class="next">
                        <a href="{{ $paginator->url($paginator->lastPage()) }}">
                        </a>
                    </li>
                @endif
        </ul>
    </div><!-- /.blog-pagination -->
</div><!-- /.col-12 col-lg-12 -->

@endif