@if ($paginator->hasPages())
    <ul class="list-unstyled" >

        {{-- Previous Page Link --}}
        @if ($paginator->onFirstPage())
            <li class="first disabled"><i class="fa fa-angle-left" aria-hidden="true"></i></li>
            <li class="disabled">Əvvəl</li>

        @else
    
            <li >
                <a href="{{ $paginator->previousPageUrl() }}">
                    <li class="first"><i class="fa fa-angle-left" aria-hidden="true"></i></li>
                </a>
            </li>
            <li>
                <a href="{{ $paginator->previousPageUrl() }}">Əvvəl</a>
            </li>
        @endif

        {{-- Pagination Elements --}}
        @foreach ($elements as $element)
            {{-- Array Of Links --}}
            @if (is_array($element))
                @foreach ($element as $page => $url)
                    @if ($page == $paginator->currentPage())
                        <li class="active" aria-current="page">{{ $page }}</li>
                    @else
                        <li ><a href="{{ $url }}">{{ $page }}</a></li>
                    @endif
                @endforeach
            @endif
        @endforeach

        {{-- Next Page Link --}}
        @if ($paginator->hasMorePages())

          <li>
            <a href="{{ $paginator->nextPageUrl() }}">Sonra</a>
        </li>
        <li class="last">
            <a href="{{ $paginator->nextPageUrl() }}" >
                <i class="fa fa-angle-right" aria-hidden="true"></i>
            </a>
        </li>


        @else
            <li class="disabled">Sonra</li>
            <li class="last disabled"><i class="fa fa-angle-right" aria-hidden="true"></i></li>
        @endif
    </ul>
@endif
