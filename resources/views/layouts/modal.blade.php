<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title">@yield('title') {!! isset($subtitle) ? '<span class="fa fa-user></span> '.$subtitle : ''!!}</h4>
</div>

@if(isset($error))
    @include('widgets.alert', ['class' => 'danger', 'msg' => $error])
    {{die()}}
@endif

@if(isset($route))
    {!! Form::open(['route' => $route, 'method' => $method ?? 'POST', 'class'=>'form-horizontal '.@$class.'', 'id' => $ajax ?? 'dtForm', 'files' => true]) !!}
@endif

<div class="modal-body">
    <div class="col-md-12">
        <div class="row">
            @yield('content')
        </div>
    </div>
    <div class="clearfix"></div>
</div>


@if(isset($route))
    <div class="modal-footer">
        {!! Form::button(trans('locale.cancel'), ['class' => 'btn btn-danger', "data-dismiss"=>"modal"]) !!}
        {!! Form::button($saveTxt ?? trans('locale.save'), ['class' => 'btn btn-success loadingButton', 'type' => 'submit', 'data-loading-text' => loading()]) !!}
    </div>
@endif


@if(isset($route))
    {!! Form::close() !!}
@endif

@if($script == true)
    <script src="{{ asset('js/admin/config.js') }}?v=33"></script>
@endif


@if(isset($editor) && $editor == true)
    <script src="{{ asset('vendor/ckeditor/ckeditor.js') }}"></script>
    <script>
        CKEDITOR.config.customConfig = '{{ asset('vendor/ckeditor/config.js') }}?v=2';
    </script>
@endif


@stack('scripts')