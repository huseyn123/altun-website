<!DOCTYPE html>
<html lang="{{$lang}}">
<head>
    <title>@if(!is_null($page_heading)){{ $page_heading }} | @endif{{ $config['name'] }}</title>

    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no, maximum-scale=1.0, user-scalable=no">
    <meta name="MobileOptimized" content="320">
    <meta name="theme-color" content="#ffffff">
    <meta property="author" content="{{ $config['name'] }}">
    <meta property="og:title" content="{{ $page_heading }}">
    <meta property="og:url" content="{{request()->url()}}">
    <link rel="shortcut icon" href="{{asset('images/favicon/favicon.png')}}">
    <meta property="og:image" content="{{isset($page_image) ? asset($page_image):  asset('images/logo.svg')}}">
    @if(isset($other_page))
        <meta name="keywords" content="{{ $other_page->meta_keywords }}">
        <meta name="description" content="{{ $other_page->meta_description }}">
        <meta property="og:description" content="{{ $other_page->meta_description }}">
    @elseif(isset($page))
        <meta name="keywords" content="{{ $page->meta_keywords }}">
        <meta name="description" content="{{ $page->meta_description }}">
        <meta property="og:description" content="{{ isset($page_description) ? strip_tags($page_description) : @$page->meta_description }}">
    @endif
    @if(!isset($index_page))
        <link rel="canonical" href="{{ $canonical_url ?? ''}}" />
    @endif

    @if(isset($relatedPages))
        @foreach($relatedPages as $key=>$value)
            <link rel="alternate" href="{{ url($value) }}" hreflang="{{$key}}" />
        @endforeach
    @endif
    <link href="{{ asset(mix('css/style.css')) }}" rel="stylesheet">
    <link rel="shortcut icon" href="{{asset("favicon/favicon.png")}}">
    <!-- GOOGLE FONTS -->
    <!--[if lt IE 9]>
    <![endif]-->
    @include('widgets.analytic', ['code' => $config['google_analytic_key'] ])
</head>

<body class="header_sticky">
    <div class="boxed">

            <!-- Preloader -->
            <div class="preloader" style="display: none"></div>

            @include('web.elements.header')

              @include('web.elements.mobile-menu')

              @yield('content')
            @include('web.elements.footer')
        <script src="{{asset(mix('js/script.js'))}}"></script>
        @stack('scripts')
    </div>
</body>
</html>
