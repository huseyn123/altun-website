@if ($breadcrumbs)
    @foreach ($breadcrumbs as $breadcrumb)
        @if (!$breadcrumb->last)
            <li class="trail-item">
                <a href="{{ $breadcrumb->url }}" title="">{{ removeSymbol($breadcrumb->title ,'/') }}</a>
                <span><img src="{{asset('images/icon/next.svg') }}"></span>
            </li>
        @else
            <li class="trail-end">
                <a  title="{{$breadcrumb->title }}">{{$breadcrumb->title }}</a>
            </li>
        @endif
    @endforeach
@endif
