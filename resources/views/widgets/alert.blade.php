<div class="alert alert-{{$class}} @if (isset($dismissable)) alert-dismissable @endif" role="alert">
    @if (isset($dismissable))<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button> @endif @if(isset($icon)) <i class="fa fa-{{$icon}} }}"></i>&nbsp;@endif <strong>{{ $message }}</strong>
    @if(isset($link))
        <a class="alert-link pull-right text-black" href="{{ $link['url'] }}">{{ $link['title'] }}</a>
    @endif
</div>