@if(is_null($row->deleted_at))
    @if(!isset($edit) || $edit == true)
        <a href="#" class="btn btn-xs btn-primary open-modal-dialog" data-link="{{ route("$route.edit", isset($translation) ? $row->tid : $row->id ) }}" data-large="{{ $largeModal ?? false }}"><i class="fa fa-edit"></i> </a>
    @endif

    @if(isset($softDelete) && $softDelete == true)
        <a href="#" class="btn btn-xs btn-danger" onclick="event.preventDefault(); datatableAction(document.getElementById('soft-delete-{{ $row->id }}'));">
            <i class="fa fa-trash"></i>
            {!! Form::open(['method' => 'PUT', 'url' => route("$route.trash", $row->id), 'id' => "soft-delete-{$row->id}", 'style' => 'display:none', 'class' => 'dtForm']) !!}
            {!! Form::close() !!}
        </a>
    @elseif(isset($forceDelete) && $forceDelete == true)
        <a href="#" class="btn btn-xs btn-danger" data-action="{{ route("$route.destroy",isset($translation) ? $row->tid : $row->id) }}" data-toggle="modal" data-target="#modal-confirm"><i class="fa fa-remove"></i></a>
    @endif
@else
    <a href="#" class="btn btn-xs btn-default" onclick="event.preventDefault(); datatableAction(document.getElementById('restore-delete-{{ $row->id }}'));">
        <i class="fa fa-repeat"></i>
        {!! Form::open(['method' => 'PUT', 'url' => route("$route.restore", $row->id), 'id' => "restore-delete-{$row->id}", 'style' => 'display:none']) !!}
        {!! Form::close() !!}
    </a>

    @if(isset($forceDelete) && $forceDelete == true)
        <a href="#" class="btn btn-xs btn-danger" data-action="{{ route("$route.destroy", $row->id) }}" data-toggle="modal" data-target="#modal-confirm"><i class="fa fa-remove"></i></a>
    @endif
@endif