<script>
    $(document).on('click','.popup_selector',function (event) {
        event.preventDefault();
        var updateID = $(this).attr('data-inputid'); // Btn id clicked
        var elfinderUrl = '{{url('manager/filemanager/popup')}}';

        // trigger the reveal modal with elfinder inside
        var triggerUrl = elfinderUrl + '/' + updateID + '?files={{isset($files) ? implode(',', $files) : false}}';

        $.colorbox({
            href: triggerUrl,
            fastIframe: true,
            iframe: true,
            width: '80%',
            height: '60%'
        })

    });

    // function to update the file selected by elfinder
    function processSelectedFile(filePath, requestingField) {
        $('#' + requestingField).val(filePath).trigger('change');
    }

    $(document).on('click','.clear_elfinder_picker',function () {
        $('#feature_image').val('');
    });

    $(document).on('click','.clear_elfinder_picker2',function () {
        $('#feature_image2').val('');
    });

</script>