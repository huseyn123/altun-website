@if(!is_null(Auth::guard('admin')->user()->image))
    <img src="{{ asset('storage/'.Auth::guard('admin')->user()->image) }}" class="{{ $class }}" alt="{{ Auth::guard('admin')->user()->name }}">
@else
    <img src="{{ asset('img/avatar-male.png') }}" class="{{ $class }}" alt="{{ Auth::guard('admin')->user()->name }}">
@endif