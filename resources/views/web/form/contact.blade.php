{!! Form::open(['url'=>route('web.contact'), 'method'=>'POST', 'id' => 'contact-form']) !!}
    <div class="form-box one-half contact-name">
        {!! Form::text('name', null, ["placeholder" => $dictionary['name'],'required' => 'required','class' => "required",'id' =>'contact-name']) !!}
    </div>
    <div class="form-box one-half contact-surname">
        {!! Form::text('surname', null, ["placeholder" => $dictionary['surname'],'required' => 'required','class' => "required",'id' =>'contact-surname']) !!}
    </div>
    <div class="form-box one-half contact-email">
         {!! Form::email('email', null, ["placeholder" => $dictionary['email'],'id' => 'contact-email','required' => 'required','class' => "required"]) !!}
         {{ Form::hidden('method', 'contact') }}
    </div>
    <div class="form-box one-half contact-subject">
        {!! Form::text('subject', null, ["placeholder" => $dictionary['subject'],'id' => 'contact-subject','required' => 'required','class' => "required"]) !!}
    </div>
    <div class="form-box">
        {!! Form::textarea('message', null, ['class' => 'required',"placeholder" => $dictionary['message'],'cols'=> "30", 'rows' => "10",'id' => 'contact-comment','required' => 'required']) !!}
        <ul class="form-result" data-error="{{ $dictionary['contact_error_message'] ?? 'Xəta baş verdi! Zəhmət olmasa az sonra təkrar cəhd edin!' }}"></ul>
    </div>
    <div class="form-box submit-box">
        {!! Form::button($dictionary['send'], ['class' => 'contact', 'autocomplete' => 'off', 'type' => 'submit']) !!}
    </div>
    <div class="contact-result invisible-box">
        <div class="result-box">
            <p>{{ $dictionary['contact_success_message'] ?? 'Sorğunuz göndərildi!' }}</p>
        </div>
    </div>

{!! Form::close() !!}