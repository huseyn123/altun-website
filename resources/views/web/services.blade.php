@extends ('layouts.web', ['page_heading' => $page->name] )

@section ('content')

    @include('web.elements.breadcrumbs')

    @include('web.elements.cover')

    @include('web.elements.'.$blade)

@endsection
