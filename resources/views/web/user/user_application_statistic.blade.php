@extends ('layouts.user-cabinet')

@section ('section')


    <nav class="account_tab clearfix">
        <a href="{{ route('user_app.all') }}" {{ activeUrl(route('user_app.all')) }}>Edilən yardımlar</a>
        <a href="{{ route('user_app.statistics') }}" {{ activeUrl(route('user_app.statistics')) }}>Statistika</a>
    </nav>

    <div class="product_list row">

        @if($years->count())
            <div class="statistics">
                <div class="years clearfix">
                    <span>İllər üzrə göstəricilər:</span>
                    @foreach($years as $year)
                        <a href="?year={{ $year }}" class="app_link @if($loop->first)active @endif" > {{ $year }}</a>
                    @endforeach
                </div>
                <div class="user_app_boxs boxs clearfix">
                    @foreach($user_app_page  as $up)
                        <div class="box">
                            <h2>{!! user_application_name($up->name) !!} </h2>
                            <span class="count"><span>{{ $up->user_application->count() }}</span>{{ $up->user_application->sum('application_price') }} AZN</span>
                        </div>
                    @endforeach
                </div>
            </div>

        @endif

    </div>

@endsection

@push('scripts')

    <script>
        $('.app_link').on("click", function(e){
            this_url=$(this).attr('href');
            var active=$(this).hasClass("active");
            e.preventDefault();

            if(!active){
                $('.statistics .years a').removeClass('active');
                $(this).addClass('active');
                $.ajax({
                    type : "get",
                    url: this_url,
                    success: function(response){
                        $('.user_app_boxs').html(response.html);
                    },
                    error: function() {
                        $(".user_app_boxs").html("<h3>{{ trans('locale.whops') }}</h3>");
                    }
                });
            }
        });

    </script>

@endpush



