@extends ('layouts.web', ['page_heading' => $product->name,'other_page'=>$product] )

@section ('content')

    @include('web.elements.breadcrumbs',['data' => $product])


    <section class="flat-product-detail">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-5">
                    <div class="product-detail-side">
                        <div id="zoomable" class="detail-imagebox">
                            @if(substr($product->getFirstMedia()->getFullUrl(), -3) == 'svg')
                                <img  src="{{$product->getFirstMedia()->getFullUrl()}}"/>
                            @else
                                <img  src="{{$product->getFirstMedia()->getUrl('blade')}}"/>
                            @endif
                        </div>
                        @if($product->getMedia('gallery')->count())
                            <div class="detail-pages">
                                <h4>İç səhifələr</h4>
                                <ul>
                                    @foreach($product->getMedia('gallery') as $gallery)
                                        <li>
                                            <a class="zoom-pages" href="{{asset($gallery->getUrl('blade'))}}" data-fancybox="gallery">
                                                <img src="{{asset($gallery->getUrl('blade'))}}">
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        @if($product->order_link)
                            <div class="detail-to-cart">
                                <a href="{{$product->order_link}}" target="_blank">{{$dictionary['c_online_p'] ?? 'Online sifariş'}} </a>
                            </div>
                        @endif
                    </div>
                </div><!-- /.col-md-5 -->
                <div class="col-12 col-md-7">
                    <div class="product-detail-main">
                        <div class="detail-name">
                            <h3 class="name-book">{{$product->name}}</h3>
                            @if($product->rel_author->count())
                                @foreach($product->rel_author as $p_aut)
                                    <a href="{{route('showPage',[$p_aut->cat_slug,$p_aut->author_slug])}}" class="name-author">{{ $p_aut->author_name}}</a>@if(!$loop->last), @endif
                                @endforeach
                            @endif
                        </div>
                        <div class="detail-prop">
                            <table>
                                <tbody>
                                <tr>
                                    <td>{{$dictionary['b_barcode'] ?? 'Kitabın kodu'}}</td>
                                    <td>{{$product->bar_code}}</td>
                                </tr>
                                @if($product->size)
                                    <tr>
                                        <td>{{$dictionary['b_size'] ?? 'Kitabın ölçüləri'}}</td>
                                        <td>{{$product->size}}</td>
                                    </tr>
                                @endif
                                @if($product->lang_title)
                                    <tr>
                                        <td>{{$dictionary['add_new_dict'] ?? 'Dili'}}</td>
                                        <td>{{$product->lang_title}}</td>
                                    </tr>
                                @endif
                                @if($product_education->count())
                                    <tr>
                                        <td>{{$dictionary['add_new_dict'] ?? 'Təhsil Mərhələsi'}}</td>
                                        <td>@foreach($product_education as $p_ed) {{ $p_ed->name}}@if(!$loop->last), @endif @endforeach</td>
                                    </tr>
                                @endif
                                @if($product->page_number)
                                    <tr>
                                        <td>{{$dictionary['page_numb'] ?? 'Səhifələrin sayı'}} </td>
                                        <td>{{$product->page_number}}</td>
                                    </tr>
                                 @endif
                                <tr>
                                    <td>{{$dictionary['b_price'] ?? 'Qiymət'}}</td>
                                    <td>{{$product->price}} <span>M</span></td></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        @if($product->content)
                            <div class="detail-content">
                                <h4>{{$dictionary['b_descriprion'] ?? 'Kitab haqqında qısa məlumat'}}</h4>
                                {!! $product->content !!}
                            </div>
                        @endif
                    </div>

                </div><!-- /.col-md-7 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </section><!-- /.flat-product-detail -->


    @if($similarProducts->count())
        <section class="flat-imagebox single">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="flat-row-title">
                        <h1>{{$dictionary['similar_products'] ?? 'Oxşar kitablar'}}</h1>
                    </div>
                </div><!-- /.col-md-12 -->
            </div><!-- /.row -->
            <div class="row">
                <div class="col-md-12">
                    <div class="similar-carousel">
                        @include('web.elements.product_carousel_crud',['products' =>$similarProducts])
                    </div><!-- /.similar-carousel -->
                </div><!-- /.col-md-12 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </section><!-- /.flat-imagebox single -->
    @endif

    @include('web.elements.projects')

    @include('web.elements.subscribe')


@endsection