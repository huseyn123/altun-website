@extends ('layouts.web', ['page_heading' => $key] )

@section ('content')

    <section class="flat-single-writer">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-4 col-lg-3">
                    <div class="flat-row-title">
                        <h3>{{$products->count()}} nəticə</h3>
                    </div><!-- /.flat-row-title -->
                </div><!-- /.col-12 col-md-4 col-lg-3 -->
                <div class="col-12 col-md-8 col-lg-9">
                    <div class="row">
                        @include('web.elements.product_crud',['f_type'=>true])
                    </div><!-- /.row -->
                </div><!-- /.col-12 col-md-8 col-lg-9 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </section><!-- /.flat-single-writer -->


@endsection


