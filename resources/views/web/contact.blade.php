@extends ('layouts.web', ['page_heading' => $page->name] )

@section ('content')

        @include('web.elements.breadcrumbs')

        <section class="flat-contact">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="flat-row-title">
                            <h2>{{$page->name}}</h2>
                        </div>
                    </div><!-- /.col-md-12 -->
                </div><!-- /.row -->
                <div class="row">
                    <div class="col-md-4">
                        <div class="box-contact">
                            <ul>
                                <li class="address">
                                    <img src="{{asset('images/icon/contact-address.svg')}}" >
                                    <p>{{$dictionary['address']}}</p>
                                </li>
                                <li class="email">
                                    <img src="{{asset('images/icon/contact-email.svg')}}" >
                                    <p><a >{{$config['contact_email']}}</a></p>
                                </li>
                                <li class="phone">
                                    <img src="{{asset('images/icon/contact-phone.svg')}}">
                                    <p><a >{{$config['contact_phone']}}</a></p>
                                </li>
                                <li class="service">
                                    <img src="{{asset('images/icon/contact-service.svg')}}" >
                                    <p>{{$dictionary['contact_text'] ?? 'Müştəri xidmətləri: Bazar ertəsi - Cümə:10:00-dan 19:00, Şənbə:10:00 - 18:00'}}</p>
                                </li>
                            </ul>
                        </div><!-- /.box-contact -->
                    </div><!-- /.col-md-4 -->
                    <div class="col-md-8">
                        <div class="form-contact">
                            @include('web.form.contact',['id' =>'ct_id'])
                        </div><!-- /.form-contact -->
                    </div><!-- /.col-md-8 -->
                </div><!-- /.row -->
            </div><!-- /.container -->
        </section><!-- /.flat-contact -->

        <section class="flat-map">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div id="flat-map" class="pdmap">
                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3037.6268394564654!2d49.84119681566727!3d40.
							417116763539475!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x403087e57145af81%3A0xae9c4f3d87b1bf6!2sAltun%
							20Kitab!5e0!3m2!1sru!2s!4v1599057235639!5m2!1sru!2s" width="100%" height="375" frameborder="0" style="border:0;"
                                    allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                        </div>
                    </div><!-- /.col-md-12 -->
                </div><!-- /.row -->
            </div><!-- /.container -->
        </section><!-- /.flat-map -->


    @include('web.elements.subscribe')

@endsection