@extends ('layouts.web', ['page_heading' => $page->name] )

@section ('content')

    @include('web.elements.breadcrumbs')


    <section class="flat-imagebox data_list">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-4 col-lg-3">
                    <div class="sidebar">
                        <div class="sidebar-child">
                            <h3 class="sidebar-title">Seriyalar</h3>
                            @include('web.elements.menu',['lang' => $lang,'type' => 'sidebar','menu_class' => 'sidebar-menu sort-series','menu_template' =>[6]])
                        </div><!-- /.sidebar-child -->
                        <div class="sidebar-child">
                            <h3 class="sidebar-title">Yaş qrupu</h3>
                            @include('web.elements.age_group', ['menu_type' => 'sidebar','menu_class' => 'sidebar-menu sort-age'])
                        </div><!-- /.sidebar-child -->
                    </div><!-- /.sidebar -->
                </div><!-- /.col-12 col-md-4 col-lg-3 -->

                <div class="col-12 col-md-8 col-lg-9">
                    <div class="flat-row-title">
                        <h1>{{$page->name}}</h1>
                        @include('web.elements.product_sorts')
                    </div><!-- /.flat-row-title -->
                    <div class="row list_item">
                        @include('web.elements.product_crud')
                    </div><!-- /.row -->
                </div><!-- /.col-12 col-md-8 col-lg-9 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </section><!-- /.flat-imagebox -->


    @include('web.elements.projects')

    @include('web.elements.subscribe')

@endsection
