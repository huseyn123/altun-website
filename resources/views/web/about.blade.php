@extends ('layouts.web', ['page_heading' => $page->name] )

@section ('content')

    @include('web.elements.breadcrumbs')

    <section class="flat-about">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="about-detail">
                        <h2 class="about-title">{{$page->name}}</h2>
                        {!! $page->content !!}
                    </div>
                </div><!-- /.col-md-12 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </section><!-- /.flat-about -->


    @if($products->count())

    <section class="flat-imagebox single">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="flat-row-title">
                        <h1>{{$dictionary['new_p'] ?? 'Ən yenilər'}}</h1>
                    </div>
                </div><!-- /.col-md-12 -->
            </div><!-- /.row -->
            <div class="row">
                <div class="col-md-12">
                    <div class="similar-carousel">
                        @include('web.elements.product_carousel_crud')
                    </div><!-- /.similar-carousel -->
                </div><!-- /.col-md-12 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </section><!-- /.flat-imagebox single -->
    @endif


    @include('web.elements.projects')

    @include('web.elements.subscribe')

@endsection
