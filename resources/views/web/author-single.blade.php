@extends ('layouts.web', ['page_heading' => $author->name,'other_page'=>$author] )

@section ('content')

        @include('web.elements.breadcrumbs',['data' => $author])



        <section class="flat-single-writer data_list">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-md-4 col-lg-3">
                        <div class="sidebar-writer">
                            <div class="writer-image">
                                @if(substr($author->getFirstMedia()->getFullUrl(), -3) == 'svg')
                                    <img  src="{{$author->getFirstMedia()->getFullUrl()}}"/>
                                @else
                                    <img  src="{{$author->getFirstMedia()->getUrl('blade')}}"/>
                                @endif
                            </div>
                            <div class="writer-detail">
                                <h2 class="writer-title">{{$author->name}}</h2>
                                {!! $author->content !!}
                            </div>
                        </div><!-- /.sidebar-writer -->
                    </div><!-- /.col-12 col-md-4 col-lg-3 -->
                    <div class="col-12 col-md-8 col-lg-9">
                        <div class="flat-row-title">
                            <h3><span>{{$products->total()}}</span> kitab</h3>
                        </div><!-- /.flat-row-title -->
                        <div class="row list_item">
                            @if($products->count())
                                @foreach($products as $product)

                                    <div class="col-6 col-sm-6 col-md-4 col-lg-3">
                                        <div class="product-box">
                                            <div class="box-image">
                                                <a href="{{route('showPage',[$product->page_slug,$product->slug])}}">
                                                    @if(substr($product->getFirstMedia()->getFullUrl(), -3) == 'svg')
                                                        <img  src="{{$product->getFirstMedia()->getFullUrl()}}"/>
                                                    @else
                                                        <img  src="{{$product->getFirstMedia()->getUrl('blade')}}"/>
                                                    @endif
                                                </a>
                                            </div>
                                            <div class="box-content">
                                                <div class="product-name">
                                                    <a href="{{route('showPage',[$product->page_slug,$product->slug])}}"><h4>{{$product->name}}</h4></a>
                                                </div>
                                                @if($product->rel_author->count())
                                                    @foreach($product->rel_author as $p_author)
                                                        @if($p_author->author_id == $author->tid)
                                                            <div class="product-author">
                                                                <a href="{{route('showPage',[$p_author->cat_slug,$p_author->author_slug])}}">{{$p_author->author_name}}</a>
                                                            </div>
                                                        @endif
                                                    @endforeach
                                                @endif
                                                <div class="product-cat">
                                                    <a href="{{route('showPage',[$product->page_slug])}}">{{$product->category_name}}</a>
                                                </div>
                                                <div class="product-price">
                                                    {{$product->price}} <span>M</span>
                                                </div>
                                            </div>
                                        </div><!-- /.product-box -->
                                    </div><!-- /.col-6 col-md-4 col-lg-3 -->

                                @endforeach


                                @if(!isset($f_type))
                                <!-- Pagination Begin -->
                                    {!! $products->appends(request()->input())->links('vendor.pagination.default') !!}
                                <!-- Pagination End -->
                                @endif
                            @endif
                    </div><!-- /.row -->
                    </div><!-- /.col-12 col-md-8 col-lg-9 -->
                </div><!-- /.row -->
            </div><!-- /.container -->
        </section><!-- /.flat-single-writer -->

        @include('web.elements.projects')

        @include('web.elements.subscribe')

@endsection

