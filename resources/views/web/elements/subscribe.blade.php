<section class="flat-newsletter">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="newsletter-title">
                    <h1>{{$dictionary['subscribe_text1'] ?? 'Təkliflər üçün abunə olun'}}</h1>
                </div>
                <p>{{$dictionary['subscribe_text1'] ?? 'There are many variations of passages of Lorem Ipsum available'}}</p>
                {!! Form::open(['url'=>route('subscribe'), 'method'=>'POST', 'class' => 'sub-form subscribe-form']) !!}
                 <div class="subscribe-content">
                        {!! Form::email('email', null, ['placeholder' => $dictionary['enter_email'], 'required' => 'required','class' =>'subscribe-email']) !!}
                        <span class="icon-newsletter"><img src="{{asset('images/icon/mail.svg')}}"></span>
                        {!! Form::button($dictionary['subscribe'] ?? '', ['type' => 'submit']) !!}
                    </div>
                {!! Form::close() !!}
                <p class="subscribe_message" ></p>
            </div><!-- /.col-md-12 -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</section><!-- /.flat-newsletter -->
