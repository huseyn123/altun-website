
<div id="mainnav-mobi" class="mainnav">
    <div class="mobinav-menu">
        <div class="btn-mega">{{$dictionary['select_category'] ?? 'Kateqoriya seç'}}</div>
        <div class="menu">
            @include('web.elements.menu', ['parent' => null,'sub' => true,'lang' => $lang, 'hidden' => [0,3,4],'type' => 'catalog','menu_class' => 'menu-child sort-series','menu_template' =>[6]])
            @include('web.elements.age_group', ['menu_class' => 'menu-child sort-age'])
        </div><!-- /.menu -->
        @if(isset($config['online_p_link']))
            <div class="btn-store">
                <a href="{{$config['online_p_link']}}" title=""><img src="{{asset('images/icon/cart.svg')}}">{{$dictionary['online_p'] ?? 'Online satış'}}</a>
            </div>
        @endif

    </div><!-- /#mobinav-menu -->
    @include('web.elements.menu', ['parent' => null,'sub' => true,'lang' => $lang, 'hidden' => [0,3,4],'type' => 'mobile','menu_class' => 'mobinav-links','menu_template' =>[2,4,5]])

    <ul class="mobinav-social">
        @include('web.elements.social')
    </ul><!-- /.mobinav-social -->
</div><!-- /#mainnav-mobi -->