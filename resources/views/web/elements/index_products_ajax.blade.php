@foreach($index_products as $product)

    <div class="col-6 col-sm-6 col-md-3 col-lg-2">
        <div class="product-box">
            <a href="{{route('showPage',[$product->page_slug,$product->slug])}}">
                <div class="box-image">
                    @if(substr($product->getFirstMedia()->getFullUrl(), -3) == 'svg')
                        <img  src="{{$product->getFirstMedia()->getFullUrl()}}"/>
                    @else
                        <img  src="{{$product->getFirstMedia()->getUrl('thumb')}}"/>
                    @endif
                </div>
                <div class="box-content">
                    <div class="product-name">
                        <h4>{{$product->name}}</h4>
                    </div>
                    <div class="product-id">
                        <span>{{$product->bar_code}}</span>
                    </div>
                    <div class="product-price">
                        <span>{{$product->price}} AZN</span>
                    </div>
                </div>
            </a>
        </div><!-- /.product-box -->
    </div><!-- /.col-6 col-lg-2 -->

@endforeach

<!-- Pagination Begin -->
{!! $index_products->appends(request()->input())->links('vendor.pagination.default') !!}
<!-- Pagination End -->
