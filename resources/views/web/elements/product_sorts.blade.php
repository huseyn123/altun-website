<div class="sort">
    <div class="popularity">
        <select name="sort" id="catalogue-sort">
            @if($sort == 0)
                <option selected disabled>{{$dictionary['select_sort'] ?? 'Sıralamanı seç'}}</option>
            @endif
            @foreach(config('config.product_filtr_order') as $key=>$value)
                <option value="{{$key}}" @if($key == $sort) selected @endif>{{$value}}</option>
            @endforeach
        </select>
    </div>
</div>