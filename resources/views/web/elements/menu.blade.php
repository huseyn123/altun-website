@if(in_array($type, ['header','footer','catalog','age_group','mobile']))
    <ul @if(isset($menu_class)) class="{{$menu_class}}" @endif>
        @foreach($menu as $m)
            @if($m->parent_id == $parent  && $m->lang == $lang && in_array($m->template_id,$menu_template))
                <li>
                    <a href="@if(in_array($m->template_id,[1])) JavaScript:Void(0);  @else {{route('showPage',[$m->slug])}} @endif"  title="{{ $m->name }}">{{ $m->name}}</a>
                </li>
            @endif
        @endforeach
    </ul>

@elseif(in_array($type, ['sidebar']))
    <ul @if(isset($menu_class)) class="{{$menu_class}}" @endif>
        @foreach($menu as $m)
            @if($m->lang == $lang && in_array($m->template_id,$menu_template))
                <li class="check-box">
                    <input type="checkbox" id="cat{{$loop->iteration}}" class="product_filtr" name="cat"
                           value="{{$m->tid}}" @if(isset($selected_cats) && count($selected_cats) && in_array($m->tid,$selected_cats)) checked
                            @endif>
                    <label for="cat{{$loop->iteration}}">{{ $m->name}}</label>
                </li>
            @endif
        @endforeach
    </ul>
@endif