@if($projects->count())
    <section class="flat-projects">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="flat-row-title">
                        <h1>{{$dictionary['i_projects'] ?? 'Lahiyyələrimiz'}}</h1>
                    </div>
                </div><!-- /.col-md-12 -->
            </div><!-- /.row -->
            <div class="row">
                <div class="col-md-12">
                    <div class="slider projects-slider owl-carousel owl-theme">
                        @foreach($projects as $project)
                            <div class="slider-item d-flex align-items-end">
                                <a @if($project->link) href="{{$project->link}}" target="_blank"  @else href="JavaScript:Void(0);" @endif >
                                    @if($project->getFirstMedia())
                                        <img src="{{asset($project->getFirstMedia()->getUrl('view'))}}" class="bg-image" />
                                    @endif
                                </a>
                            </div><!-- /.slider-item -->
                        @endforeach
                    </div><!-- /.slider main-slider -->
                </div><!-- /.col-md-12 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </section><!-- /.flat-projects -->
@endif