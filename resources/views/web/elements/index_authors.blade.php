@if($index_authors->count())

    <section class="flat-imagebox writers">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="flat-row-title">
                    <h1>Yazarlarımız</h1>
                </div>
            </div><!-- /.col-md-12 -->
        </div><!-- /.row -->
        <div class="row">
            <div class="col-md-12">
                <div class="writers-carousel">
                    @foreach($index_authors as $author)

                        <div class="imagebox writer">
                        <div class="box-image">
                            <a href="{{route('showPage',[$author->page_slug,$author->slug])}}">
                                @if($author->getFirstMedia())
                                    @if(substr($author->getFirstMedia()->getFullUrl(), -3) == 'svg')
                                        <img  src="{{$author->getFirstMedia()->getFullUrl()}}"/>
                                    @else
                                        <img  src="{{$author->getFirstMedia()->getUrl('blade')}}"/>
                                    @endif
                                @endif
                            </a>
                        </div>
                        <div class="box-content">
                            <div class="writer-name">
                                <h4>{{$author->name}}</h4>
                            </div>
                            <div class="writer-details">
                                <a href="{{route('showPage',[$author->page_slug,$author->slug])}}" title="">{{$dictionary['read_more']}}</a>
                            </div>
                        </div>
                    </div><!-- /.imagebox writer -->

                    @endforeach

                </div><!-- /.writers-carousel -->
            </div><!-- /.col-md-12 -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</section><!-- /.flat-imagebox writers -->

@endif