@foreach(config('config.social-network') as $key => $item)
    @if(isset($config[$item]) && trim($config[$item]) != '')
        <a href="{{ $config[$item] }}" title="{{ $item }}" target="_blank" class="bar">
            <span class=" icon fa fa-{{ $item }}" aria-hidden="true"></span>
        </a>
    @endif
@endforeach
