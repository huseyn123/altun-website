<!-- Popup Begin -->
<div class="popup">

    <div class="popup_inner" data-id="agent">
        <span class="close_popup"></span>
        <div class="popup_form">
            <h1>{{ $dictionary['sales_agent'] ?? 'Satış Agentləri'}}</h1>
            {!! Form::open(['url'=>route('web.service_form'),'class' =>'clearfix','method'=>'POST', 'id' => 'sales_form']) !!}

                <div class="item">
                    {!! Form::hidden('type','sales_agent') !!}
                </div>

                <div class="item">
                    {!! Form::text('full_name', null, ["placeholder" => '*'.$dictionary['full_name'],'class' => 'alphaonly ipt_style']) !!}
                </div>
                <div class="item">
                    {!! Form::text('email', null, ["placeholder" => '*'.$dictionary['email'],'class' => 'ipt_style']) !!}
                </div>
                <div class="item">
                    {!! Form::text('phone', null, ["placeholder" => '*'.$dictionary['phone'],'class' => 'ipt_style phonenumber']) !!}
                </div>

                <div class="item">
                    <select name="service_type"  class="ipt_style">
                        <option value>{{ $dictionary['service_type'] ?? 'Xidmətin növü'}}</option>
                        @foreach($menu as $select_item)
                            @if($select_item->template_id == 22)
                                <option value="{{ $select_item->name }}">{{ $select_item->name }}</option>
                            @endif
                        @endforeach
                    </select>
                </div>

                <div class="item">
                    {!! Form::button($dictionary['send'], ['class' => 'loadingButton', 'data-loading-text' =>g_icon('refresh', null, 'spinning'), 'autocomplete' => 'off', 'type' => 'submit']) !!}
                </div>

            {!! Form::close() !!}

        </div>
    </div>
    @include('web.elements.succes_form',['id' => 'success_sales_form','close' => true])


    <div class="popup_inner" data-id="call">
        <span class="close_popup"></span>
        <div class="popup_form">
            <h1>{{ $dictionary['call_y'] ?? 'Sizə Zəng Edək'}}</h1>
                {!! Form::open(['url'=>route('web.project_form'),'class' =>'clearfix','method'=>'POST', 'id' => 'call_form']) !!}

                    <div class="item">
                        {!! Form::text('full_name', null, ["placeholder" => '*'.$dictionary['full_name'],'class' => 'alphaonly ipt_style']) !!}
                    </div>
                    <div class="item">
                        {!! Form::text('email', null, ["placeholder" => '*'.$dictionary['email'],'class' => 'ipt_style']) !!}
                    </div>
                    <div class="item">
                        {!! Form::text('phone', null, ["placeholder" => '*'.$dictionary['phone'],'class' => 'ipt_style phonenumber']) !!}
                    </div>
                    <div class="item">
                        <select name="service_type" class="ipt_style">
                            <option value>{{ $dictionary['service_type'] ?? 'Xidmətin növü'}}</option>
                            @foreach($menu as $select_item)
                                @if($select_item->template_id == 22)
                                    <option value="{{ $select_item->name }}">{{ $select_item->name }}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                    <div class="item">
                        {!! Form::button($dictionary['send'], ['class' => 'loadingButton', 'data-loading-text' =>g_icon('refresh', null, 'spinning'), 'autocomplete' => 'off', 'type' => 'submit']) !!}
                    </div>
            {!! Form::close() !!}

        </div>
    </div>
    @include('web.elements.succes_form',['id' => 'success_call_form','close' => true])

</div>
<!-- Popup End -->