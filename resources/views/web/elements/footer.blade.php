<footer>
    <div class="footer-main">
        <div class="container">
            <div class="row">
                <div class="col-6 col-md-6">
                    <div class="logo logo-ft">
                        <a href="{{route('home')}}">
                            <img src="{{asset('images/logos/logo.png')}}" >
                        </a>
                    </div><!-- /.logo-ft -->
                </div><!-- /.col-md-6 -->
                <div class="col-6 col-md-6">
                    <div class="widget-ft widget-social">
                        <ul>
                            <li>
                                <p>{{$dictionary['follow_us'] ?? 'Bizi sosyal şəbəkələrdə izləyin'}}</p>
                            </li>
                            @include('web.elements.social')
                        </ul>
                    </div>
                </div><!-- /.col-md-6 -->
            </div><!-- /.row -->
            <div class="row">
                <div class="col-md-12 col-lg-8">
                    @include('web.elements.menu', ['parent' => null,'sub' => true,'lang' => $lang, 'hidden' => [0,3,4],'type' => 'catalog','menu_class' => 'widget-ft widget-categories','menu_template' =>[6]])
                </div><!-- /.col-md-12 col-lg-8 -->
                <div class="col-md-12 col-lg-4">
                    @include('web.elements.age_group', ['menu_class' => 'widget-ft widget-categories-age'])
                </div><!-- /.col-md-12 col-lg-4 -->
            </div><!-- /.row -->
            <div class="row">
                <div class="col-md-12">
                    @include('web.elements.menu', ['parent' => null,'sub' => true,'lang' => $lang, 'hidden' => [0,3,4],'type' => 'footer','menu_class' => 'widget-ft widget-menu','menu_template' =>[2,4,5]])
                    <div class="copyright">
                        <p>{{ $dictionary['copyright'] }}</p>
                    </div>
                </div><!-- /.col-md-12 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </div><!-- /.footer-main -->
</footer><!-- /footer -->