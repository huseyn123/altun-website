<ul class="list-unstyled">
    @if($main_template_id == 5)
        @foreach($menu as $pm)
            @if($pm->parent_id == $main_id && !in_array($pm->visible, $hidden) && $pm->lang == $lang)
                <li><a href="{{ route('showPage',[$main_slug]) }}" title="{{ $pm->name  }}">{{ $pm->name }}</a></li>
            @endif
        @endforeach
    @else
        @foreach($hotels_menu as $ht)
            @if($ht->page_id == $main_id && $ht->lang == $lang)
                <li><a href="{{ route('showPage',[$main_slug,$ht->slug]) }}" title="{{ $ht->name  }}">{{ $ht->name }}</a></li>
            @endif
        @endforeach
    @endif
</ul>