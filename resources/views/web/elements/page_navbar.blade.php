@foreach($main_page->MenuChildren as $child)
    <a @if($child->tid == $page->tid) class="active" @endif href="{{ route('showPage',[$child->slug]) }}">{{ $child->name }}</a>
@endforeach
