<div id="top">
    <div class="container">
        <div class="row">
            <div class="col-md-9">
                @if(isset($social_icons))
                    <div class="social">
                        @include('web.elements.social')
                    </div>
                @endif
                <div class="address">{{ $dictionary['footer_address'] ?? 'Xətai .r, Afiyəddin cəlilov 26' }}</div>
            </div>
            <div class="col-md-3"><a class="call">{{ $config['hotline'] ?? '*5551' }}</a></div>
        </div>
    </div>
</div>