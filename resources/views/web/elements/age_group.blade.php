@if(isset($menu_type) && $menu_type == 'sidebar')

    <ul @if(isset($menu_class)) class="{{$menu_class}}" @endif>
        @foreach($ages as $age)
            <li class="check-box">
                <input type="checkbox" class="product_filtr" id="age{{$loop->iteration}}" name="age"
                value="{{$age->id}}" @if(isset($selected_ages) && count($selected_ages) && in_array($age->id,$selected_ages)) checked @endif>
                <label for="age{{$loop->iteration}}"><span>{{$age->age}}</span> {{$dictionary['age'] ?? 'Yaş'}}</label>
            </li>
        @endforeach
    </ul>

@else

    <ul @if(isset($menu_class)) class="{{$menu_class}}" @endif>
        @foreach($ages as $age)
            <li><a href="{{route('product.age', $age->id)}}" title=""><span>{{$age->age}}</span>{{$dictionary['age'] ?? 'Yaş'}} </a></li>
        @endforeach
    </ul>

@endif