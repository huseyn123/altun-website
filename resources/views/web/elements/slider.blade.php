@if($sliders->count())
    <section class="flat-row flat-slider">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="slider main-slider owl-carousel owl-theme">
                        @foreach($sliders as $slider)
                            <div class="slider-item d-flex align-items-end">
                                @if($slider->getFirstMedia())
                                    <img src="{{asset($slider->getFirstMedia()->getUrl('view'))}}" class="bg-image" />
                                @endif
                                <div class="item-text">
                                    <h2>{{$slider->title}}</h2>
                                    <p>
                                        {!! convertName($slider->summary,'/') !!}
                                    </p>
                                    @if($slider->link)
                                        <span class="btn-shop">
                                            <a href="{{$slider->link}}" target="_blank">{{$dictionary['read_more']}}</a>
                                        </span>
                                    @endif
                                </div>
                            </div><!-- /.slider-item -->
                        @endforeach
                    </div><!-- /.slider main-slider -->
                </div><!-- /.col-md-12 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </section><!-- /.flat-slider -->
@endif

