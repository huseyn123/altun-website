<script>
    $( ".top-search .box-search" ).each(function() {
        $( ".box-search input" ).on('input', (function() {
            $(this).parent('.box-search').children('.search-suggestions').css({
                opacity: '1',
                visibility: 'visible',
                top: '58px'
            });
        }));
        $( ".box-search input" ).on('blur', (function() {
            $(this).parent('.box-search').children('.search-suggestions').css({
                opacity: '0',
                visibility: 'hidden',
                top: '100px'
            });
        }));
    });
    $( ".top-search .box-search input" ).on('keyup', (function() {
        var value = $(this).val();
        $(".box-suggestions").addClass('on-progress');
        $.ajax({
            url: '{{route('product.search')}}',
            type: 'get',
            dataType: 'json',
            data: {
                key: value
            },
            error: function(xhr, status,error) {
                $(".box-suggestions").removeClass('on-progress');
            },
            success: function(resp) {
                if (resp.result == 1) {
                    $('.top-search .box-search .title-books').html(resp.html);
                    $(".box-suggestions").removeClass('on-progress');
                }
            }
        });
    }));

</script>