<div class="top-search">
    @if(isset($type) && $type == 'mobile')
        <div class="search-button">
            <a href="#" class="search-toggle" id="top-search-trigger" data-selector=".box-search"></a>
        </div>
    @endif
        {!! Form::open(['url'=>route('product.search'), 'method'=>'POST','class' =>'box-search']) !!}

        {!! Form::text('search', null, ["placeholder" => $dictionary['w_search_product'] ?? 'Axdardığınız məhsulu yazın…','required' => 'required','class' => "required",'id' =>'search']) !!}
        <span class="icon-search-left"><img src="{{asset('images/icon/search.svg')}}"></span>
        <span class="icon-search-right"><img src="{{asset('images/icon/search.svg')}}"></span>
        <span class="product-count"><span>{{$productsCount}}</span> məhsul</span>
        <div class="search-suggestions">
            <div class="box-suggestions">
                <div class="title-books">

                </div>
            </div><!-- /.box-suggestions -->
        </div><!-- /.search-suggestions -->
        {!! Form::close() !!}</div><!-- /.top-search -->
