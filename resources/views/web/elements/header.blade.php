<section id="header" class="header">
    <div class="header-top">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    @include('web.elements.menu', ['parent' => null,'sub' => true,'lang' => $lang, 'hidden' => [0,3,4],'type' => 'header','menu_class' => 'flat-stores','menu_template' =>[2,4,5]])
                </div><!-- /.col-md-6 -->
                <div class="col-md-6">
                    <ul class="flat-social">
                        @include('web.elements.social')
                    </ul><!-- /.flat-social -->
                </div><!-- /.col-md-6 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </div><!-- /.header-top -->
    <div class="header-bottom">
        <div class="container">
            <div class="row">
                <div class="col-md-2">
                    <div id="logo" class="logo">
                        <a href="{{route('home')}}">
                            <img src="{{asset('images/logos/logo.png')}}" >
                        </a>
                    </div><!-- /#logo -->
                </div><!-- /.col-md-2 -->
                <div class="col-md-8">
                    <div class="user-navigation-container">
                        <div id="mega-menu">
                            <div class="btn-mega"><span></span>{{$dictionary['select_category'] ?? 'Kateqoriya seç'}}</div>
                            <div class="menu">
                                <div class="row">
                                    <div class="col-lg-6 col-md-12">
                                        <h3 class="cat-title">{{$dictionary['for_s'] ?? 'Seriya üzrə'}}</h3>
                                         @include('web.elements.menu', ['parent' => null,'sub' => true,'lang' => $lang, 'hidden' => [0,3,4],'type' => 'catalog','menu_class' => 'menu-child sort-series','menu_template' =>[6]])
                                    </div><!-- /.col-lg-6 col-md-12 -->
                                    <div class="col-lg-4 col-md-12">
                                        <h3 class="cat-title">{{$dictionary['age_f'] ?? 'Yaş üzrə'}} </h3>
                                        @include('web.elements.age_group', ['menu_class' => 'menu-child sort-age'])
                                    </div><!-- /.col-lg-6 col-md-12 -->
                                    <div class="col"></div>
                                </div><!-- /.row -->
                            </div><!-- /.menu -->
                        </div><!-- /#mega-menu -->

                        @include('web.elements.product_search')

                    </div><!-- /.user-navigation-container -->
                </div><!-- /.col-md-8 -->
                <div class="col-md-2">
                    @if(isset($config['online_p_link']))
                        <div class="online-store-container">
                            <a href="{{$config['online_p_link']}}" >{{$dictionary['online_p'] ?? 'Online satış'}} </a>
                        </div>
                    @endif
                </div><!-- /.col-md-2 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </div><!-- /.header-bottom -->

    <div class="header-mobile d-block d-md-block d-lg-none">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-12">
                    <div id="logo" class="logo">
                        <a href="{{route('home')}}">
                            <img src="{{asset('images/logos/logo.png')}}">
                        </a>
                    </div><!-- /#logo -->
                    @include('web.elements.product_search',['type' =>'mobile'])
                    <div class="btn-menu">
                        <span></span>
                    </div>
                </div><!-- /.col-12 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </div><!-- /.header-mobile -->

</section><!-- /#header -->