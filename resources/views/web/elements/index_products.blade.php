@if($index_products->count())
    <section class="flat-imagebox data_list home">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="flat-row-title">
                    <h1>{{$dictionary['new_p'] ?? 'Ən yenilər'}}</h1>
                </div>
            </div><!-- /.col-md-12 -->
        </div><!-- /.row -->
        <div class="row pr_list list_item">
            @include('web.elements.index_product_crud')
        </div><!-- /.row -->
    </div><!-- /.container -->
</section><!-- /.flat-imagebox -->
@endif
