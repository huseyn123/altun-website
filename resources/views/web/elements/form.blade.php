<script type="text/javascript">

    $(function(){

        $("#form").validate({
            rules: {
                full_name: {
                    required: true,
                },
                email: {
                    required: true,
                    email: true
                },
                subject: {
                    required: true,
                },
//                phone: {
//                    required: true,
//                    minlength:9,
//                    maxlength:15
//                },
                message: {
                    required: false,
                }
            },

            success: function(label) {
                label.html('').removeClass('error').addClass('ok');
            },
            errorPlacement: function(error, element) {
                @if(!isset($withoutMsg))
                $(element).addClass( "error-item" );
                @endif
            },
            highlight: function ( element, errorClass, validClass ) {
                $(element).addClass( "error-item" ).removeClass( "valid-item" );
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).addClass( "valid-item" ).removeClass( "error-item" );
            },
            submitHandler: function (form) {
                contactForm($(form),'contact');
                return false;
            }
        });

        jQuery.validator.addMethod("lettersonly", function(value, element) {
            return this.optional(element) || /^[a-z\s]+$/i.test(value);
        }, "Only alphabetical characters");

    });

</script>