
@foreach(config('config.social-network') as $key => $item)

    @if(isset($config[$item]) && trim($config[$item]) != '')
        <li>
            <a class="btn social-{{$item}}" href="{{ $config[$item] }}" target="_blank"></a>
        </li>
    @endif

@endforeach

