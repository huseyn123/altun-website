<!-- SIDEBAR -->
<div class="sidebar col-xs-4">
    <!-- ABOUT ME -->
    <div class="widget about-me">
        <div class="ab-image">
            <img src="{{ asset("storage/".$config['about_me']) }}" alt="{{ $dictionary['about'] }}">
            <div class="ab-title">{{ $dictionary['about'] }}</div>
        </div>
        <div class="ad-text">
            <p>{{ $dictionary['about_me'] }}</p>
        </div>
    </div>

    @if($latestPosts->count() > 0)
        <!-- LATEST POSTS -->
        <div class="widget latest-posts">
            <h3 class="widget-title">
                {{ $dictionary['latest_posts'] }}
            </h3>
            <div class="posts-container">
                @foreach($latestPosts as $post)
                    <div class="item">
                        <img src="{{ asset($post->getFirstMedia()->getUrl('thumb')) }}" alt="{{ $post->name }}" class="post-image" style="max-width: 100px">
                        <div class="info-post">
                            <h5><a href="{{ route('showPage', [$post->page_slug, $post->slug]) }}">{{ $post->name }}</a></h5>
                            <span class="date">{{ blogDate($post->published_at) }}</span>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                @endforeach
                <div class="clearfix"></div>
            </div>
        </div>
    @endif

    <!-- FOLLOW US -->
    <div class="widget follow-us">
        <h3 class="widget-title">
            {{ $dictionary['follow_me'] }}
        </h3>
        <div class="follow-container">
            @include('web.elements.social')
        </div>
        <div class="clearfix"></div>
    </div>

    @if($tags->count() > 0)
        <!-- TAGS -->
        <div class="widget tags">
            <h3 class="widget-title">
                {{ $dictionary['tags'] }}
            </h3>
            <div class="tags-container">
                @foreach($tags as $tag)
                    <a href="#">{{ $tag->name }}</a>
                @endforeach
            </div>
            <div class="clearfix"></div>
        </div>
    @endif
    <!-- ADVERTISING -->
    <div class="widget advertising">
        <div class="advertising-container">
            <img src="{{ asset('img/350x300.png') }}" alt="banner 350x300">
        </div>
    </div>

    @include('web.elements.subscribe')
</div> <!-- #SIDEBAR -->

<div class="clearfix"></div>