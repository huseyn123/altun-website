@foreach($products as $product)

    <div class="col-6 col-sm-6 col-md-4 col-lg-3">
        <div class="product-box">
            <div class="box-image">
                <a href="{{route('showPage',[$product->page_slug,$product->slug])}}">
                    @if(substr($product->getFirstMedia()->getFullUrl(), -3) == 'svg')
                        <img  src="{{$product->getFirstMedia()->getFullUrl()}}"/>
                    @else
                        <img  src="{{$product->getFirstMedia()->getUrl('blade')}}"/>
                    @endif
                </a>
            </div>
            <div class="box-content">
                <div class="product-name">
                    <a href="{{route('showPage',[$product->page_slug,$product->slug])}}"><h4>{{$product->name}}</h4></a>
                </div>
                @if($product->rel_author->count())
                    @foreach($product->rel_author as $p_author)
                        @if($p_author->author_id == $author->tid)
                            <div class="product-author">
                                <a href="{{route('showPage',[$p_author->cat_slug,$p_author->author_slug])}}">{{$p_author->author_name}}</a>
                            </div>
                        @endif
                    @endforeach
                @endif
                <div class="product-cat">
                    <a href="{{route('showPage',[$product->page_slug])}}">{{$product->category_name}}</a>
                </div>
                <div class="product-price">
                    {{$product->price}} <span>M</span>
                </div>
            </div>
        </div><!-- /.product-box -->
    </div><!-- /.col-6 col-md-4 col-lg-3 -->

@endforeach


@if(!isset($f_type))
    <!-- Pagination Begin -->
    {!! $products->appends(request()->input())->links('vendor.pagination.default') !!}
    <!-- Pagination End -->
@endif