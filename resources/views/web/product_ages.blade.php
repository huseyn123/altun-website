@extends ('layouts.web', ['page_heading' => ' '] )

@section ('content')

    <section class="flat-imagebox data_list">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="flat-row-title">
                        @include('web.elements.product_sorts')
                    </div>
                </div><!-- /.col-md-12 -->
            </div><!-- /.row -->
            <div class="row list_item">
                @include('web.elements.index_product_crud',['index_products' => $products])
            </div><!-- /.row -->
        </div><!-- /.container -->
    </section><!-- /.flat-imagebox -->




@endsection