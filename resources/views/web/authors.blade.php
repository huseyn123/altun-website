@extends ('layouts.web', ['page_heading' => $page->name] )

@section ('content')

    @include('web.elements.breadcrumbs')

    <section class="flat-writers">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="flat-row-title">
                        <h1>{{$dictionary['w_all'] ?? 'Yazarlarımız'}}</h1>
                    </div>
                </div><!-- /.col-md-12 -->
            </div><!-- /.row -->
            <div class="row">
                <div class="col-md-12">

                    @foreach($authors as $author)
                        <article class="writerbox">
                        <div class="writerbox-side">
                            <div class="writer-image">
                                @if($author->getFirstMedia())
                                    @if(substr($author->getFirstMedia()->getFullUrl(), -3) == 'svg')
                                        <img  src="{{$author->getFirstMedia()->getFullUrl()}}"/>
                                    @else
                                        <img  src="{{$author->getFirstMedia()->getUrl('blade')}}"/>
                                    @endif
                                @endif
                            </div>
                            <div class="writer-more">
                                <a href="{{route('showPage',[$author->page_slug,$author->slug])}}" title="">{{$dictionary['read_more']}}</a>
                            </div>
                        </div>
                        <div class="writerbox-main">
                            <h2 class="writer-name">{{$author->name}}</h2>
                            <p>
                                {{$author->summary}}
                            </p>

                        </div>
                    </article><!-- /.writerbox -->
                    @endforeach

                </div><!-- /.col-md-12 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </section><!-- /.flat-writers -->


    @if($products->count())
        <section class="flat-imagebox single">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="flat-row-title">
                        <h1>{{$dictionary['w_all_b'] ?? 'Yazarların kitabları'}}</h1>
                    </div>
                </div><!-- /.col-md-12 -->
            </div><!-- /.row -->
            <div class="row">
                <div class="col-md-12">
                    <div class="similar-carousel">
                        @include('web.elements.product_carousel_crud')
                    </div><!-- /.similar-carousel -->
                </div><!-- /.col-md-12 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </section><!-- /.flat-imagebox single -->
    @endif


    @include('web.elements.projects')

    @include('web.elements.subscribe')


@endsection
