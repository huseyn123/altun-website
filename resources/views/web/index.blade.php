@extends ('layouts.web', ['page_heading' => null,'index_page'=>true])

@section ('content')


    @include('web.elements.slider')

    @include('web.elements.index_products')

    @include('web.elements.index_authors')

    @include('web.elements.projects')

    @include('web.elements.subscribe')

@endsection


