@component('mail::message')


    İstifadəçinin məlumatları:

    @if(isset($content['name']))
        - Ad: {{$content['name']}}
    @endif

    @if(isset($content['surname']))
        - Soyad: {{$content['surname']}}
    @endif

    @if(isset($content['email']))
        - Email: {{$content['email']}}
    @endif

    @if(isset($content['subject']))
        - Mövzu: {{$content['subject']}}
    @endif

    @if(isset($content['message']))
        - Mesaj: {{$content['message']}}
    @endif


@endcomponent