<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-body">
                @if(isset($tabs) && is_array($tabs))
                    <ul class="nav nav-pills pull-left">
                    </ul>
                @endif

                @if(isset($create))
                    <div class="box-header pull-right">
                        <h3 class="box-title"><button class="btn btn-success btn-block btn-flat open-modal-dialog" data-link="{{ route($create) }}" data-large="{{ $largeModal ?? false }}"><span class="fa fa-plus"></span> {{ trans('locale.create') }}</button></h3>
                    </div>
                @endif

                @if(isset($locale) && count(config('app.locales')) > 1)
                    <div class="box-header pull-right">
                        {!! Form::select('lang', array_merge(config('app.locales'), ['all' => 'Bütün']), array_first(config('app.locales')), ['id' => 'change-lang', 'class' => 'form-control']) !!}
                    </div>
                @endif

                @if(isset($filter))
                    <div class="box-header pull-right">
                        {!! Form::select('type', $filter, isset($inv_t)  ? 0 : 1, ['id' => 'filter-type', 'class' => 'form-control']) !!}
                    </div>
                @endif

                @if(isset($route) && $route == 'product')

                    {{--<div class="box-header pull-right">--}}
                        {{--{!! Form::select('tag', $tags->prepend('Tag', 'all'), 0, ['id' => 'filter-tag', 'class' => 'form-control select-search', 'style' => 'width:150px']) !!}--}}
                    {{--</div>--}}

                    {{--<div class="box-header pull-right">--}}
                        {{--{!! Form::select('category', $categories->prepend('Kateqoriya', 'all'), 0, ['id' => 'filter-category', 'class' => 'form-control select-search', 'style' => 'width:150px']) !!}--}}
                    {{--</div>--}}
                @endif

                @if(isset($route))
                    <div class="clearfix"></div>
                @endif

                <div class="tab-content">
                    <div class="tab-pane active"><br>
                        @if(isset($filtering))
                            <div class="dataTables_wrapper">
                                <div class="col-md-12 text-center">
                                    @include('filter.'.$filtering)
                                </div>
                            </div>
                        @endif
                        {{ $table }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>