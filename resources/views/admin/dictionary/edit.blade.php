@extends('layouts.modal', ['route' => $route, 'method' => 'PUT', 'editor' => true, 'script' => true])
@section('title', $data->name ?? $title)

@section('content')
    <div class="col-md-12">
        @include('widgets.lang-tab', ['input' => 'textarea', 'editor' => $info->editor, 'info' => $words, 'multiRow' => true, 'name' => 'content'])
    </div>
@endsection

@push('scripts')
    <script>
        @foreach(config('app.locales') as $key => $locale)
            CKEDITOR.replace('editor-{{ $key }}', { customConfig: '{{ asset('vendor/ckeditor/config-mini.js?v=1') }}' });
        @endforeach
    </script>
@endpush