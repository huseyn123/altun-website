@extends ('layouts.modal', ['script' => true])
@section ('title', $create_title)

@section('content')

    <div class="col-lg-10 col-lg-offset-1">

        {!! $fields !!}

    </div>

@endsection
