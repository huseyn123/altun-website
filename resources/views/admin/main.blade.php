@extends ('layouts.admin')
@section ('title', $title)

@section ('content')

    @include('widgets.modal-confirm')

    @component('admin.components.dt', ['create' => $route.'.create', 'route' => $route])
        @slot('table')
            {!! $dataTable->table(['class' => 'table table-striped table-hover', 'id' => $route, 'width' => '100%']) !!}
        @endslot
    @endcomponent

@endsection

@push('scripts')
    {!! $dataTable->scripts() !!}
@endpush