@extends('layouts.modal', ['route' => $route, 'ajax' => '', 'class' => 'actionPage', 'script' => true, 'editor' => true] )
@section('title', $title)

@section('content')
    {!! $fields !!}
@endsection

@push('scripts')
    <script>
        CKEDITOR.replace('editor');

        $('#meta_keywords').select2({
            tags: true,
            maximumSelectionLength: 10

        }).on("change", function(e) {
            var isNew = $(this).find('[data-select2-tag="true"]');
            if(isNew.length){
                isNew.replaceWith('<option selected value="'+isNew.val()+'">'+isNew.val()+'</option>');
            }
        });

    </script>
@endpush