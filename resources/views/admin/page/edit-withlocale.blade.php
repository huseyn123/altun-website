@extends('layouts.modal', ['editor' => true, 'script' => $script ?? true, 'route' => null])
@section('title', $info->name)

@section('content')
    <ul class="nav nav-tabs">
        <li class="active"><a href="#page" data-toggle="tab"> Parametrlər</a></li>
        @foreach(config('app.locales') as $key => $locale)
            <li><a href="#{{$key}}" data-toggle="tab"> {{ $key }}</a></li>
        @endforeach
    </ul>
    <br>
    <div class="tab-content">

        <div id="page" class="tab-pane in active">
            {!! Form::open(['url'=>route("$model.update", $info->id), 'method'=>'PUT', 'class' => 'form-horizontal actionPage']) !!}
                {!! $parameters !!}
                @include('widgets.form-submit', ['text' => trans('locale.save'), 'class' => 'success', 'icon' => 'save'])
            {!! Form::close() !!}
        </div>

        @foreach($relatedPage as $relPage)
            <div id="{{ $relPage->lang }}" class="tab-pane">
                {!! Form::open(['url'=>route("$route.update", $relPage->id), 'method'=>'PUT', 'class' => 'form-horizontal actionPage']) !!}

                    {!! $fields[$relPage->lang] !!}

                    {!! Form::hidden('lang', $relPage->lang) !!}

                    @include('widgets.form-submit', ['text' => trans('locale.save'), 'class' => 'success', 'icon' => 'save'])
                {!! Form::close() !!}
            </div>
        @endforeach

        @foreach(array_diff_key(config('app.locales'), $langs) as $key => $locale)

            <div id="{{$key}}" class="tab-pane">

                {!! Form::open(['url'=>route("$route.store"), 'method'=>'POST', 'class' => 'form-horizontal actionPage']) !!}

                    {!! $fields[$key] !!}

                    {!! Form::hidden('lang', $key) !!}

                    @if($model == 'articles')
                        {!! Form::hidden('article_id', $info->id) !!}
                    @elseif($model == 'product')
                        {!! Form::hidden('product_id', $info->id) !!}
                    @elseif($model == 'authors')
                        {!! Form::hidden('author_id', $info->id) !!}
                    @else
                        {!! Form::hidden('page_id', $info->id) !!}
                    @endif

                    @include('widgets.form-submit', ['text' => trans('locale.save'), 'class' => 'success', 'icon' => 'save', 'del' => true, 'route' => 'page'])
                {!! Form::close() !!}

            </div>

        @endforeach
    </div>
@endsection

@push('scripts')
    <script>




        @foreach(config('app.locales') as $key => $locale)

        @if(isset($authors))
            $('#product_author{{$key}}').select2({
                data: {!! json_encode($authors[$key]) !!},
                tags: true,
                maximumSelectionLength: 10

            }).on("change", function(e) {
                var isNew = $(this).find('[data-select2-tag="true"]');
                if(isNew.length){
                    isNew.replaceWith('<option selected value="'+isNew.val()+'">'+isNew.val()+'</option>');
                }
            });

            $('#product_author{{$key}}').val({!! json_encode($authors[$key]) !!}).trigger('change');

        @endif

        @if(isset($education))
        $('#product_education{{$key}}').select2({
            data: {!! json_encode($education[$key]) !!},
            tags: true,
            maximumSelectionLength: 10

        }).on("change", function(e) {
            var isNew = $(this).find('[data-select2-tag="true"]');
            if(isNew.length){
                isNew.replaceWith('<option selected value="'+isNew.val()+'">'+isNew.val()+'</option>');
            }
        });

        $('#product_education{{$key}}').val({!! json_encode($education[$key]) !!}).trigger('change');

        @endif



        CKEDITOR.replace('editor{{ $key }}');

        @if(isset($editor2))
            CKEDITOR.replace('editor2{{ $key }}');
        @endif


        @if(isset($keys))
            $('#meta_keywords{{$key}}').select2({
                data: {!! json_encode($keys[$key]) !!},
                tags: true,
                maximumSelectionLength: 10

            }).on("change", function(e) {
                var isNew = $(this).find('[data-select2-tag="true"]');
                if(isNew.length){
                    isNew.replaceWith('<option selected value="'+isNew.val()+'">'+isNew.val()+'</option>');
                }
            });

            $('#meta_keywords{{$key}}').val({!! json_encode($keys[$key]) !!}).trigger('change');
        @endif
        @endforeach

    </script>
@endpush