@extends('layouts.modal', ['route' => $route, 'method' => 'PUT','script' => true])
@section('title', $data->name ?? $title)

@section('content')

    <div class="col-lg-10 col-lg-offset-1">

        @if(isset($lang_tab))
            @include('widgets.lang-tab', ['input' => 'text', 'info' => $data,'name' => 'title','tab_title' => 'Ad'])
        @endif

        {!! $fields !!}

    </div>


@endsection
