{!! Form::open(['url'=>route('gallery.store', [$route, $data->id]), 'method'=>'POST', 'class' => 'dropzone', 'files'=>true, 'id'=>'real-dropzone']) !!}

<div class="dz-message">

</div>

<div class="fallback">
    <input name="file" type="file" multiple />
</div>

{{ Form::hidden('type',$gallery_type) }}


<div class="dropzone-previews" id="dropzonePreview"></div>

@include('widgets.dropzone-template')

{!! Form::hidden('csrf-token', csrf_token(), ['id' => 'csrf-token']) !!}

{!! Form::close() !!}