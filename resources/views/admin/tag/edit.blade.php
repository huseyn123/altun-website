@extends('layouts.modal', ['route' => $route, 'method' => 'PUT', 'editor' => true, 'script' => true])
@section('title', $info->keyword)

@section('content')
    <div class="form-group">
        <label for="keyword" class="col-md-3 control-label">Açar söz</label>
        <div class="col-md-8">
            <input class="form-control" name="keyword" type="text" required="required" value="{{ $info->keyword }}">
        </div>
    </div>
    <ul class="nav nav-tabs">
        @foreach(config('app.locales') as $key => $locale)
            <li @if($loop->first) class="active" @endif><a href="#{{$key}}" data-toggle="tab"> {{ $key }}</a></li>
        @endforeach
    </ul>
    <br>
    <div class="tab-content">
        @foreach(config("app.locales") as $key => $locale)
            <div id="{{$key}}" class="tab-pane fade @if($loop->first) in active @endif">
                <div class="form-group">
                    <label for="name" class="col-md-3 control-label">Teq</label>
                    <div class="col-md-8">
                        <input class="form-control" name="name[{{$key}}]" type="text" value="{{ $tags[$loop->iteration-1]['name'] ?? null }}">
                    </div>
                </div>
            </div>
        @endforeach
    </div>
@endsection