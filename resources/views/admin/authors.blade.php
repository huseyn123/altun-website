@extends ('layouts.admin', ['table' => 'authors'])
@section ('title', $title)

@section('content')

    @include('widgets.modal-confirm')

    @component('admin.components.dt', ['create' => 'authors.create','filter' => config('config.filter-type'),'route' => 'authors', 'largeModal' => true, 'locale' => true,])
        @slot('table')
            {!! $dataTable->table(['class' => 'table table-striped table-hover', 'id' => 'authors', 'width' => '100%']) !!}
        @endslot
    @endcomponent

@endsection

@push('scripts')
    {!! $dataTable->scripts() !!}
@endpush