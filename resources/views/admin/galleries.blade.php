@extends ('layouts.admin')
@section ('title', $title)

@section('content')

    @include('widgets.modal-confirm')

    @if($route == 'apartments')
        @include('widgets.apartment_gallery')
    @endif

    <h3 class="text-center">{{ $data->id }}</h3>
    <h5 class="text-center">Yükləmək üçün aşağıdakı qutuya click edin. Şəklin ölçüsü {{ $width }}x{{ $height }} nisbətdə olmalıdır.</h5>
    @include('admin.gallery.dropzone')

    {!! $dataTable->table(['class' => 'table table-striped table-hover', 'id' => 'galleries', 'width' => '100%']) !!}

@endsection

@push('scripts')
    <script src="{{ asset("js/dropzone.js") }}"></script>

    @include('widgets.dropzone', ['maxFile' => albumLimit(), 'data' => $data])

    {!! $dataTable->scripts() !!}
@endpush