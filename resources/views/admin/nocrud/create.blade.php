@extends ('layouts.modal', ['script' => true ])
@section ('title', $title)

@section('content')

    <div class="col-lg-10 col-lg-offset-1">

        @foreach($form_data as $crud)
            @include('widgets.lang-tab', ['input' => 'text', 'name' => $crud['name'],'tab_title' => isset($crud['tab_title']) ? $crud['tab_title'] : null ])
        @endforeach

    </div>

@endsection
