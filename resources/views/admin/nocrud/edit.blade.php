@extends('layouts.modal', ['route' => $route, 'method' => 'PUT','script' => true])
@section('title', $data->name ?? $title)

@section('content')

    <div class="col-lg-10 col-lg-offset-1">


        @foreach($form_data as $crud)
            @include('widgets.lang-tab', ['input' => 'text', 'info' => $data,'name' => $crud['name']])
        @endforeach

    </div>


@endsection
