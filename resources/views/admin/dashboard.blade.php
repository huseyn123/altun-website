@extends('layouts.admin')
@section('title', $title)
@section('content')


    <p class="text-center text-black text-bold">Aşağıdakı diaqramlarda Google Analyticsdən gələn nəticələr əks olunacaq.</p>
    @if(is_null(getConfig()['google_analytic_view_id']))
        @include('widgets.alert', array('class' => 'danger', 'message' => 'Google Analytics quraşdırılmayıb!', 'link' => ['title' => 'Quraşdır', 'url' => route('analytic.index') ]))
    @endif

    <div class="row">
        <div class="col-md-12">
            @include('widgets.line-chart')
        </div>
    </div>

    <!-- Main row -->
    <div class="row">
        <div class="col-md-6">
            @include('widgets.pie-chart')
        </div>
        <!-- /.col -->
        <div class="col-md-6">
            @include('widgets.bar-chart')
        </div>
    </div>
@endsection

@push('scripts')

    <script src="{{ asset('js/admin/raphael.js') }}"></script>
    <script src="{{ asset('js/admin/morris.js') }}"></script>
    <script src="{{ asset('js/admin/chart.js') }}"></script>

    <script>
        $(function () {
            "use strict";

            // LINE CHART
            var line = new Morris.Line({
                element: 'line-chart',
                resize: true,
                data: {!! json_encode($mostVisited) !!},
                xkey: 'pageTitle',
                ykeys: ['pageViews'],
                labels: ['Baxış sayı'],
                lineColors: ['#3c8dbc'],
                hideHover: 'auto',
                parseTime:false
            });
        });


        var areaChartData = {
            labels  : {!! json_encode($visitedMonths) !!},
            datasets: [

                {
                    label               : 'Digital Goods',
                    fillColor           : 'rgba(60,141,188,0.9)',
                    strokeColor         : 'rgba(60,141,188,0.8)',
                    pointColor          : '#3b8bba',
                    pointStrokeColor    : 'rgba(60,141,188,1)',
                    pointHighlightFill  : '#fff',
                    pointHighlightStroke: 'rgba(60,141,188,1)',
                    data                : {!! json_encode($visitors) !!},
                }
            ]
        }


        //-------------
        //- PIE CHART -
        //-------------
        // Get context with jQuery - using jQuery's .get() method.
        var pieChartCanvas = $('#pieChart').get(0).getContext('2d')
        var pieChart       = new Chart(pieChartCanvas)
        var PieData        = {!! json_encode($browsers) !!}
        var pieOptions     = {
            //Boolean - Whether we should show a stroke on each segment
            segmentShowStroke    : true,
            //String - The colour of each segment stroke
            segmentStrokeColor   : '#fff',
            //Number - The width of each segment stroke
            segmentStrokeWidth   : 2,
            //Number - The percentage of the chart that we cut out of the middle
            percentageInnerCutout: 50, // This is 0 for Pie charts
            //Number - Amount of animation steps
            animationSteps       : 100,
            //String - Animation easing effect
            animationEasing      : 'easeOutBounce',
            //Boolean - Whether we animate the rotation of the Doughnut
            animateRotate        : true,
            //Boolean - Whether we animate scaling the Doughnut from the centre
            animateScale         : false,
            //Boolean - whether to make the chart responsive to window resizing
            responsive           : true,
            // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
            maintainAspectRatio  : true,
            //String - A legend template
            legendTemplate       : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<segments.length; i++){%><li><span style="background-color:<%=segments[i].fillColor%>"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>'
        }
        //Create pie or douhnut chart
        // You can switch between pie and douhnut using the method below.
        pieChart.Doughnut(PieData, pieOptions)


       //-------------
        //- BAR CHART -
        //-------------
        var barChartCanvas                   = $('#barChart').get(0).getContext('2d')
        var barChart                         = new Chart(barChartCanvas)
        var barChartData                     = areaChartData
        var barChartOptions                  = {
          //Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
          scaleBeginAtZero        : true,
          //Boolean - Whether grid lines are shown across the chart
          scaleShowGridLines      : true,
          //String - Colour of the grid lines
          scaleGridLineColor      : 'rgba(0,0,0,.05)',
          //Number - Width of the grid lines
          scaleGridLineWidth      : 1,
          //Boolean - Whether to show horizontal lines (except X axis)
          scaleShowHorizontalLines: true,
          //Boolean - Whether to show vertical lines (except Y axis)
          scaleShowVerticalLines  : true,
          //Boolean - If there is a stroke on each bar
          barShowStroke           : true,
          //Number - Pixel width of the bar stroke
          barStrokeWidth          : 2,
          //Number - Spacing between each of the X value sets
          barValueSpacing         : 5,
          //Number - Spacing between data sets within X values
          barDatasetSpacing       : 1,
          //String - A legend template
          legendTemplate          : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<datasets.length; i++){%><li><span style="background-color:<%=datasets[i].fillColor%>"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>',
          //Boolean - whether to make the chart responsive
          responsive              : true,
          maintainAspectRatio     : true
        }

        barChartOptions.datasetFill = false
        barChart.Bar(barChartData, barChartOptions)
    </script>
@endpush
