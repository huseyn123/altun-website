@extends ('layouts.admin')
@section ('title', $title)

@section ('content')

    @component('admin.components.dt')
        @slot('table')
            {!! $dataTable->table(['class' => 'table table-striped table-hover', 'id' => 'configs', 'width' => '100%']) !!}
        @endslot
    @endcomponent

@endsection

@push('scripts')
    {!! $dataTable->scripts() !!}
@endpush