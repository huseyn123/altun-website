@extends ('layouts.admin')
@section ('title', $title)

@section ('content')

    @include('widgets.modal-confirm')

    @component('admin.components.dt', ['create' => 'productAge.create', 'route' => 'productAge'])
        @slot('table')
            {!! $dataTable->table(['class' => 'table table-striped table-hover', 'id' => 'productAge', 'width' => '100%']) !!}
        @endslot
    @endcomponent

@endsection

@push('scripts')
    {!! $dataTable->scripts() !!}
@endpush