@extends('layouts.modal', ['route' => $route, 'method' => 'PUT', 'script' => $script ?? true, 'editor' => $editor ?? false])
@section('title', $data->name ?? $title)

@section('content')
    {!! $fields !!}
@endsection