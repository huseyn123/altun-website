@extends('layouts.modal', ['route' => $route, 'editor' => $editor ?? false, 'script' => $script ?? true])
@section('title', $title)

@section('content')
    {!! $fields !!}
@endsection

@push('scripts')
<script>

    @if(isset($editor))
        CKEDITOR.replace('editor');
    @endif

    @if(isset($translation))
        CKEDITOR.replace('editor2');
    @endif
</script>
@endpush
