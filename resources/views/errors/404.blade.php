@extends ('layouts.web', [
'error_c' => true,
'src' => 'img/404.jpg',
'page_heading' => Cache::get('dictionary_'.app()->getLocale())['nothing_found_title'],
'menuWithoutSlug' => true,
'menu' => \App\Logic\Menu::all(),
'config' => \App\Logic\WebCache::config()->pluck('value', 'key'),
'dictionary' => Cache::get('dictionary_'.app()->getLocale()),
'social' => Cache::get('social'),
'lang' => app()->getLocale(),

])

@section ('content')

    <div class="container">
        <ol id="breadcrumb" class="container" itemscope itemtype="http://schema.org/BreadcrumbList">
            <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                <a itemprop="item" href="{{ route('home') }}"><span itemprop="name">{{ $dictionary['home_page'] }}</span></a>
                <span itemprop="position" content="1"><i class="fa fa-angle-right"></i></span>
            </li>
            <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                <a itemprop="item"><span itemprop="name">Səhifə tapılmadı</span></a>
                <span itemprop="position" content="2"></span>
            </li>
        </ol>
    </div>

    <div class="container">
        <div class="not_found">
            <img  src="{{asset('images/404.svg')}}" />
            <div>
                <div class="title">{{ $dictionary['nothing_found_heading'] }}</div>
                <a href="{{ route('home') }}">{{ $dictionary['home_page'] }}</a>
            </div>
        </div>
    </div>


@endsection

