<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => 'The :attribute must be accepted.',
    'active_url'           => 'The :attribute is not a valid URL.',
    'after'                => 'The :attribute must be a date after :date.',
    'after_or_equal'       => 'The :attribute must be a date after or equal to :date.',
    'alpha'                => 'The :attribute may only contain letters.',
    'alpha_dash'           => 'The :attribute may only contain letters, numbers, and dashes.',
    'alpha_num'            => 'The :attribute may only contain letters and numbers.',
    'array'                => 'The :attribute must be an array.',
    'before'               => 'The :attribute must be a date before :date.',
    'before_or_equal'      => 'The :attribute must be a date before or equal to :date.',
    'between'              => [
        'numeric' => 'The :attribute must be between :min and :max.',
        'file'    => 'The :attribute must be between :min and :max kilobytes.',
        'string'  => 'The :attribute must be between :min and :max characters.',
        'array'   => 'The :attribute must have between :min and :max items.',
    ],
    'boolean'              => 'The :attribute field must be true or false.',
    'confirmed'            => 'The :attribute confirmation does not match.',
    'date'                 => 'The :attribute is not a valid date.',
    'date_format'          => 'The :attribute does not match the format :format.',
    'different'            => 'The :attribute and :other must be different.',
    'digits'               => 'The :attribute must be :digits digits.',
    'digits_between'       => 'The :attribute must be between :min and :max digits.',
    'dimensions'           => 'The :attribute has invalid image dimensions.',
    'distinct'             => 'The :attribute field has a duplicate value.',
    'email'                => 'The :attribute must be a valid email address.',
    'exists'               => 'The selected :attribute is invalid.',
    'file'                 => 'The :attribute must be a file.',
    'filled'               => 'The :attribute field must have a value.',
    'image'                => 'The :attribute must be an image.',
    'in'                   => 'The selected :attribute is invalid.',
    'in_array'             => 'The :attribute field does not exist in :other.',
    'integer'              => 'The :attribute must be an integer.',
    'ip'                   => 'The :attribute must be a valid IP address.',
    'ipv4'                 => 'The :attribute must be a valid IPv4 address.',
    'ipv6'                 => 'The :attribute must be a valid IPv6 address.',
    'json'                 => 'The :attribute must be a valid JSON string.',
    'max'                  => [
        'numeric' => 'The :attribute may not be greater than :max.',
        'file'    => 'The :attribute may not be greater than :max kilobytes.',
        'string'  => 'The :attribute may not be greater than :max characters.',
        'array'   => 'The :attribute may not have more than :max items.',
    ],
    'mimes'                => 'The :attribute must be a file of type: :values.',
    'mimetypes'            => 'The :attribute must be a file of type: :values.',
    'min'                  => [
        'numeric' => 'The :attribute must be at least :min.',
        'file'    => 'The :attribute must be at least :min kilobytes.',
        'string'  => 'The :attribute must be at least :min characters.',
        'array'   => 'The :attribute must have at least :min items.',
    ],
    'not_in'               => 'The selected :attribute is invalid.',
    'not_regex'            => 'The :attribute format is invalid.',
    'numeric'              => 'The :attribute must be a number.',
    'present'              => 'The :attribute field must be present.',
    'regex'                => 'The :attribute format is invalid.',
    'required'             => 'The :attribute field is required.',
    'required_if'          => 'The :attribute field is required when :other is :value.',
    'required_unless'      => 'The :attribute field is required unless :other is in :values.',
    'required_with'        => 'The :attribute field is required when :values is present.',
    'required_with_all'    => 'The :attribute field is required when :values is present.',
    'required_without'     => 'The :attribute field is required when :values is not present.',
    'required_without_all' => 'The :attribute field is required when none of :values are present.',
    'same'                 => 'The :attribute and :other must match.',
    'size'                 => [
        'numeric' => 'The :attribute must be :size.',
        'file'    => 'The :attribute must be :size kilobytes.',
        'string'  => 'The :attribute must be :size characters.',
        'array'   => 'The :attribute must contain :size items.',
    ],
    'string'               => 'The :attribute must be a string.',
    'timezone'             => 'The :attribute must be a valid zone.',
    'unique'               => 'The :attribute has already been taken.',
    'uploaded'             => 'The :attribute failed to upload.',
    'url'                  => 'The :attribute format is invalid.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */


    'custom' => [
        'email' => [
            'required' => 'E-poçt ünvanı qeyd olunmayıb.',
            'email' => 'E-poçt forması yanlışdır.',
            'unique' => 'E-poçt artıq istifadə olunur.',
            'confirmed' => 'E-poçt üst üstə düşmür'
        ],
        'first_name' => [
            'required' => 'Adınızı qeyd etməmisiniz.',
            'min' => 'Adınızı düzgün şəkildə yazın',
        ],
        'last_name' => [
            'required' => 'Soyadınızı qeyd etməmisiniz.',
            'min' => 'Soyadınızı düzgün şəkildə yazın',
        ],
        'full_name' => [
            'required' => 'Ad və soyadınızı qeyd etməmisiniz.',
            'min' => 'Ad və soyadınızı düzgün şəkildə yazın',
        ],
        'phone' => [
            'required' => 'Əlaqə nömrənizi qeyd etməmisiniz.',
            'unique' => 'Telefon nömrəsi artıq istifadə olunur.',
            'numeric' => 'Əlaqə nömrənizi düzgün şəkildə yazın.',
            'digits_between' => 'Əlaqə nömrənizi düzgün şəkildə yazın.',
        ],
        'sex' => [
            'required' => 'Cinsiyyəti müəyyən etməmisiz.',
        ],
        'cell' => [
            'required' => 'Əlaqə nömrənizi qeyd etməmisiniz.',
        ],
        'entry_date' => [
            'required' => 'Giriş Tarixi qeyd etməmisiniz.',
        ],
        'output_date' => [
            'required' => 'Çıxış Tarixi qeyd etməmisiniz.',
        ],
        'type' => [
            'required' => 'Otağ növü qeyd etməmisiniz.',
        ],
        'child' => [
            'required' => 'Uşaq sayı qeyd etməmisiniz.',
            'numeric' => 'Uşaq sayı rəqəm olmalıdır.',
        ],
        'feeding' => [
            'required' => 'Qidalanmanı qeyd etməmisiniz-.',
        ],
        'subject' => [
            'required' => 'Mövzu boş saxlanılmamalıdır.',
            'min' => 'Mövzu :min simvoldan az olmamalıdır',
        ],
        'text' => [
            'required' => 'Mətn boş saxlanılmamalıdır.',
            'min' => 'Mətn :min simvoldan az olmamalıdır',
            'max' => 'Mətn :max simvoldan çox olmamalıdır'
        ],
        'phone-full' => [
            'required_with' => 'Əlaqə nömrənizi düzgün şəkildə yazın.'
        ],
        'password' => [
            'required' => 'Şifrənizi qeyd etməmisiniz.',
            'min' => 'Şifrə ən azı 6 simvoldan ibarət olmalıdır.',
        ],
        'resume' => [
            'required' => 'CV əlavə edilməyib.',
            'mimes' => 'CV :values formatda olmalıdır.',
            'max' => 'CV :max kb-dan çox olmamalıdır.',
        ],

        //adminka//

        'name' => [
            'required' => 'Ad boş saxlanılmamalıdır.',
        ],
        'title' => [
            'required' => 'Başlıq boş saxlanılmamalıdır.',
        ],
        'published_at' => [
            'required' => 'Tarix qeyd olunmayıb.',
        ],
        'image_original' => [
            'required' => 'Şəklin seçilməsi mütləqdir.',
        ],
        'lang_id' => [
            'required' => 'Dil seçilməyib.',
        ],
        'category_id' => [
            'required' => 'Kateqoriyanın seçilməsi mütləqdir. Əgər kateqoriya siyahıda yoxdursa, səhifələr -> kateqoriyalar bölməsinə daxil olub yaratmağınız gərəkdir.',
        ],
        'current_password' => [
            'required' => 'İndiki şifrənizi qeyd etməmisiniz!',
            'current_password_match' => 'İndiki şifrəniz düzgün yazılmayıb!',
        ],
        'slug' => [
            'unique' => ':attribute artıq istifadə olunur.'
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [],

];
