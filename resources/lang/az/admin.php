<?php
/**
 * Created by PhpStorm.
 * User: Rashad
 * Date: 4/14/18
 * Time: 21:09
 */

return [
    'login' => 'Daxil ol',
    'login_text' => '',
    'login_remember' => 'Yadda saxla',
    'login_sign_in' => 'Daxil ol',
    'password_lost' => 'Şifrəni unutdum',
    'password_reset' => 'Şifrənizin dəyişdirilməsi barədə olan məktub göstərdiyiniz elektron ünvana göndəriləcək.',
    'password_reset_link' => 'Göndər',
    'logout' => 'Çıxış',
    'profile' => 'Profil',
];
