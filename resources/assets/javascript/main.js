(function($) {

	'use strict';
			 var isMobile = {
					 Android: function() {
							 return navigator.userAgent.match(/Android/i);
					 },
					 BlackBerry: function() {
							 return navigator.userAgent.match(/BlackBerry/i);
					 },
					 iOS: function() {
							 return navigator.userAgent.match(/iPhone|iPad|iPod/i);
					 },
					 Opera: function() {
							 return navigator.userAgent.match(/Opera Mini/i);
					 },
					 Windows: function() {
							 return navigator.userAgent.match(/IEMobile/i);
					 },
					 any: function() {
							 return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
					 }
			 }; // is Mobile

        var responsiveMenu = function() {
            $('.btn-menu').on('click', function() {         
								$('#mainnav-mobi').slideToggle(300);
								$('.header-mobile').toggleClass('brdr');
								$(this).toggleClass('active');
								$('#mainnav-mobi .menu').slideUp(300);
								$('#mainnav-mobi .btn-mega').removeClass('active');
                return false;
            });

            $('#mainnav-mobi .btn-mega').on('click', function() {         
								$('#mainnav-mobi .menu').slideToggle(300);
                $(this).toggleClass('active');
                return false;
            });

        }; // Responsive Menu

			 var mainSlider = function() {
					 $('.main-slider').owlCarousel({
							 autoplay:true,
							 autoplayTimeout:8000,
							 autoplaySpeed:1200,
							 smartSpeed:700,
							 nav: false,
							 dots: true,
							 margin:0,
							 touchDrag:true,
							 mouseDrag:true,
							 loop:true,
							 items:1,
							 responsive:{
								0:{
										dots: false
								},
								479:{
										dots: false
								},
								599:{
										 dots: false
								},
								768:{
										dots: true
								},
								991:{
										dots: true
								},
								1200: {
										dots: true
								}
						}
					 });
			 };// main Slider

			 var projectsSlider = function() {
					 $('.projects-slider').owlCarousel({
							 autoplay:true,
							 autoplayTimeout:8000,
							 autoplaySpeed:1200,
							 smartSpeed:700,
							 nav: false,
							 dots: true,
							 margin:0,
							 touchDrag:true,
							 mouseDrag:true,
							 loop:true,
							 items:1,
							 responsive:{
								0:{
										dots: false
								},
								479:{
										dots: false
								},
								599:{
										 dots: false
								},
								768:{
										dots: true
								},
								991:{
										dots: true
								},
								1200: {
										dots: true
								}
						}
					 });
			 };// Projects Slider


			var slideWriters = function() {
				$(".writers-carousel").owlCarousel({
					autoplay:true,
					nav: false,
					dots: true,
					responsive: true,
					margin:30,
					loop:true,
					items:4,
					responsive:{
						0:{
							items: 1,
							dots: false,
							margin:20,
							stagePadding: 40
						},
						479:{
							items: 1,
							dots: false,
							stagePadding: 50
						},
						600:{
							items: 2,
							dots: false
						},
						768:{
							items: 3,
							margin:20,
						},
						991:{
							items: 4
						},
						1200: {
							items: 4
						}
					}
				});
			};// slide Writers
			 
			 var slideSimilar = function() {
				$(".similar-carousel").owlCarousel({
					autoplay:true,
					nav: true,
					dots: true,
					responsive: true,
					margin:30,
					loop:true,
					touchDrag:false,
					mouseDrag:false,
					items:6,
					responsive:{
						0:{
							items: 2,
							dots: false,
							margin:20,
							touchDrag:true,
							mouseDrag:true
						},
						479:{
							items: 2,
							dots: false,
							touchDrag:true,
							mouseDrag:true
						},
						600:{
							items: 3,
							dots: false,
							touchDrag:true,
							mouseDrag:true
						},
						768:{
							items: 4,
							margin:20,
							touchDrag:true,
							mouseDrag:true
						},
						991:{
							items: 6
						},
						1200: {
							items: 6
						}
					}
				});
			};// slide Similar

			var mailSubscribe = function() {

			};

			 var scrollUp = function() {
					$.scrollUp({
						scrollText: '<i class=\'fa fa-angle-double-up\' aria-hidden=\'true\'></i>',
						scrollDistance: 1800,
						scrollSpeed: 500,
						animationSpeed: 500
					});
			 }; // Scrool Up

			 var showSuggestions = function() {
					 $( ".top-search .box-search" ).each(function() {
							 $( ".box-search input" ).on('input', (function() {
									 $(this).parent('.box-search').children('.search-suggestions').css({
											 opacity: '1',
											 visibility: 'visible',
											 top: '58px'
									 });
							 }));
							 $( ".box-search input" ).on('blur', (function() {
									 $(this).parent('.box-search').children('.search-suggestions').css({
											 opacity: '0',
											 visibility: 'hidden',
											 top: '100px'
									 });
							 }));
					 });

					 $( ".top-search .box-search" ).each(function() {
							 $( ".box-search input" ).on('input', (function() {

										// var product_id = $(this).data("id");
										// if(product_id) {
											
											$(".box-suggestions").addClass('on-progress');
											
											$.ajax({
												url: '/api/search',
												type: 'POST',
												dataType: 'jsonp',
												data: { 
														// product_id: product_id
												},
												
												error: function() {
														$(".box-suggestions").removeClass('on-progress');
												},

												success: function(resp) {
														if (resp.result == 1) {

														}

														$(".box-suggestions").removeClass('on-progress');
												}
											});
										// }
							 }));
					 });
			 }; // Show Suggestions

			 var mobileSearchToggle = function() {
				$("#top-search-trigger").off("click").on("click", function(e) {
				var selector = $(this).data('selector');
				
				$(selector).toggleClass('show').find('#search').focus();
				$(this).toggleClass('active');
				
				e.stopPropagation(),
				e.preventDefault();
				});
			 }; // Mobile Search Toggle

			var contactForm = function() {
				$("body").on("input", ":input", function() {
					$(this).removeClass("error");
				});

				$("#contact-form").on("submit", function() {
						var form = $(this);
						var submitable = true;
						$(":input.required", form).each(function() {
								if (!$(this).val()) {
										$(this).addClass("error");
										submitable = false;
								}
						});
						if (submitable) {
								// $("body").addClass("on-progress");
								$.ajax({
										url: form.attr("action"),
										type: 'POST',
										dataType: 'jsonp',
										data: {
												action: form.find(":input[name='action']").val(),
												fullname: form.find(":input[name='fullname']").val(),
												phone: form.find(":input[name='phone']").val(),
												time: form.find(":input[name='time']").val(),
												oformat: "json"
										},
										error: function() {
												$("body").removeClass("error");
												form.find(".form-result").text(form.find(".form-result").data("error"));
										},
										success: function(resp) {
												// $("body").removeClass("on-progress");
												if (resp.result == 1) {
														form.find(".contact-result").removeClass('invisible-box');
												} else {
														form.find(".form-result").html("");
														for (var i = 0; i < resp.msg.length; i++) {
																form.find(".form-result").append("<li>"+resp.msg[i]+"</li>");
														}
												}
										}
								});
						}
						return false;
				});
			}; // Contact Form

			 var detectViewport = function() {
					 $('[data-waypoint-active="yes"]').waypoint(function() {
							 $(this).trigger('on-appear');
					 }, { offset: '100%', triggerOnce: true });
					 $(window).on('load', function() {
							 setTimeout(function() {
									 $.waypoints('refresh');
							 }, 100);
					 });
			 }; // Detect Viewport

			 var removePreloader = function() { 
					 $(window).on('load', function() {
        			$('.preloader').delay(1000).fadeOut(600);
					 });  
			 }; // Remove Preloader

			var zoomImage = function() {
				$(document).ready(function(){
					$('#zoomable').zoom();
				});
			}; // Zoom Image

			var zoomPages = function() {
				$(document).ready(function(){
					$(".zoom-pages").fancybox({
						toolbar  : false,
						smallBtn : true,
						protect: true,
						iframe : {
							preload : false
						},
						afterLoad : function(instance, current) {
							var pixelRatio = window.devicePixelRatio || 1;

							if ( pixelRatio > 1.5 ) {
								current.width  = current.width  / pixelRatio;
								current.height = current.height / pixelRatio;
							}
						}
					});
				});
			}; // Zoom Video

	 // Dom Ready
	 $(function () {
		 mainSlider();
		 projectsSlider();
		 slideWriters();
		 slideSimilar();
		 // mailSubscribe();
		 // showSuggestions();
		 detectViewport();
		 // contactForm();
		 mobileSearchToggle();
		 responsiveMenu();
		 // removePreloader();
         zoomImage();
         zoomPages();
	 });
	 
		/**
		 * Validate email
		 * @param  {string}  email 
		 * @return {Boolean}       
		 */
		function isValidEmail(email) {
				var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
				return re.test(email);
		}

		/**
		 * Get the width of window default scrollbar
		 * @return {Integer} 
		 */
		function getScrollbarWidth() {
			var scrollDiv = document.createElement("div");
			scrollDiv.className = "scrollbar-measure";
			document.body.appendChild(scrollDiv);
			var w = scrollDiv.offsetWidth - scrollDiv.clientWidth;
			document.body.removeChild(scrollDiv);

			return w;
		}
	 

})(jQuery);