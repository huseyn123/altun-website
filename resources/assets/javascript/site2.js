//contact

$("body").on("input", ":input", function() {
    $(this).removeClass("error");
});

$("#contact-form").on("submit", function() {
    var form = $(this);
    var submitable = true;
    $(":input.required", form).each(function() {
        if (!$(this).val()) {
            $(this).addClass("error");
            submitable = false;
        }
    });
    if (submitable) {
        // $("body").addClass("on-progress");
        $.ajax({
            url:  form.attr('action'),
            type: form.attr('method'),
            data: new FormData(form[0]),
            dataType: "json",
            processData: false,
            contentType: false,
            error: function(xhr, status, error) {
                $("body").removeClass("error");
                form.find(".form-result").text(form.find(".form-result").data("error"));
            },
            success: function(resp) {
                // $("body").removeClass("on-progress");
                if (resp.code == 0) {
                    form.find(".contact-result").removeClass('invisible-box');
                } else {
                    form.find(".form-result").html("");
                    form.find(".form-result").append("<li>" + resp.msg + "</li>");
                }
            }
        });
    }
    return false;
});

//
//

$(document).on('submit', '.subscribe-form', function(e) {
    event.preventDefault();
    var action = $(this);
    $.ajax({
        url:  action.attr('action'),
        type: action.attr('method'),
        data: new FormData(action[0]),
        dataType: "json",
        processData: false,
        contentType: false,
        beforeSend: function(xhr) {
            action.find('input').removeClass('error');
            action.find('button').attr('disabled', 'disabled')
        },
        success: function(data) {
            if (data.code == 1) {
                $('.flat-newsletter .subscribe_message').text(data.msg);
                action.find('[name="email"]').addClass('error');
            } else if (data.code == 0) {
                action.find('input[type=email]').val('');
                $('.flat-newsletter .subscribe_message').text(data.msg);
            }
            action.find('button').removeAttr('disabled');
        }
    })
})

$('.sidebar-menu li.check-box .product_filtr').change(function(){
    console.log('sss');
    const queryString = window.location.search;
    var page = queryString.split('page=')[1];
    ProductFiltr(page,'category');
});

function ProductFiltr(page, blade_type){
    if(page === undefined){page =1}

    $('.preloader').show();
    var filtr = [];
    var sort = $('.sort .popularity select[name$="sort"]').val();
    if(sort !== undefined){
        if(sort !== 'null' && sort != 0){
            parametr='?sort='+sort;

        }else{
            parametr='?sort';
        }
    }else{
        parametr = '';
    }

    if(blade_type == 'category'){
        $.each($('.sidebar-menu li.check-box .product_filtr'), function(){
            if($(this).is(':checked') === true){
                filtr.push({'name':$(this).attr('name')+encodeURI('[]'),'val' : $(this).val()});
            }
        });
    }
    filtr.push({'name':'page','val':page});

    if (filtr.length > 0 && history.pushState) {
        $.each(filtr,function (key,value) {
            if(parametr != ''){
                parametr +='&'+value.name+'='+value.val;
            }else{
                parametr +='?'+value.name+'='+value.val;
            }
        });
        var newurl = window.location.protocol + "//" + window.location.host + window.location.pathname + parametr;
        window.history.pushState({path:newurl},'',newurl);

        $.ajax({
            url: newurl,
            type: 'GET',
            dataType: "json",
            beforeSend: function(response) {
            },
            success: function(response){
                $('.data_list .list_item').html(response.html);
                $('.preloader').hide();
            },
            error: function(res){
                $('.preloader').hide();
            }

        });
    }
}


$(document).on('click', '.imagebox-pagination ul.flat-pagination li a', function(event){
    event.preventDefault();
    var page = $(this).attr('href').split('page=')[1];
    ProductFiltr(page);
});

$(".sort .popularity select" ).change(function() {
    const queryString = window.location.search;
    var page = queryString.split('page=')[1];
    ProductFiltr(page);
});


// $(function() {
//
//     $.mask.definitions['9'] = '';
//     $.mask.definitions['d'] = '[0-9]';
//     $(".phonenumber").mask("+(994 dd) ddd dd dd");
//
// });


$("#subscribe-form").on("submit",function (event){

    event.preventDefault();
    subscribe($(this));
});

$('.report .tabs a').click(function () {
    var route = $(this).attr('href');

    $.ajax({
        url: route,
        beforeSend: function(response) {
        }
    })
    .done(function(response) {
        $('.report  .tabs_content').html(response.html);
    })
    .fail(function(xhr, ajaxOptions, thrownError) {
        requestSent = false;
        var json = JSON.parse(xhr.responseText);
    });
});


function subscribe(action){

    if (action.data('submitted') === true) {
        return false;
    }
    else
    {
        $.ajax({
            url:  action.attr('action'),
            type: action.attr('method'),
            timeout: 20000,
            data: action.serialize(),
            dataType: "json",

            beforeSend: function(response) {
                $("#loadingSubButton").button('loading');
            },

            success: function(response){
                $("#loadingSubButton").button('reset');

                if(response.code == 0) {

                    $('#subscribe-form')[0].reset();
                    setTimeout(function(){ $("#subscribeBox").html('');}, 3000);
                }

                $("#subscribeBox").html(response.msg);
            },

            error: function(res){
                $("#loadingSubButton").button('reset');
                $("#subscribeBox").html('<p>Unexpected Error!</p>');
            }
        });
    }
}



$(function(){

    function preventNumberInput(e) {
        var keyCode = (e.keyCode ? e.keyCode : e.which);
        if (keyCode > 47 && keyCode < 58) {
            e.preventDefault();
        }
    }
    function preventOnlyNumberInput(e) {
        var keyCode = (e.keyCode ? e.keyCode : e.which);
        if (keyCode > 31 && (keyCode < 48 || keyCode > 57)){
            e.preventDefault();
        }
    }

    $('.alphaonly').keypress(function(e) {
        preventNumberInput(e);
    });

    $('.alphaonlynumb').keypress(function(e) {
        preventOnlyNumberInput(e);
    });



});

//Rezervation
function g_form(action,type){
    $.ajax({
        url:  action.attr('action'),
        type: action.attr('method'),
        data: new FormData(action[0]),
        dataType: "json",
        processData: false,
        contentType: false,
        beforeSend: function(response) {
            $(".msg").html('');
            $("#loadingButton").button('loading');
        },

        success: function(response){
            $("#loadingButton").button('reset');

            if(response.code == 1) {

                $('button[type=submit]').attr('disabled',true);

                action.data('submitted', true);
                $(".msg").html(response.msg);
            }
            else{
                $("#"+type)[0].reset();
                $(".msg").html('<i class="fa fa-check" aria-hidden="true"></i>'+response.msg);
            }

        },

        error: function(res){
            $("#loadingButton").button('reset');
            $(".msg").html('Unexpected Error!');
        }
    });
}
$("#product_form").on("submit",function (event){

    event.preventDefault();
    MainForm($(this),'product_form');
});

$("#st_id").on("submit",function (event){

    event.preventDefault();
    MainForm($(this),'st_id');
});

$(".main_f").on("submit",function (event){
    var id=$(this).attr('id');
    event.preventDefault();
    MainForm($(this),id);
});

function MainForm(action,type)  {
    $.ajax({
        url:  action.attr('action'),
        type: action.attr('method'),
        data: new FormData(action[0]),
        dataType: "json",
        processData: false,
        contentType: false,
        beforeSend: function(response) {
            $("#"+type+"_message").html('');
        },

        success: function(response){

            if(response.code == 1) {
                $("#"+type+" span.error_message").html(response.msg);
            }
            else{
                $('#'+type)[0].reset();
                $('.main_'+type).addClass('success');
                $('.s_form').show();
            }
        },

        error: function(res){
            $("#"+type+" .error_message").html('Unexpected Error!');
        }
    });
}






