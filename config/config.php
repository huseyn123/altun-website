<?php

return [
    'status' => ['Gizli', 'Aktiv'],
    'application_status' => ['Təsdiq et','Təsdiqlənib'],
    'alert' => ['danger', 'success'],
    "slider-type" => [1 => "Slider", "Kompaniya"],
    "article-status" => ["Qaralama", "Dərc olunub"],
    "building_status" => ["Köhnə tikili", "Yeni tikili"],
    "room_count" => [1=>1,2,3,4,5],
    "label" => ["danger", "success"],
    "menu-target" => [1 => "Self", 0 => "Blank"],
    "menu-visibility" => [0 => "Hidden","Header and Footer", "Header", "Footer","Footer2"],
    "menu-visibility-boolean" => [0 => "Gizli", "Aktiv"],
    "menu-visibility-label" => [0 => "danger", "success", "warning", "warning", "info"],
    "role" => [1 => "Administrator", "Editor"],
    "blog_page" => [0 => "No", "Yes"],

    "social-network" => ['facebook','instagram','youtube'],
    "social-network-class" => ['facebook' =>'facebook','instagram' => 'instagram','youtube' => 'youtube-play'],



    "subscriber-label" => ["0" => "warning", "1" => "success"],
    "subscriber-status" => ["0" => "pending", "1" => "active"],
    "yes-no" => [0 => "minus", "plus"],
    'prefix-number' => [50 => 50, 51 => 51, 55 => 55, 70 => 70, 77 => 77],
    'slug_replacement' => ['az' => 'azerbaijani', 'en' => 'azerbaijani', 'ru' => 'russian'],
    'lang' => ['az' => 'AZ', 'en' => 'EN', 'ru' => 'RU'],

    "model" => ['articles' => 'App\Models\Article','page' => 'App\Models\Page', 'pageTranslation' => 'App\Models\PageTranslation','product' => 'App\Models\Product','products' => 'App\Models\Product', 'productTranslation' => 'App\Models\ProductTranslation', 'articleTranslation' => 'App\Models\ArticleTranslation','authors' => 'App\Models\Author', 'authorTranslation' => 'App\Models\AuthorTranslation'],

    "product_filtr_order" => [1 => 'Əvvəlcə ucuz',2=>'Əvvəlcə bahalı'],
    "product_filtr_order_type" => [1 => 'asc',2=>'desc'],

    "slider_category" => [
        0 =>'Slider',
        1 =>'Layihələr',
    ],

    "template" => [
//        1 =>'Dropdown',
        2 =>'Haqqımızda',
        4 =>'Əlaqə',
        5 =>'Yazarlar səhifəsi',
        6 =>'Kitablar səhifəsi',
    ],


    'filter-type' => ['Bütün', 'Aktiv', 'Silinən'],
    'filter-type-2' => ['Bütün', 'Aktiv', 'Silinən'],
    'custom-apartment-type' => ['Bütün', 'Aktiv', 'Silinən'],

    'gallery-side' => [1 => 'left', 'right', 'center'],
    'gallery_size' =>[
        20 =>['width' => '345','height'=>'255'],
    ],
     'page_size'  =>[
         2 =>['width' => '720','height'=>'430'],

     ],
    'thumb_page_size'  =>[
        2 =>['width' => '200','height'=>'120'],
    ],

    'page_size2'  =>[
        4 =>['width' => '720','height'=>'555'],
    ],
    'thumb_page_size2'  =>[
    ],

    'crud'  =>[
        'articles-list' =>['width' => '720','height'=>'525'],
    ],

];
